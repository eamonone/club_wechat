package com.club100;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Auther: Grazer_Ma
 * @Date: 2020/7/22 10:46:55
 * @Description: Club100ApplicationTests 测试类。
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Club100ApplicationTests {

    @Test
    void contextLoads() {
    }

}
