/*
package com.club100.project.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.*;
import com.club100.project.wechat.utils.EmptyUtils;
import com.club100.project.wechat.utils.JsonUtils;
import com.club100.project.wechat.utils.PrimaryKeyIdUtils;
import com.club100.project.wechat.utils.ReturnUtils;
import lombok.extern.slf4j.Slf4j;
import me.himanshusoni.gpxparser.GPXParser;
import me.himanshusoni.gpxparser.modal.GPX;
import me.himanshusoni.gpxparser.modal.Waypoint;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

*/
/**
 * @Auther: Grazer_Ma
 * @Date: 2020/7/22 10:48:26
 * @Description: DevTests 测试类。
 *//*

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DevTests {

    @Resource
    private QiniuService qiniuService;

    @Autowired
    private RestTemplate restTemplate;

    // Main card information.
    @Autowired
    private ICardService cardService;

    // Favorite route information.
    @Autowired
    private IFavoriteRouteService favoriteRouteService;

    // Main inspire information.
    @Autowired
    private IInspireService inspireService;

    // Mark picture information.
    @Autowired
    private IMarkPictureService markPictureService;

    // Mark route information.
    @Autowired
    private IMarkRouteService markRouteService;

    // Mark route user information.
    @Autowired
    private IMarkRouteUserService markRouteUserService;

    // Main route information.
    @Autowired
    private IRouteService routeService;

    // Main route and route detail information.
    @Autowired
    private IRouteAndDetailService routeAndDetailService;

    // Route detail information.
    @Autowired
    private IRouteDetailService routeDetailService;

    // User information.
    @Autowired
    private IUserService userService;

    // Mail part.
    @Autowired
    private JavaMailSender javaMailSender;

    @Test
    public void updateUserCity() {

        User user = new User();
        user.setUserId(666L);
        user.setUserCity(12);

        int i = userService.updateUserCity(user);
        System.out.println("Rows: " + i);

    }

    @Test
    public void toJson() {
        ReturnInfo returnInfo = new ReturnInfo();
        returnInfo.code = 200;
        returnInfo.message = "message";
        returnInfo.data = "data";
        System.out.println(" ====== return: " + returnInfo);
        System.out.println(" ====== return.toJson: " + returnInfo.toJson(returnInfo));
    }

    @Test
    public void createObj() {
//        EmptyUtils emptyUtils = new EmptyUtils();
//        System.out.println(emptyUtils.toString());
        System.out.println();
    }

    @Test
    public void selectOneUserById() {

        User testUserEntity = new User();
        testUserEntity.setUserId(666L);
        User user = userService.selectOneById(testUserEntity);

        System.out.println("===+++User: " + user);
        System.out.println("===+++User: " + user.toJson(user));

    }

    @Test
    public void selectCardList() {

        List<Card> cardList = cardService.page(null);
        System.out.println("===+++cardList: " + cardList);

    }

    @Test
    public void selectCardAtMostFiveList() {

        // Initialization.
        Card searchedCardEntity = new Card();
        // Assign variables.
        searchedCardEntity.setCityType(1);
        // Search for the card's information.
        List<Card> atMostFiveCardsList = cardService.selectCardAtMostFiveList(searchedCardEntity);

        System.out.println("===atMostFiveCardsList: " + atMostFiveCardsList);

        String json = JSON.toJSONString(atMostFiveCardsList);
        System.out.println("===json: " + json);

        String s = JsonUtils.S_listToJsonArray(atMostFiveCardsList);
        System.out.println("===sss: " + s);

    }

    @Test
    public void slideToMainRoute() {

        // Initialization.
        Route searchedRouteEntity = new Route();
        // Assign variables.
        searchedRouteEntity.setCityType(1);
        // Search for the route's information (At most 2).
        List<Route> atMostTwoRoutesList = routeService.selectRouteAtMostTwoList(searchedRouteEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(atMostTwoRoutesList));

    }

    @Test
    public void slideToMainInspire() {

        // Initialization.
        Inspire searchedInspireEntity = new Inspire();
        // Assign variables.
        searchedInspireEntity.setCityType(1);
        // Search for the inspire's information (At most 2).
        List<Inspire> atMostTwoInspiresList = inspireService.selectInspireAtMostTwoList(searchedInspireEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(atMostTwoInspiresList));

    }

    @Test
    public void userInfo() {

        // Initialization.
        User searchedUserEntity = new User();
        // Assign variables.
        searchedUserEntity.setUserId(666L);
        // Search for the user's information.
        User returnedUser = userService.selectOneById(searchedUserEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        System.out.println("====== Result: " + returnedUser.toJson(returnedUser));

    }

    @Test
    public void selectFavoriteRoute() {

        // Initialization.
        FavoriteRoute searchedFavoriteRouteEntity = new FavoriteRoute();
        // Assign variables.
        searchedFavoriteRouteEntity.setUserId(666L);
        // Search for the favorite routes' information.
//        List<FavoriteRoute> favoriteRoutesList = favoriteRouteService.selectFavoriteRouteList(searchedFavoriteRouteEntity);
        List<FavoriteRoute> favoriteRoutesList = favoriteRouteService.selectFavoriteRouteByPageList(666L, 0, 2);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.
        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(favoriteRoutesList));

    }

    @Test
    public void selectMarkRoute() {

        // Initialization.
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setUserId(666L);
        // Search for the mark routes' information.
        List<MarkRoute> markRoutesList = markRouteService.selectMarkRouteByUserIDList(searchedMarkRouteEntity);
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(userInfo)");
        // Read success.

        List<Long> list = new ArrayList<>();

        markRoutesList.stream().forEach(eachRoute -> {
            System.out.println("EachRoute: " + eachRoute);
            System.out.println("GetRouteId: " + eachRoute.getRouteId());
            list.add(eachRoute.getRouteId());
        });

        Long[] longArray = list.toArray(new Long[list.size()]);
        System.out.println("Array: " + longArray);
        System.out.println("Array0: " + Long.valueOf(longArray[0].toString()));
        System.out.println("Array1: " + Long.valueOf(longArray[1].toString()));

        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(markRoutesList));

    }

    @Test
    public void selectByRouteIds() {

//        List<Route> routesList = routeService.selectByRouteIds(1L, 2L, 3L);
//        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(routesList));

    }

    @Test
    public void selectRouteList() {

        Integer[] routeTerrain = {1, 2, 3};
//        Integer[] routeTerrain = new Integer[]{};
//        System.out.println("====routeTerrain: " + routeTerrain);
//        System.out.println("====routeTerrain: " + routeTerrain[0]);
        Integer[] routeType = {2, 3};
//        Integer[] routeType = new Integer[]{};
//        System.out.println("====routeType: " + routeType);
//        System.out.println("====routeType: " + routeType[0]);
//        List<Route> returnedRouteLists = routeService.selectRouteList(userCity, routeDistance1Begin, routeDistance1End, routeDistance2Begin, routeDistance2End, routeDistance3Begin, routeDistance3End, routeDistance4Begin, routeDistance4End, routeDistance5Begin, routeDistance5End, routeDistance6Begin, routeDistance6End, routeTerrain, routeType);
//        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(selectRouteList));

    }

    @Test
    public void selectInspireList() {

        // Search for the inspire's information (with or without conditions).
        List<Inspire> returnedInspireLists = inspireService.selectInspireList();
        // Read failure.
//        if (null == returnedUser) return ReturnUtils.S_returnReadFailureInfo("club_user(chooseCity)");
        // Read success.
        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(returnedInspireLists));

    }

    @Test
    public void moreRecentlyRider() {

        // Initialization
        MarkRoute searchedMarkRoute = new MarkRoute();
        // Assign variables.
        searchedMarkRoute.setRouteId(1L);
        // Search for all marked users' information.
        List<MarkRouteUser> returnedMarkRouteUsersList = markRouteUserService.selectMarkedUserList(searchedMarkRoute);
        System.out.println("+++++++++++++   : " + returnedMarkRouteUsersList);
        // Read success.
        System.out.println("====== Result: " + JsonUtils.S_listToJsonArray(returnedMarkRouteUsersList));

    }

    @Test
    public void insertMarkPictureInBatch() {

        // Initialization & Assign variables.
        List<MarkPicture> insertedMarkPictureList = new ArrayList<>();
        Long routeId = 12345L;
        Long userId = 54321L;
        Map<Integer, String> picsMap = new HashMap<>();
        picsMap.put(1, "aaa");
        picsMap.put(2, "bbb");
        picsMap.put(3, "ccc");
        picsMap.put(4, "ddd");
        picsMap.put(5, "eee");
        picsMap.put(6, "fff");

        // Insert the pictures in batch.
        for (Map.Entry<Integer, String> eachPicMap : picsMap.entrySet()) {
            MarkPicture insertedMarkPictureEntity = new MarkPicture();
            insertedMarkPictureEntity.setMarkPictureSequence(eachPicMap.getKey());
            insertedMarkPictureEntity.setMarkPictureUrl(eachPicMap.getValue());
            insertedMarkPictureEntity.setRouteId(routeId);
            insertedMarkPictureEntity.setUserId(userId);
            insertedMarkPictureList.add(insertedMarkPictureEntity);
        }

        int insertInBatchRowsInt = markPictureService.insertMarkPictureInBatch(insertedMarkPictureList);
        System.out.println("======Result: " + insertInBatchRowsInt);

    }

    @Test
    public void selectRouteAndDetail() {

        // Initialization.
        RouteAndDetail searchedRouteAndDetailEntity = new RouteAndDetail();
        // Assign variables.
        searchedRouteAndDetailEntity.setRouteId(1L);
        // Search for the route and route detail's information.
        RouteAndDetail returnedRouteAndDetail = routeAndDetailService.selectRouteAndDetail(searchedRouteAndDetailEntity);
        // Read success.
        System.out.println("====== Result: " + returnedRouteAndDetail.toJson(returnedRouteAndDetail));

    }

    @Test
    public void gpxParser() {

        GPXParser gpxParser = new GPXParser();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("/Users/grazerma/Desktop/testGpx.gpx");
            GPX gpx = gpxParser.parseGPX(fileInputStream);

            Iterator<Waypoint> iterator = gpx.getWaypoints().iterator();
            if (iterator.hasNext()) {
                Waypoint waypoint = iterator.next();
                System.out.println("===waypoint: " + waypoint);
                System.out.println("===getElevation: " + waypoint.getElevation());
                System.out.println("===getGeoIdHeight: " + waypoint.getGeoIdHeight());
            }

            System.out.println("gpx: " + gpx);
            System.out.println("gpx.getVersion: " + gpx.getVersion());
            System.out.println("gpx.getCreator: " + gpx.getCreator());
            System.out.println("gpx.getRoutes: " + gpx.getRoutes());
            System.out.println("gpx.getTracks: " + gpx.getTracks());
            System.out.println("gpx.getWaypoints: " + gpx.getWaypoints());
            System.out.println("gpx.getXmlns: " + gpx.getXmlns());
            System.out.println("gpx.getExtensionData: " + gpx.getExtensionData());
            System.out.println("gpx.getExtensionsParsed: " + gpx.getExtensionsParsed());
            System.out.println("gpx.getClass: " + gpx.getClass());
            System.out.println("gpx.getMetadata: " + gpx.getMetadata());
            System.out.println("gpx.getMetadata().getName(): " + gpx.getMetadata().getName());
            System.out.println("gpx.getMetadata().getDesc(): " + gpx.getMetadata().getDesc());
            System.out.println("gpx.getMetadata().getKeywords(): " + gpx.getMetadata().getKeywords());
            System.out.println("gpx.getMetadata().getAuthor(): " + gpx.getMetadata().getAuthor());
            System.out.println("gpx.getMetadata().getTime(): " + gpx.getMetadata().getTime());
            System.out.println("gpx.getMetadata().getBounds(): " + gpx.getMetadata().getBounds());
            System.out.println("gpx.getMetadata().getCopyright(): " + gpx.getMetadata().getCopyright());
            System.out.println("gpx.getMetadata().getLinks(): " + gpx.getMetadata().getLinks());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    */
/*@Test
    public void sendSimpleMail() throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("club100@club100.cn");
        message.setTo("490982326@qq.com");
        message.setSubject("See you tomorrow.");
        message.setText("Happy day, Be better!");

        javaMailSender.send(message);
    }*//*


    */
/*@Test
    public void sendAttachmentsMail() throws Exception {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("club100@club100.cn");
        helper.setTo("490982326@qq.com");
        helper.setSubject("主题：有附件哦");
        helper.setText("有附件的邮件哈哈哈");
        URL url = new URL("https://images.club100.cn/matongqixing.gpx");
        helper.addAttachment("666.gpx", new ByteArrayResource(IOUtils.toByteArray(url.openStream())));

        javaMailSender.send(mimeMessage);

    }*//*


    @Test
    public void qiniu() {

        String token = qiniuService.getToken();
        System.out.println("token: " + token);

    }

    */
/**
     * 有参GET请求
     *//*

    @Test
    public void test2() {
        // 第一种 getForEntity中直接写参数
        // 参数在链接地址后 用数字标识先后位置
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid={1}&secret={2}&grant_type={3}&js_code={4}";
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class, "wxad24a51a97d67b92", "08ab3c84f258f787d0302c6194b0850e", "authorization_code", "qwer");
        log.info("响应内容1====>" + entity.getBody());

        */
/*//*
/第一种 用Map封装参数
        // 参数在链接地址后 用参数名标识位置
        String url1 = "http://localhost:9999/spring-boot-gx/testGet2?id={id}&userName={userName}";
        Map<String,Object> map = new HashMap<>();
        map.put("id",6);
        map.put("userName","波波烤鸭");
        ResponseEntity<String> entity1 = restTemplate.getForEntity(url1, String.class,map);
        log.info("响应内容2====>"+entity1.getBody());*//*



//        final String uri = "https://www.baidu.com";
//        RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
//
//        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
//
//        System.out.println(result);
    }

    @Test
    public void selectRouteAndDetail2() {

        RouteAndDetail routeAndDetailEntity = new RouteAndDetail();
        routeAndDetailEntity.setRouteId(1L);
        RouteAndDetail routeAndDetail = routeAndDetailService.selectRouteAndDetail(routeAndDetailEntity);
        System.out.println("routeAndDetail: " + routeAndDetail);

    }

    @Test
    public void testJson() {

        String result = "{\"openId\":\"oR0o54zMYKHAw2hP_JR4XgF6K5Aw\",\"nickName\":\"米高高高高高\",\"gender\":0,\"language\":\"zh_CN\",\"city\":\"\",\"province\":\"\",\"country\":\"\",\"avatarUrl\":\"https://wx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLALQpBssScVia2x4OXJ9H4IyGmicq7fUaCmiaOAL8rC1SGiahLVpmUQA2gFjW1s4FJAYR0gbENl3zlraA/132\",\"watermark\":{\"timestamp\":1597245863,\"appid\":\"wxad24a51a97d67b92\"}}";
        JSONObject jsonObject = JSONObject.parseObject(result);
        System.out.println(jsonObject.getString("openId"));

    }

    @Test
    public void insertUser() {

        long returnedUserId = PrimaryKeyIdUtils.S_SimpleLongAutoGenerator();
        System.out.println("returnedUserId: " + returnedUserId);
        String result = "{\"openId\":\"oR0o54zMYKHAw2hP_JR4XgF6K5Aw\",\"nickName\":\"米高高高高高\",\"gender\":0,\"language\":\"zh_CN\",\"city\":\"\",\"province\":\"\",\"country\":\"\",\"avatarUrl\":\"https://wx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLALQpBssScVia2x4OXJ9H4IyGmicq7fUaCmiaOAL8rC1SGiahLVpmUQA2gFjW1s4FJAYR0gbENl3zlraA/132\",\"watermark\":{\"timestamp\":1597245863,\"appid\":\"wxad24a51a97d67b92\"}}";
        JSONObject userInfoJSON = JSONObject.parseObject(result);
        User insertUserEntity = new User();
        insertUserEntity.setUserId(returnedUserId);
        insertUserEntity.setUserCity(1);
        insertUserEntity.setMarkTimes(0);
        insertUserEntity.setState(1);
        insertUserEntity.setWxUserCity(userInfoJSON.getString("city"));
        insertUserEntity.setWxOpenId(userInfoJSON.getString("openId"));
        insertUserEntity.setWxUnionId(userInfoJSON.getString("unionId"));
        insertUserEntity.setWxUserAvatarUrl(userInfoJSON.getString("avatarUrl"));
        insertUserEntity.setWxUserCountry(userInfoJSON.getString("country"));
        insertUserEntity.setWxUserGender(userInfoJSON.getInteger("gender"));
        insertUserEntity.setWxUserProvince(userInfoJSON.getString("province"));
        insertUserEntity.setWxNickName(userInfoJSON.getString("nickName"));
        // Insert
        int rows = userService.insert(insertUserEntity);
        System.out.println("===Rows: " + rows);

    }

    @Test
    public void checkUser() {

        User searchedUserEntity = new User();
        searchedUserEntity.setWxOpenId("oR0o54zMYKHAw2hP_JR4XgF6K5Aw666");
        User ifExistUserEntity = userService.selectUser(searchedUserEntity);

        System.out.println("~~~ifExistUserEntity: " + ifExistUserEntity);

        if (EmptyUtils.isNotEmpty(ifExistUserEntity)) System.out.println("aaaaaaaa");



    }

    @Test
    public void testMap() {

        Map<Integer, String> picsMap = new HashMap<>();
        picsMap.put(1, "1234");
        picsMap.put(2, "4321");

        Long routeId = 30L;
        Long userId = 5L;

        // Initialization.
        MarkRoute searchedMarkRouteEntity = new MarkRoute();
        // Assign variables.
        searchedMarkRouteEntity.setRouteId(routeId);
        searchedMarkRouteEntity.setUserId(userId);
        // Search if the route is marked or not.
        int selectCountInt = markRouteService.selectMarkRouteCountList(searchedMarkRouteEntity);

    }


}
*/
