package com.club100.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author zhangqiang
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
