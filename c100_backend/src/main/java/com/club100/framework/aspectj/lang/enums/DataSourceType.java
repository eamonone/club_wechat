package com.club100.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author zhangqiang
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
