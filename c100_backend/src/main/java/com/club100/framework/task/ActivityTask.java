package com.club100.framework.task;

import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.service.IActivityService;
import com.club100.project.wechat.utils.EmptyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/09 14:58:20
 * @Description: 活动定时任务。
 */
@Component
@Slf4j
public class ActivityTask {

    // Activity information.
    @Autowired
    private IActivityService activityService;

    @Scheduled(cron = "0 0 0/1 * * ?") // 1 hour.
//    @Scheduled(cron = "0/5 * * * * ?") // 5 seconds.
    public void modifyActivityTime() {

        List<Activity> activitiesList = activityService.selectActivityByBeginTime();
        if (0 != activitiesList.size()) {
            activitiesList.forEach(eachActivityEntity -> {
                eachActivityEntity.setActivityBeginTime(eachActivityEntity.getActivityBeginTime().plusDays(7));
                eachActivityEntity.setActivityEndTime(eachActivityEntity.getActivityEndTime().plusDays(7));
                activityService.updateActivity(eachActivityEntity);
            });
        }

    }

}
