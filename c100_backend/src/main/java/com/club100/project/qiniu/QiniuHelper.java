package com.club100.project.qiniu;

import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.model.BatchStatus;
import com.qiniu.util.Auth;
import com.club100.common.exception.CustomException;
import lombok.extern.slf4j.Slf4j;

/**
 * @auther: zhangqiang
 * @date: 2019/3/26 12:06
 * @description:
 */
@Slf4j
public class QiniuHelper {
  /**
   * @param accessKey
   * @param secretKey
   * @param bucketname
   * @param keyList 要删除的key集合
   * @return
   */
  public static Integer batchDelete(
      String accessKey, String secretKey, String bucketname, String[] keyList) {
    Configuration cfg = new Configuration(Zone.zone0());

    Auth auth = Auth.create(accessKey, secretKey);
    BucketManager bucketManager = new BucketManager(auth, cfg);

    BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
    // 单次批量请求的文件数量不得超过1000
    if (keyList.length > 1000) {
      throw new CustomException("单次批量请求的文件数量不得超过1000");
    }
    batchOperations.addDeleteOp(bucketname, keyList);
    Response response;
    try {
      response = bucketManager.batch(batchOperations);
      BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
      int errCount = 0;
      for (int i = 0; i < keyList.length; i++) {
        BatchStatus status = batchStatusList[i];
        String key = keyList[i];
        if (status.code != 200) {
          log.info(key + "\t" + status.data.error);
          errCount++;
        }
      }
      return errCount;

    } catch (QiniuException e) {
      log.error(e.getMessage());
    }
    return 0;
  }
}
