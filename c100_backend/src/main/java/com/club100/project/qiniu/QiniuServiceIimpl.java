package com.club100.project.qiniu;

import com.alibaba.fastjson.JSON;
import com.club100.common.core.lang.UUID;
import com.club100.project.wechat.utils.PrimaryKeyIdUtils;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @auther: zhangqiang
 * @date: 2019/2/21 11:21
 * @description:
 */
@Service
@Slf4j
public class QiniuServiceIimpl implements QiniuService {

    // 要上传的空间
    @Value("${qiniu.space}")
    private String bucketname;

    @Value("${qiniu.access_key}")
    private String accessKey;

    @Value("${qiniu.secret_key}")
    private String secretKey;

    @Override
    public String getToken() {
        Auth auth = Auth.create(accessKey, secretKey);

        StringMap strMap = new StringMap();
        strMap.putNotEmpty(
                "returnBody",
                "{\"exif\":$(exif),\"name\":$(fname), \"key\": $(key), \"hash\": $(etag), \"width\": $(imageInfo.width), \"height\": $(imageInfo.height), \"ext\":${imageInfo.format}, \"fsize\":${fsize}}");
        strMap.putNotNull("callbackFetchKey", 1);
        String token = auth.uploadToken(bucketname, null, 3600, strMap);
        return token;
    }

    @Async
    @Override
    public void batchDelete(Long id, String[] keyList) {
        int errCount = QiniuHelper.batchDelete(accessKey, secretKey, bucketname, keyList);
        if (errCount == 0) {
            log.info("已成功删除相册{}在七牛云的资源{}", id, keyList);
        }
    }

    /**
     * @param localFilePath
     * @return
     */
    @Override
    public String upload(String localFilePath) {
        Auth auth = Auth.create(accessKey, secretKey);
//    String key = null;
        String key = localFilePath;
        String upToken = auth.uploadToken(bucketname);

        // 构造一个带指定 Region 对象的配置类
//    Configuration cfg = new Configuration(Region.region0());
        Configuration cfg = new Configuration(Region.region2()); // 华南地区
        UploadManager uploadManager = new UploadManager(cfg);
        try {
            Response response = uploadManager.put(localFilePath, key, upToken);
            // 解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
            //      System.out.println(putRet.key);
            //      System.out.println(putRet.hash);
            return putRet.key;
        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            log.error("{}", ex);
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                log.error("{}", ex2);
            }
            return "";
        }
    }

    @Override
    public String upload(MultipartFile multipartFile) {
        String img = "";
        try {
            //随机化文件名
            String fileName = PrimaryKeyIdUtils.S_uuidWithoutHyphenAutoGenerator() + ".png";
            //构造一个带指定Zone对象的配置类
            //指定上传文件服务器地址：
            Configuration cfg = new Configuration(Region.region2()); // 华南地区
            //...其他参数参考类注释
            //上传管理器
            UploadManager uploadManager = new UploadManager(cfg);
            //身份认证
            Auth auth = Auth.create(accessKey, secretKey);
            //指定覆盖上传
            String upToken = auth.uploadToken(bucketname, fileName);
            //上传
            Response response = uploadManager.put(multipartFile.getBytes(), fileName, upToken);
            // 解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
            img = putRet.key;
        } catch (QiniuException ex) {
            System.err.println(ex.getMessage());
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }
}
