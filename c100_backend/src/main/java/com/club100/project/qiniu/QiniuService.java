package com.club100.project.qiniu;

import org.springframework.web.multipart.MultipartFile;

/**
 * @auther: zhangqiang
 * @date: 2019/2/21 11:13
 * @description:
 */
public interface QiniuService {
  String getToken();

  /**
   * 批量删除七牛文件
   *
   * @param id
   * @param keys
   */
  void batchDelete(Long id, String[] keys);

  String upload(String localFilePath);

  String upload(MultipartFile multipartFile);
}
