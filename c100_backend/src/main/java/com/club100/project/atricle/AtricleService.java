package com.club100.project.atricle;

import com.club100.project.atricle.domain.Atricle;
import com.club100.project.atricle.domain.AtricleQueryRequest;
import com.club100.project.atricle.domain.AtricleVo;

import java.util.List;

/**
 * @auther: zhangqiang
 * @date: 2020/6/21 14:56
 * @description:
 */
public interface AtricleService {
  /**
   * 发布内容
   *
   * @param atricle
   * @return
   */
  int insert(Atricle atricle);

  List<Atricle> selectArticleList(AtricleQueryRequest atricleQueryRequest, Integer preference, Integer videoTime1Begin, Integer videoTime1End, Integer videoTime2Begin, Integer videoTime2End, Integer videoTime3Begin, Integer videoTime3End, Integer videoTime4Begin, Integer videoTime4End, Integer[] videoType);

  List<Atricle> page(AtricleQueryRequest atricleQueryRequest);

  int deleteById(Long... id);

  AtricleVo selectArticleById(Long id);

  int updateArticle(Atricle atricle);
}
