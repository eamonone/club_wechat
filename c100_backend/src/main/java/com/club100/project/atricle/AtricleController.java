package com.club100.project.atricle;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.atricle.domain.Atricle;
import com.club100.project.atricle.domain.AtricleQueryRequest;
import com.club100.project.atricle.domain.AtricleVo;
import com.club100.project.wechat.utils.EmptyUtils;
import com.club100.project.wechat.utils.ReturnUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Objects;

/**
 * 文章内容管理器
 *
 * @auther: zhangqiang
 * @date: 2020/6/21 14:55
 * @description:
 */
@RequestMapping("/v1/atricle")
@RestController
public class AtricleController extends BaseController {
    @Resource
    private AtricleService atricleService;

    @PostMapping
    public AjaxResult insert(@RequestBody Atricle atricle) {
        atricle = P_ModifyTheContent(atricle);
        atricleService.insert(atricle);
        return AjaxResult.success();
    }

    @GetMapping
    public TableDataInfo page(AtricleQueryRequest atricleQueryRequest) {
        startPage();

        return getDataTable(atricleService.page(atricleQueryRequest));
    }

    @RequestMapping(value = "/selectArticle")
    @ResponseBody
    public TableDataInfo selectArticleList(AtricleQueryRequest atricleQueryRequest, Integer preference, Integer[] videoTime, Integer[] videoType) {

        // Judge if the parameter is empty.
        if (EmptyUtils.isEmpty(preference)) preference = 1;
        if (EmptyUtils.isEmpty(videoTime)) videoTime = new Integer[]{0};
        if (EmptyUtils.isEmpty(videoType)) videoType = new Integer[]{0};

        Integer videoTime1Begin = null;
        Integer videoTime1End = null;
        Integer videoTime2Begin = null;
        Integer videoTime2End = null;
        Integer videoTime3Begin = null;
        Integer videoTime3End = null;
        Integer videoTime4Begin = null;
        Integer videoTime4End = null;

        if (Arrays.asList(videoTime).contains(1)) {
            videoTime1Begin = 120;
            videoTime1End = 300;
        }
        if (Arrays.asList(videoTime).contains(2)) {
            videoTime2Begin = 300;
            videoTime2End = 420;
        }
        if (Arrays.asList(videoTime).contains(3)) {
            videoTime3Begin = 420;
            videoTime3End = 600;
        }
        if (Arrays.asList(videoTime).contains(4)) {
            videoTime4Begin = 600;
            videoTime4End = 36001;
        }

        startPage();

        return getDataTable(atricleService.selectArticleList(atricleQueryRequest, preference, videoTime1Begin, videoTime1End, videoTime2Begin, videoTime2End, videoTime3Begin, videoTime3End, videoTime4Begin, videoTime4End, videoType));

    }

    @GetMapping("mp")
    public AjaxResult mpPage(AtricleQueryRequest atricleQueryRequest) {

        startPage();

        Long atricleIdLong = atricleQueryRequest.getAtricleId();
        AtricleVo atricleVo = atricleService.selectArticleById(atricleIdLong);

        Atricle searchedArticleEntity = new Atricle();
        searchedArticleEntity.setAtricleId(atricleIdLong);
        searchedArticleEntity.setUserClickCount((atricleVo.getUserClickCount() + 1));

        atricleService.updateArticle(searchedArticleEntity);

        return AjaxResult.success(getPageTable(atricleService.page(atricleQueryRequest)));

    }

    /*@GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable Long id) {
        return AjaxResult.success(postService.selectPostById(id));
    }*/

    @DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(atricleService.deleteById(id));
    }

    @RequestMapping("selectArticleById")
    public AtricleVo selectArticleById(Long id) {

        return atricleService.selectArticleById(id);

    }

    @RequestMapping("/updateArticle")
    public int updateArticle(@RequestBody Atricle atricle) {

        return atricleService.updateArticle(atricle);

    }

    /**
     * Modify the content.
     *
     * @param modifiedArticleEntity
     * @return
     */
    private Atricle P_ModifyTheContent(Atricle modifiedArticleEntity) {

        // Initialize.
        String headerString = "<section class=\"_135editor\" style=\"margin: 0px; padding: 0px; max-width: 100%; overflow-wrap: break-word !important; box-sizing: border-box !important;\">\n<p style=\"margin: 0px; padding: 0px; clear: both; max-width: 100%; overflow-wrap: break-word !important; box-sizing: border-box !important; min-height: 1em;\">&nbsp;</p>\n";
        String emptyLineString = "\n<p style=\"margin: 0px; padding: 0px; clear: both; max-width: 100%; overflow-wrap: break-word !important; box-sizing: border-box !important; min-height: 1em; color: #000000; font-family: PingFangSC-ultralight; font-size: 12px; letter-spacing: 2px; text-align: justify;\" align=\"justify\">&nbsp;</p>\n";
        String beforeContent = "<p style=\"margin: 0px; padding: 0px; clear: both; max-width: 100%; overflow-wrap: break-word !important; box-sizing: border-box !important; min-height: 1em; color: #000000; font-family: PingFangSC-ultralight; font-size: 12px; letter-spacing: 2px; text-align: justify;\" align=\"justify\"><span style=\"margin: 0px; padding: 0px; max-width: 100%; font-size: 14px; font-family: 微软雅黑, 'Microsoft YaHei'; overflow-wrap: break-word !important; box-sizing: border-box !important;\">";
        String afterContent = "</span></p>";
        String footerString = "\n</section>\n<p style=\"line-height: 2em; letter-spacing: 2px;\">&nbsp;</p>\n<section class=\"_135editor\" data-role=\"paragraph\">\n<p>&nbsp; &nbsp;</p>\n</section>";

        StringBuilder stringBuilderNewContent = new StringBuilder();

        String modifiedContentString = modifiedArticleEntity.getContent().replace("&", "&amp;").replace("“", "&ldquo;").replace("”", "&rdquo;");
        String[] contents = modifiedContentString.split("\n");

        stringBuilderNewContent.append(headerString);

        for (int i = 0; i < contents.length; i++) {
            StringBuilder stringBuilderContent = new StringBuilder();
            stringBuilderContent.append(beforeContent).append(contents[i]).append(afterContent);
            if (1 == (contents.length - i)) {
                stringBuilderNewContent.append(stringBuilderContent);
            } else {
                stringBuilderNewContent.append(stringBuilderContent).append(emptyLineString);
            }
        }

        stringBuilderNewContent.append(footerString);

        modifiedArticleEntity.setContent(String.valueOf(stringBuilderNewContent));

        return modifiedArticleEntity;

    }

}

