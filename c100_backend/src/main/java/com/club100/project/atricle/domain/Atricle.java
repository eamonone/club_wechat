package com.club100.project.atricle.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @auther: zhangqiang
 * @date: 2020/6/21 14:57
 * @description: 富文本编辑器发布的内容POJO
 */
@Setter
@Getter
public class Atricle extends BaseEntity implements Serializable {
  private Integer userClickCount;
  private Integer videoSecondTime;
  private Integer videoType;
  private Long atricleId;
  private String title;
  private String content;
  private String articleThumbnail;
  private String shareThumbnail;
  private String vid;
  private String vidLocation;
}
