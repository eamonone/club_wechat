package com.club100.project.atricle.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 内容查询请求视图
 *
 * @auther: zhangqiang
 * @date: 2020/6/21 16:21
 * @description:
 */
@Setter
@Getter
public class AtricleQueryRequest {
  private String title;
  private Long atricleId;
}
