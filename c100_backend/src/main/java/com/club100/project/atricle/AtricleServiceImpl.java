package com.club100.project.atricle;

import com.club100.common.utils.SecurityUtils;
import com.club100.project.atricle.domain.Atricle;
import com.club100.project.atricle.domain.AtricleQueryRequest;
import com.club100.project.atricle.domain.AtricleVo;
import com.club100.project.atricle.mapper.AtricleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @auther: zhangqiang
 * @date: 2020/6/21 15:03
 * @description:
 */
@Slf4j
@Service
public class AtricleServiceImpl implements AtricleService {
    @Resource
    private AtricleMapper atricleMapper;

    @Override
    @Transactional
    public int insert(Atricle atricle) {
        atricle.setCreateBy(SecurityUtils.getUsername());
        atricle.setCreateTime(new Date());
        return atricleMapper.insertAtricle(atricle);
    }

    @Override
    public List<Atricle> selectArticleList(AtricleQueryRequest atricleQueryRequest, Integer preference, Integer videoTime1Begin, Integer videoTime1End, Integer videoTime2Begin, Integer videoTime2End, Integer videoTime3Begin, Integer videoTime3End, Integer videoTime4Begin, Integer videoTime4End, Integer[] videoType) {
        return atricleMapper.selectArticleList(atricleQueryRequest, preference, videoTime1Begin, videoTime1End, videoTime2Begin, videoTime2End, videoTime3Begin, videoTime3End, videoTime4Begin, videoTime4End, videoType);
    }

    @Override
    public List<Atricle> page(AtricleQueryRequest atricleQueryRequest) {
        return atricleMapper.selectAtricleforList(atricleQueryRequest);
    }

    @Override
    @Transactional
    public int deleteById(Long... id) {
        return atricleMapper.delteById(id);
    }

    @Override
    public AtricleVo selectArticleById(Long id) {
        return atricleMapper.selectArticleById(id);
    }

    @Override
    public int updateArticle(Atricle atricle) {
        return atricleMapper.updateArticle(atricle);
    }

}
