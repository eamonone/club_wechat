package com.club100.project.insurance.sdk;

import com.club100.project.wechat.utils.MyOkHttpClient;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

/**
 * @description: 重写post
 * @author: Mr.Wang
 * @create: 2020-07-31 11:13
 **/
public class MyPost {

  public static String postBodyWithHeader(String url, String body, String contentType) {
    try {
      MediaType mediaType = MediaType.parse(contentType);
      Request request = (new Request.Builder()).url(url).post(RequestBody.create(mediaType, body)).build();
      Response response = MyOkHttpClient.client.newCall(request).execute();
      if (!response.isSuccessful()) {
      } else {
        return response.body().string();
      }
    } catch (IOException var7) {
      var7.printStackTrace();
    }
    return "";
  }
}
