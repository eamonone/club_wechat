package com.club100.project.insurance.sdk;


import okhttp3.*;

import java.io.IOException;

/**
 * @description: 重写put
 * @author: Mr.Wang
 * @create: 2020-07-31 11:13
 **/
public class MyPut {

  public static String PutBodyWithHeader(String url, String body, String contentType, String accessToken)  {
    try {
      MediaType mediaType = MediaType.parse(contentType);
      Request request = (new Request.Builder()).url(url).put(RequestBody.create(mediaType, body)).addHeader("access-token", accessToken).build();
      OkHttpClient okHttpClient = new OkHttpClient();
      Response response = okHttpClient.newCall(request).execute();
      if (!response.isSuccessful()) {
      } else {
        return response.body().string();
      }
    } catch (IOException var7) {

    }
    return "";
  }
}
