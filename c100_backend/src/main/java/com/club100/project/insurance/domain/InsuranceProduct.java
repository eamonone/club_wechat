package com.club100.project.insurance.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * insurance_product
 * @author 
 */
@Data
public class InsuranceProduct implements Serializable {
    /**
     * 主键 ID。
     */
    private Long id;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 计划名称
     */
    private String planName;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 产品类别
     */
    private String productType;

    /**
     * 产品Id
     */
    private String productId;

    /**
     * 计划Id
     */
    private String caseId;

    /**
     * 方案代码
     */
    private String caseNo;

    /**
     * 创建时间。
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}