package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements Serializable {



  String    insureNum;   //投保单号
  String    caseCode;   //方案代码
  String    productName;   //产品名称
  String    planName;   //计划名称
  String    partnerUniqueKey;   //第三方用户信息(投保有传才会有)
  int    totalNum;   //总份数
  long    payAmount;   //保单总保费（单位：分）
  String    companyName;   //保险公司名称
  String    startDate;   //起保日期 格式：yyyy-MM-dd HH:mm:ss
  String    endDate;   //终保日期 格式：yyyy-MM-dd HH:mm:ss
  String    deadline;   //保险期限
  String    deadlineText;   //保障期限说明
  int    issueStatus;   //出单状态 0：未出单 1：已出单 2：延时出单 3：取消出单 4：出单失败
  int    effectiveStatus;   //生效（退保）状态 0：未生效 1：已生效 2：退保中 3：已退保 4：支付成功核保异常
  int    payStatus;   // 支付状态 0：未支付 1：已支付 2：不能支付 3：扣款中 4：扣款失败 5：扣款成功
  String    categoryName;   //产品二级分类名称
  String  insureTime;   //投保时间 格式：yyyy-MM-dd HH:mm:ss


 String payedTime;
 int gateway;
 Applicant applicant;
 String priceArgs;
 List<Insurant> insurants;
 List<EnsureProject> projects;
 Other other;

}
