package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnlinePayReq {
  //5.在线支付 请求

  private String     transNo;        //必填	交易流水号，生成规则参考名词解释
  private int     partnerId;        //必填	开发者身份标识，获取方式参考名词解释
//  private int     subPartnerId;        //	开发者子标识，多层级账户使用
  private String     insureNums;        //必填	投保单号，多个单号使用英文逗号“,”分隔
  private long     money;        //必填	订单支付总金额（单位：分）
  private int     gateway;        //必填	支付类型 1：支付宝 3：银联  21:微信 ,24保险公司插件支付（部分产品支持）
//  private int     bankId;        //	当通过支付宝调起银联支付时必填
  private int     clientType;        //必填	客户端类型1：PC，2：H5
  private String     productUrl;        //	产品详情跳转地址（支付宝PC端跳转使用）
  private String     callBackUrl;        //必填	支付成功回调（跳转）地址 微信中如果中途退出，回调地址会拼接result=cancel返回；如果支付成功，回调地址会拼接result=ok可通过该值确认跳转到支付成功/失败界面 支付宝中支付成功直接跳转到该页面
}
