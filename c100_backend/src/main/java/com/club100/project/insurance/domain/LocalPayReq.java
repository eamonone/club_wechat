package com.club100.project.insurance.domain;

import lombok.Data;

@Data
public class LocalPayReq {


  /**
   * transNo : huize20171027100140000004
   * partnerId : 1000037
   * money : 48600
   * insureNums : 20170412002698
   */

  private String transNo;
  private int partnerId;
  private Long money;
  private String insureNums;
}
