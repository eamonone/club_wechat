package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EnsureProject implements Serializable {

  private String projectName;   //保障项目名称
  private String sumInsured;   //保额
  private String unitText;   //保额单位
  private String startDate;   //保障开始时间 格式：yyyy-MM-dd HH:mm:ss
  private String endDate;   //保障结束时间格式：yyyy-MM-dd HH:mm:ss
  private String insuredText;   //保障内容说明
  private String valid;   //高保额生效状态 0：未生效 1：生效
}
