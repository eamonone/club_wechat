package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product1 {
  //产品列表返回

 private String    caseCode;  //方案代码
 private String    prodName;  //产品名称
 private String    planName;  //计划名称
 private String    companyName;  //保险公司名称
 private int    offShelf;  //是否下架 0：否 1：是
 private int    fristCategory;  //一级分类，取值参考附录5
 private int    secondCategory;  //二级分类，取值参考附录5
}
