package com.club100.project.insurance.mapper;

import com.club100.project.insurance.domain.InsuranceOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface InsuranceOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InsuranceOrder record);

    int insertSelective(InsuranceOrder record);

    InsuranceOrder selectByPrimaryKey(Long id);

    @Select("select * from insurance_order where out_trade_no = #{outTradeNo} limit 1")
    @ResultMap("BaseResultMap")
    InsuranceOrder selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);

  /**
   * 通过用户姓名、份证号、活动id查询在当前活动的入保信息。包括未支付和已经退保
   *
   * @param userName
   * @param cardCode
   * @return
   */
    @Select("select * from insurance_order where user_name = #{userName} and card_code = #{cardCode} and activity_id = #{activityId} and status != 2 limit 1")
    @ResultMap("BaseResultMap")
    InsuranceOrder selectByUserNameAndCardCode(@Param("userName") String userName,@Param("cardCode") String cardCode,@Param("activityId") Long activityId);

    int updateByPrimaryKeySelective(InsuranceOrder record);

    int updateByPrimaryKey(InsuranceOrder record);
}