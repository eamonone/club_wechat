package com.club100.project.insurance.baodan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notify implements Serializable {

  private int notifyType; //通知类型 保单生成：9
  private String sign; //签名，MD5（key+data）
  private ProducePolicyNotify IssueNotify; //保单生成通知信息
  private String test;
}
