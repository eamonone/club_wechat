package com.club100.project.insurance.domain;

import com.club100.project.wechat.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetailResp {
  //产品详情

  private String  transNo;    //交易流水号，生成规则参考名词解释

  private int partnerId;    //开发者身份标识，获取方式参考名词解释

  private List<Product> products;    //产品列表

}
