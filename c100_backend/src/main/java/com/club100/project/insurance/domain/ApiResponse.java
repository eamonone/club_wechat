package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
 //用于校验订单是否需要认证

  private int respCode;   //响应码
  private String respMsg;   //响应消息
  private TApproveTaskDto data;   //响应数据对象
}
