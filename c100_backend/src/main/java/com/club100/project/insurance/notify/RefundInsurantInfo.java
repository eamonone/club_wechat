package com.club100.project.insurance.notify;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RefundInsurantInfo implements Serializable {

  Integer  cardTypeId;   //证件类
  String  cardNumber;   //证件
  String  cName;   //被保人中文
  Long  refundPremium;   //被保人退保保费(单位分
  Long  buyPremium;   //被保人购买支付保费(单位分
  Integer  buyCount;   //购买份数
}
