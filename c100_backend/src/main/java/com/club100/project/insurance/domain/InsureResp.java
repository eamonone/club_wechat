package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsureResp {

  private String  transNo;    //交易流水号，生成规则参考名词解释

  private int partnerId;    //开发者身份标识，获取方式参考名词解释

  private String insureNum;    //投保单号

  private String startDate;    //起保日期 格式：yyyy-MM-dd

  private String endDate;    //终保日期 格式：yyyy-MM-dd

  private String cpsInsureUrl;    //生成订单成功跳转cps页面投保地址

  private int needAuth; //是否需要认证（1需要，0不需要）
}
