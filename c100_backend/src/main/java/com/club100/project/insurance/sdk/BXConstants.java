package com.club100.project.insurance.sdk;

/**
 * 常量
 */
public class BXConstants {

  //测试环境 核心服务 普通服务
  public static final String TEST_CORE = "https://testapi.xiebao18.com/api/";
  public static final String TEST_NORMAL = "https://testapi.xiebao18.com/border/";

  //生产环境 核心服务 普通服务
  public static final String PRO_CORE = "https://api.xiebao18.com/api/";
  public static final String PRO_NORMAL = "https://api.xiebao18.com/border/";

  // 开发者身份标识
  public static final String PARTNER_ID = "2070894";
  // test、pro 签名Key
  public static final String TEST_SIGN_KEY = "TNMzAwZmFmOTQ3NzR2070894";
  public static final String PRO_SIGN_KEY = "ONOTI3OTZkMzVlOWN2070894";




    //试算
    public static final String SIMPLE_TRIAL = "simpleTrial";
    //承保
    public static final String SIMPLE_INSURE = "simpleInsure";
    //本地支付
    public static final String LOCAL_PAY = "localPay";
    //在线支付
    public static final String ONLINE_PAY = "onlinePay";

    //保单地址
    public static final String BD_DOWNLOAD_URL = "downloadUrl";
    //退保
    public static final String SURRENDER = "surrenderPolicy";

    //产品列表
    public static final String PRODUCT_LIST = "productList";
    public static final String PRODUCT_DETAIL = "productDetail";

    //投保属性
    public static final String PRODUCT_INSURED_Attr = "productInsuredAttr";

    //投保单查询
    public static final String ORDER_DETAIL = "orderDetail";

    //保单下载
    public static final String POLICY_DOWNLOAD = "policyDownload";
    //居住城市
    public static final String PRODUCT_INSURED_AREA = "productInsuredArea";
    //职业信息
    public static final String PRODUCT_INSURED_JOB = "productInsuredJob";
    //出行目的地
    public static final String PRODUCT_DESTINATION = "productDestination";
    //财产地址
    public static final String PROPERTY_ADDRESS = "propertyAddress";
    //认证校验
    public static final String AUTH_INFO = "authInfo";
}

