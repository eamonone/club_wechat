package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PolicyUrlReq {
  //6.获取保单文件下载地址

  private String     transNo;        //必填	交易流水号，生成规则参考名词解释
  private int     partnerId;        //必填	开发者身份标识，获取方式参考名词解释
  private int     subPartnerId;        //	开发者子标识，多层级账户使用
  private String     insureNum;        //必填	投保单号，多个单号使用英文逗号“,”分隔
}
