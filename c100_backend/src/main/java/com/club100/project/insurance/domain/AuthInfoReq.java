package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthInfoReq {
 //用于校验订单是否需要认证

  private String transNo;    //必填	交易流水号，生成规则参考名词解释
  private int partnerId;    //必填	开发者身份标识，获取方式参考名词解释
  long insureNum;     //必填	投保订单号
  String callbackUrl;     //必填	流程操作完成后跳转渠道页面地址
  int directPay;     //非必填	页面流程完成后1走携保支付，0渠道自己支付。默认1
  int subPartnerId;     //非必填	如果投保时传了值此接口也要传对应
}
