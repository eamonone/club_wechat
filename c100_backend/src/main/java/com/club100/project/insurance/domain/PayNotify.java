package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PayNotify implements Serializable {

  int  partnerId; //开发者身份标识
  String  partnerUniqueKey; //开发者用户标识
  String  caseCode; //方案代码
  String  insureNum; //投保单号
  boolean  state;  //是否支付成功 true：成功 false：失败
  String  payTime;  //支付时间，格式：yyyy-MM-dd HH:mm:ss
  long  price;  //支付金额（单位：分)
  String  onlinePaymentId;  // 在线支付网关标识 1：支付宝 3：银联 14：财付通 21:微信 -11：银行代扣
  String  failMsg;  // 失败原因（银行代扣失败会返回失败原因）
  Map<String,Object> otherInfo;  //其他信息
}
