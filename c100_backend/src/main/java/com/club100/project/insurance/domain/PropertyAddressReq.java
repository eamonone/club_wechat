package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyAddressReq {
 //用于获取财产地址信息（二级和三级地区需根据上一级地区编码再次调用此接口获得）

  private String transNo;    //必填	交易流水号，生成规则参考名词解释
  private int partnerId;    //必填	开发者身份标识，获取方式参考名词解释
  private String caseCode;    //必填	方案代码，获取方式参考名词解释
  private String areaCode;    //地区编码（用于查询下一级地区，顶级默认空）
}
