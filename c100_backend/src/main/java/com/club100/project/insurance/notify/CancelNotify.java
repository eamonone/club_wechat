package com.club100.project.insurance.notify;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CancelNotify implements Serializable {

  int  partnerId;  //开发者身份标识
  String partnerUniqueKey;  //开发者用户标识
  String caseCode;  //方案代码
  String insureNum;  //投保单号
  boolean state;  //是否退保/退保重出成功  true：成功 false：失败
  String newInsureNum;  //退保重出新单号（退保重出时返回）
  List<String> cancelInsurants;  //退保被保人列表（按被保人退保时返回）
  String cancelMsg;  //退保原因（暂未返回）
  String failMsg;  //失败原因
  Map<String,Object> otherInfo;  //其他信息
  List<RefundInsurantInfo> refundInsurantInfos; // 退保被保人信息（v1.4版本部分退保通知新增消息字段）
  Long remainingTotalPremium;//剩余总保费（总实际支付减去已退保退款的保费，包括前几次退保的，是当前保单最终保费情况）

}
