package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailReq implements Serializable {

 private  String   transNom;  //必填	交易流水号，生成规则参考名词解释

  private  int   partnerIdm;  //必填	开发者身份标识，获取方式参考名词解释

  private int   subPartnerIdm;  //	开发者子标识，多层级账户使用

  private String   insureNum;  //必填	投保单号

}
