package com.club100.project.insurance.mapper;

import com.club100.project.insurance.domain.InsuranceProduct;

public interface InsuranceProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(InsuranceProduct record);

    int insertSelective(InsuranceProduct record);

    InsuranceProduct selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(InsuranceProduct record);

    int updateByPrimaryKey(InsuranceProduct record);
}