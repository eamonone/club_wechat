package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductAttribute {

  String    name; //属性名称
  String    apiName; //api接口请求参数名
  int    type; // 属性类型 类型 0：下拉框 1：日历 2：日历+下拉框 3：文本输入框 4：地区 5：职业 6：密码框 7：文本 8：对话框 9：单选框 10：复选框
  String    regex; // 属性校验正则表达式
  String    defaultRemind; //默认提醒信息
  String    errorRemind; //出错提醒信息
  int    required; //是否必填 0：否 1：是
  List<AttributeValue> attributeValues;  //属性值列表
  List<AttributeRestrict> attributeRestricts; //属性约束列表
}
