package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDestination {

  private String cName;  //中文名称
  private String eName;  //英文名称
  private int continentId;  //洲id
  private  String continentName;  //洲名称
  private  String initial;  //国家拼音首字母索引
}
