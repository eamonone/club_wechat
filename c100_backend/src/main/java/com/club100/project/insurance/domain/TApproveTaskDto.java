package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TApproveTaskDto implements Serializable {

  long  insureNum;  //投保单号

  int  needAuth;  //是否需要认证（1是，0否）

  String  authUrl;  //认证页面地址（需要认证时对应认证页面地址）
}
