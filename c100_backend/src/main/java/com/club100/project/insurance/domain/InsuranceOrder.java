package com.club100.project.insurance.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * insurance_order
 * @author
 */
@Data
public class InsuranceOrder implements Serializable {
  /**
   * 主键 ID。
   */
  private Long id;

  /**
   * 活动 ID。
   */
  private Long activityId;

  /**
   * 订单号
   */
  private String outTradeNo;

  /**
   * 方案代码
   */
  private String caseCode;

  /**
   * 被保险人姓名
   */
  private String userName;

  /**
   * 性别（0：女 1：男）
   */
  private Integer userSex;

  /**
   * 出生日期
   */
  private String birthday;

  /**
   * 证件号
   */
  private String cardCode;

  /**
   * 手机号
   */
  private String mobile;

  /**
   * 起保日期
   */
  private String startDate;

  /**
   * 终保日期
   */
  private String endDate;

  /**
   * 投保单号
   */
  private String insureNum;

  /**
   * 保单价格
   */
  private Long singlePrice;

  /**
   * 保单状态（0：未支付 1：已经支付 2：退保）
   */
  private Integer status;

  /**
   * 响应信息
   */
  private String response;

  /**
   * 创建时间。
   */
  private Date createTime;

  /**
   * 修改时间
   */
  private Date updateTime;

  private static final long serialVersionUID = 1L;
}