package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailResp implements Serializable {

  private String transNo;  //交易流水号
  private int partnerId;  //开发者身份标识
  private OrderDetail orderDetail;  //投保单详情
}
