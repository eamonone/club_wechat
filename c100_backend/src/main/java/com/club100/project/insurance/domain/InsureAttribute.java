package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsureAttribute {
private String  productName;   //产品名称
private int  state;   //产品状态 0：待审 1：上架 2：下架 3：停售 4：测试
private String  companyName;   //保险公司名称
private int  calculateType;   //保障期限计算方式 0：自然 1：固定
private int  minPeople;   //一单最少被保人数限制
private int  maxPeople;   //一单最多被保人数限制
private int  beneficiaryType;   //受益方式  1：法定 2：指定 12：法定和指定
private int  firstBeneficiaryLimit;   //第一顺序受益人个数
private int  secondBeneficiaryLimit;   //第二顺序受益人个数
private int  observationPeriod;   //观察期（单位：天）
private int  periodHesitation;   //犹豫期（单位：天）
private int  singleDayLimit;   //单次出行天数限制（单位：天）
private int  insureTime;   //起保时间  1：可选起保日期 2：固定起保日期 3：起保日期为出单日期
private int  insureTimePrecision;   //保障期限精确度
private int  firstDate;   //可选的投保日期开始时间 0：当日
private int  latestDate;   //可选的投保日期结束时间 0：当日
private String  deadline;   //当日投保截止时间 格式： 24:00
private String  nextDayDeadline;   //次日投保截止时间 格式： 24:00
private int  insuranceTime;   //投保起保时间（单位：小时）

private String   fixedDateStr;    //固定起保日期
private String   fixedLatestDateDtr;    //固定起保日期投保截止时间
private int   insureIncludeBirthday;    //保费计算是否包含生日当天 0：否 1：是
private int   insuredAgeCalculation;    //被保人年龄计算方式 1：起保日期 2：投保日期
private String   insureDeclare;    //投保声明
private String   jobGrade;    //  职业等级 以英文逗号“,”分隔
private int   destinationSelectOne;    //目的地只允许用户选择一个国家/地区 0：否 1：是
private String   smallLogoImg;    //保险公司logo小图
private String   bigLogoImg;    //保险公司logo大图
private int   healthWebId;    //健康告知id（PC），如果id值大于0则用户需填写健康告知
private int   healthAppId;    //健康告知id（H5），部分H5告知共用PC端，建议以PC端进行判断
private int   financialId;    //财务告知id
private int   otherId;    //其他告知id
private int   isForcePay;    //强制续期缴费说明
private String   forcePayContent;    //其他告知id
private int   isHzOwner;    //是否支持在线测试 0：否 1：是
private List<AttributeModule> attrModules;    //模块信息
}
