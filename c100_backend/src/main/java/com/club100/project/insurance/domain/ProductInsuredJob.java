package com.club100.project.insurance.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductInsuredJob {
 //获取产品职业信息

  private long  id;   // 职业id（用户承保接口）
  private String  name;   // 职业名称
  private long  parentId;   // 上一级职业id
  private String  isInsure;   // 是否支持投保
}
