package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RoutePicture;
import com.club100.project.wechat.mapper.RoutePictureMapper;
import com.club100.project.wechat.service.IRoutePictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/08/10 17:04:55
 * @Description: 路线详情图片服务实现类。
 */
@Slf4j
@Service
public class RoutePictureServiceImpl implements IRoutePictureService {

    @Autowired
    private RoutePictureMapper routePictureMapper;


    @Override
    public List<RoutePicture> selectRoutePicture(RoutePicture routePicture) {
        return routePictureMapper.selectRoutePicture(routePicture);
    }

}
