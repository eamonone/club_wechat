package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/14 14:59:48
 * @Description: 产品返回值。
 */
@AllArgsConstructor
public class ReturnProductInfo implements Serializable {

    public Long returnProductInfoId;
    public String returnActivityNameInfo;
    public String[] returnProductInfo;

    public String toJson(ReturnProductInfo returnProductInfo) {

        // ReturnProductInfo 对象转 JSON。
        return JSON.toJSONString(returnProductInfo);

    }

}
