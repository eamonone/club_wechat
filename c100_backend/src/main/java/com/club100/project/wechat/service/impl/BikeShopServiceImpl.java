package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BikeShop;
import com.club100.project.wechat.mapper.BikeShopMapper;
import com.club100.project.wechat.service.IBikeShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 11:16:41
 * @Description: 车店服务实现类。
 */
@Slf4j
@Service
public class BikeShopServiceImpl implements IBikeShopService {

    @Autowired
    private BikeShopMapper bikeShopMapper;

    @Override
    public int insertBikeShop(BikeShop bikeShop) {
        return bikeShopMapper.insertBikeShop(bikeShop);
    }

    @Override
    public BikeShop selectBikeShopById(Long bikeId) {
        return bikeShopMapper.selectBikeShopById(bikeId);
    }

    @Override
    public List<BikeShop> selectBikeShopList(BikeShop bikeShop) {
        return bikeShopMapper.selectBikeShopList(bikeShop);
    }

    @Override
    public List<BikeShop> selectBikeShopListWithSysUserId(BikeShop bikeShop) {
        return bikeShopMapper.selectBikeShopListWithSysUserId(bikeShop);
    }

    @Override
    public int updateBikeShop(BikeShop bikeShop) {
        return bikeShopMapper.updateBikeShop(bikeShop);
    }

    @Override
    public int deleteByBikeShopIds(Long... bikeId) {
        return bikeShopMapper.deleteByBikeShopIds(bikeId);
    }

    @Override
    public List<BikeShop> selectBikeShopDropdownList(BikeShop bikeShop) {
        return bikeShopMapper.selectBikeShopDropdownList(bikeShop);
    }

}
