package com.club100.project.wechat.domain.v3;

import lombok.Data;

import java.util.List;

@Data
public class GoodsDetail {

  private String goods_name;
  private String wechatpay_goods_id;
  private int quantity;
  private String merchant_goods_id;
  private int unit_price;
}
