package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.mapper.ActivityMapper;
import com.club100.project.wechat.service.IActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 20:07:17
 * @Description: 活动服务实现类。
 */
@Slf4j
@Service
public class ActivityServiceImpl implements IActivityService {

    @Autowired
    private ActivityMapper activityMapper;

    @Override
    public int insertActivity(Activity activity) {
        return activityMapper.insertActivity(activity);
    }

    @Override
    public List<Activity> selectActivityListWithConditions(Activity activity, Integer singleDay, Integer multiDays, Integer notStarted, Integer inProgress, Integer hasEnded, LocalDate currentDate, Integer free, Integer charge, Integer userCity) {
        return activityMapper.selectActivityListWithConditions(activity, singleDay, multiDays, notStarted, inProgress, hasEnded, currentDate, free, charge, userCity);
    }

    @Override
    public Activity selectActivityById(Long activityId) {
        return activityMapper.selectActivityById(activityId);
    }

    @Override
    public List<Activity> selectActivityListWithOrders(Integer tabSequence, LocalDate currentDate, Long bikeShopId) {
        return activityMapper.selectActivityListWithOrders(tabSequence, currentDate, bikeShopId);
    }

    @Override
    public List<Activity> selectActivityList(Activity activity) {
        return activityMapper.selectActivityList(activity);
    }

    @Override
    public List<Activity> selectActivityListWithSysUserId(Activity activity) {
        return activityMapper.selectActivityListWithSysUserId(activity);
    }

    @Override
    public int updateActivity(Activity activity) {
        return activityMapper.updateActivity(activity);
    }

    @Override
    public int deleteActivityByIds(Long... activityId) {
        return activityMapper.deleteActivityByIds(activityId);
    }

    @Override
    public List<Activity> selectActivityDropdownList(Activity activity) {
        return activityMapper.selectActivityDropdownList(activity);
    }

    @Override
    public List<Activity> selectActivityAtMostTwoList(Activity activity) {
        return activityMapper.selectActivityAtMostTwoList(activity);
    }

    @Override
    public Activity selectActivityQrcodeUrl(Long activityId) {
        return activityMapper.selectActivityQrcodeUrl(activityId);
    }

    @Override
    public List<Activity> selectActivityByBeginTime() {
        return activityMapper.selectActivityByBeginTime();
    }

}
