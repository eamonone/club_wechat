package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/27 15:20:28
 * @Description: 打卡用户 Mapper 接口。
 */
public interface MarkRouteUserMapper {

    List<MarkRouteUser> selectMarkedUserList(MarkRoute markRoute);

    List<MarkRouteUser> selectMarkedUserAtMostSixList(MarkRoute markRoute);

}
