package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 11:42:12
 * @Description: 活动后勤保障关联 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityLogistics extends BaseEntity implements Serializable {

    private Long activityLogisticsId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long activityId; // 活动主键。
    private Long logisticsId; // 后勤保障主键。
    private Long sysUserId;
    private String activityName;

    public String toJson(ActivityLogistics activityLogistics) {

        // ActivityLogistics 对象转 JSON。
        return JSON.toJSONString(activityLogistics);

    }

}
