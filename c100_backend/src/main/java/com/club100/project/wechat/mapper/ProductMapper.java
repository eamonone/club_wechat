package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Product;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 17:09:04
 * @Description: 商品 Mapper 接口。
 */
public interface ProductMapper {

    int insertProduct(Product product);

    int insertProductInBatch(List<Product> productList);

    List<Product> selectProductList(Product product);

    List<Product> selectProductListWithPrivilege(Product product);

    int deleteProduct(Long... activityId);

}
