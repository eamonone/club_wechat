package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/01 12:34:57
 * @Description: 披萨 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pizza extends BaseEntity implements Serializable {

    private Long pizzaGetterId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃；3：审核中。）。
    private Long userId; // 用户主键 ID。
    private String markPictureUrl; // 披萨图片 URL。
    private String returnPizzaPictureInfo; // 组合后返回的披萨图片 URL。
    private String weChatId; // 微信 ID。

    public String toJson(Pizza pizza) {

        // Pizza 对象转 JSON。
        return JSON.toJSONString(pizza);

    }

}
