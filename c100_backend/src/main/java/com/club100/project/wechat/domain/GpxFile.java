package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/17 17:01:02
 * @Description: GPX 文件转换 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GpxFile extends BaseEntity implements Serializable {

    private String gpxFileUrl;

    public String toJson(GpxFile gpxFile) {

        // GpxFile 对象转 JSON。
        return JSON.toJSONString(gpxFile);

    }

}
