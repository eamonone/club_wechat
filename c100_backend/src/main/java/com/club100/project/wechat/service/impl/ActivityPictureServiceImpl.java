package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityPicture;
import com.club100.project.wechat.mapper.ActivityPictureMapper;
import com.club100.project.wechat.service.IActivityPictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/12 14:54:44
 * @Description: 活动详情图片服务实现类。
 */
@Slf4j
@Service
public class ActivityPictureServiceImpl implements IActivityPictureService {

    @Autowired
    private ActivityPictureMapper activityPictureMapper;

    @Override
    public List<ActivityPicture> selectActivityPicture(ActivityPicture activityPicture) {
        return activityPictureMapper.selectActivityPicture(activityPicture);
    }

    @Override
    public List<ActivityPicture> selectActivityPictureWithPrivilege(ActivityPicture activityPicture) {
        return activityPictureMapper.selectActivityPictureWithPrivilege(activityPicture);
    }

    @Override
    public int insertActivityPicture(ActivityPicture activityPicture) {
        return activityPictureMapper.insertActivityPicture(activityPicture);
    }

    @Override
    public int insertActivityPictureInBatch(List<ActivityPicture> activityPictureList) {
        return activityPictureMapper.insertActivityPictureInBatch(activityPictureList);
    }

    @Override
    public int deleteActivityPicture(Long... activityId) {
        return activityPictureMapper.deleteActivityPicture(activityId);
    }

}
