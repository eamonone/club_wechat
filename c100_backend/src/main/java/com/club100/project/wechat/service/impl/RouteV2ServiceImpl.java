package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RouteV2;
import com.club100.project.wechat.mapper.RouteV2Mapper;
import com.club100.project.wechat.service.IRouteV2Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 16:50:26
 * @Description: 路线V2服务实现类。
 */
@Slf4j
@Service
public class RouteV2ServiceImpl implements IRouteV2Service {

    @Autowired
    private RouteV2Mapper routeV2Mapper;

    @Override
    public List<RouteV2> selectRouteV2AtMostTwoList(RouteV2 routeV2) {
        return routeV2Mapper.selectRouteV2AtMostTwoList(routeV2);
    }

    @Override
    public List<RouteV2> selectRouteV2List(Integer cityType, Integer routeDistance1Begin, Integer routeDistance1End, Integer routeDistance2Begin, Integer routeDistance2End, Integer routeDistance3Begin, Integer routeDistance3End, Integer routeDistance4Begin, Integer routeDistance4End, Integer routeDistance5Begin, Integer routeDistance5End, Integer routeDistance6Begin, Integer routeDistance6End, Integer[] routeTerrain, Integer[] routeType) {
        return routeV2Mapper.selectRouteV2List(cityType, routeDistance1Begin, routeDistance1End, routeDistance2Begin, routeDistance2End, routeDistance3Begin, routeDistance3End, routeDistance4Begin, routeDistance4End, routeDistance5Begin, routeDistance5End, routeDistance6Begin, routeDistance6End, routeTerrain, routeType);
    }

}
