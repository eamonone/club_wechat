package com.club100.project.wechat.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * club_product
 * @author 
 */
@Data
public class ClubProduct implements Serializable {
    /**
     * 主键 ID。
     */
    private Long id;

    /**
     * 数据创建时间。
     */
    private Date createtime;

    /**
     * 数据修改时间。
     */
    private Date updatetime;

    /**
     * 状态（1：可用；2：废弃。）。
     */
    private Integer state;

    /**
     * 关联城市类型（详情见 club_city 表）。
     */
    private Integer citytype;

    /**
     * 商品名称。
     */
    private String productname;

    /**
     * 商品价格。
     */
    private Double productprice;

    /**
     * 活动主键。
     */
    private Long activityid;

    private static final long serialVersionUID = 1L;
}