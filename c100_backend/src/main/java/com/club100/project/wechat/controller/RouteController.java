package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.atricle.domain.AtricleQueryRequest;
import com.club100.project.wechat.domain.Card;
import com.club100.project.wechat.domain.City;
import com.club100.project.wechat.domain.Route;
import com.club100.project.wechat.domain.RouteAndDetail;
import com.club100.project.wechat.service.IRouteAndDetailService;
import com.club100.project.wechat.service.IRouteService;
import com.club100.project.wechat.utils.EmptyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 16:29:46
 * @Description: 路线控制器。
 */
@RequestMapping("/v1/route")
@RestController
public class RouteController extends BaseController {

//    private String PREFIX = "/v1/route/";

    @Autowired
    private IRouteService routeService;
    @Autowired
    private IRouteAndDetailService routeAndDetailService;

    @PostMapping
    public AjaxResult insert(@RequestBody Route route) {

        routeService.insert(route);
        return AjaxResult.success();

    }

    @RequestMapping(value = "/insertOneRoute")
    @ResponseBody
    public int insertOneRoute(@RequestBody Route route) {

        Double difficultyLevelDouble = P_CalculateDifficultyLevel(route.getClimb(), route.getDistance());
        route.setDifficultyLevel(difficultyLevelDouble);

        return routeService.insertOneRoute(route);

    }

    @GetMapping
    public TableDataInfo selectRouteAndDetail2(RouteAndDetail routeAndDetail) {

        startPage();
        return getDataTable(routeAndDetailService.selectRouteAndDetail2(routeAndDetail));

    }

//    @GetMapping("mp")
//    public AjaxResult mpPage(Route route) {
//
//        startPage();
//        return AjaxResult.success(getPageTable(routeService.selectRouteList2(route)));
//
//    }

    @DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(routeService.deleteByRouteIds(id));

    }

    @RequestMapping(value = "/updateRoute")
    public Integer updateRoute(@RequestBody Route route) {

        return routeService.updateRoute(route);

    }

    private Double P_CalculateDifficultyLevel(Double climbDouble, Double distanceDouble) {

        Double difficultyLevelDouble;

        if (EmptyUtils.isEmpty(climbDouble) || EmptyUtils.isEmpty(distanceDouble)) {
            difficultyLevelDouble = -1.00;
        } else {
            if ((0 <= climbDouble && climbDouble <= 200) && (0 <= distanceDouble && distanceDouble <= 35)) {
                difficultyLevelDouble = 1.00;
            } else if ((0 <= climbDouble && climbDouble <= 200) && (35 < distanceDouble && distanceDouble <= 90) || (200 < climbDouble && climbDouble <= 300) && (0 <= distanceDouble && distanceDouble <= 35)) {
                difficultyLevelDouble = 2.00;
            } else if ((0 <= climbDouble && climbDouble <= 300) && (90 < distanceDouble && distanceDouble <= 120) || (300 < climbDouble && climbDouble <= 800) && (0 <= distanceDouble && distanceDouble <= 90) || (200 < climbDouble && climbDouble <= 300) && (35 < distanceDouble && distanceDouble <= 90)) {
                difficultyLevelDouble = 3.00;
            } else if ((0 <= climbDouble && climbDouble <= 500) && (120 < distanceDouble && distanceDouble <= 170) || (300 < climbDouble && climbDouble <= 800) && (90 < distanceDouble && distanceDouble <= 120) || (800 < climbDouble && climbDouble <= 1500) && (0 <= distanceDouble && distanceDouble <= 90)) {
                difficultyLevelDouble = 4.00;
            } else {
                difficultyLevelDouble = 5.00;
            }
        }

        return difficultyLevelDouble;

    }

    /**
     * @Description 单个路线上线按钮操作。
     * @param routeId
     * @return
     */
    @RequestMapping(value = "/uploadRoute")
    public int uploadRoute(@RequestBody Long routeId) {

        Route updatedRouteEntity = new Route();
        updatedRouteEntity.setRouteId(routeId);
        updatedRouteEntity.setState(1);

        return routeService.updateRoute(updatedRouteEntity);

    }

}
