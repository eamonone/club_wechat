package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Route;
import com.club100.project.wechat.domain.RouteDetail;
import com.club100.project.wechat.mapper.RouteDetailMapper;
import com.club100.project.wechat.mapper.RouteMapper;
import com.club100.project.wechat.service.IRouteDetailService;
import com.club100.project.wechat.service.IRouteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/28 16:40:07
 * @Description: 路线详情服务实现类。
 */
@Slf4j
@Service
public class RouteDetailServiceImpl implements IRouteDetailService {

    @Autowired
    private RouteDetailMapper routeDetailMapper;

    @Override
    public int insertOneRouteDetail(RouteDetail routeDetail) {
        return routeDetailMapper.insertOneRouteDetail(routeDetail);
    }

    @Override
    public RouteDetail selectRouteDetail(RouteDetail routeDetail) {
        return routeDetailMapper.selectRouteDetail(routeDetail);
    }

    @Override
    public List<RouteDetail> selectRouteDetailList(RouteDetail routeDetail) {
        return routeDetailMapper.selectRouteDetailList(routeDetail);
    }

    @Override
    public int updateRouteDetail(RouteDetail routeDetail) {
        return routeDetailMapper.updateRouteDetail(routeDetail);
    }

}
