package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Card;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/16 17:51:17
 * @Description: 主页卡片 Mapper 接口。
 */
public interface CardMapper {

    int insertCard(Card card);

    List<Card> selectCardAtMostFiveList(Card card);

    List<Card> selectCardList(Card card);

    int deleteByCardIds(Long... id);

    int updateCard(Card card);

}
