package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.Inspire;
import com.club100.project.wechat.service.IInspireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 15:54:14
 * @Description: 激励控制器。
 */
@RequestMapping("/v1/inspire")
@RestController
public class InspireController extends BaseController {

//    private String PREFIX = "/v1/inspire/";

    @Autowired
    private IInspireService inspireService;

    @PostMapping
    public AjaxResult insert(@RequestBody Inspire inspire) {

        inspireService.insert(inspire);
        return AjaxResult.success();

    }

    @GetMapping
    public TableDataInfo page(Inspire inspire) {

        startPage();
        return getDataTable(inspireService.selectInspireList());

    }

    @GetMapping("mp")
    public AjaxResult mpPage(Inspire inspire) {

        startPage();
        return AjaxResult.success(getPageTable(inspireService.selectInspireList()));

    }

    @DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(inspireService.deleteByInspireIds(id));

    }

}
