package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.City;
import com.club100.project.wechat.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 21:06:16
 * @Description: 城市控制器。
 */
@RequestMapping("/v1/city")
@RestController
public class CityController extends BaseController {

//    private String PREFIX = "/v1/city/";

    @Autowired
    private ICityService cityService;

    /**
     * 获取城市列表。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:list')")
    @GetMapping("/list")
    public TableDataInfo list(City city) {

        startPage();
        List<City> list = cityService.page(city);
        return getDataTable(list);

    }

    @RequestMapping("/addCity")
    public int insert(@RequestBody City city) {

        return cityService.insert(city);

    }

    @GetMapping
    public TableDataInfo page(City city) {

        startPage();
        return getDataTable(cityService.page(city));

    }

    @GetMapping("/selectCityDropdownList")
    public AjaxResult selectCityDropdownList() {

        return AjaxResult.success(cityService.selectCityDropdownList());

    }

    @GetMapping("/selectCityNationwideDropdownList")
    public AjaxResult selectCityNationwideDropdownList() {

        List<City> citiesList = cityService.selectCityDropdownList();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        Long club100SysUserIdLong = 110L;
        Long zwiftChinaSysUserIdLong = 114L;
        if (club100SysUserIdLong.equals(sysUserIdLong) || zwiftChinaSysUserIdLong.equals(sysUserIdLong)) {

            City nationwideCityEntity = new City();
            nationwideCityEntity.setCityId(-100L);
            nationwideCityEntity.setCityNameCH("全国");
            nationwideCityEntity.setCityNameEN("Nationwide");

            citiesList.add(nationwideCityEntity);

        }

        return AjaxResult.success(citiesList);

    }

    /*@GetMapping("mp")
    public AjaxResult mpPage(City city) {

        startPage();
        return AjaxResult.success(getPageTable(cityService.page(city)));

    }*/

    /**
     * 根据城市编号获取详细信息。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:query')")
    @RequestMapping(value = "/getCityById")
    public AjaxResult getCityById(@RequestBody Long cityId) {

        return AjaxResult.success(cityService.selectCityById(cityId));

    }

    @RequestMapping(value = "/update")
    public int update(@RequestBody City city) {

        return cityService.updateCity(city);

    }

    /**
     * 删除城市。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:remove')")
//    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{cityIds}")
    public AjaxResult deleteById(@PathVariable Long[] cityIds) {

        return toAjax(cityService.deleteByCityIds(cityIds));

    }

    /**
     * 上线城市。
     */
    @RequestMapping(value = "/uploadCity")
    public int uploadCity(@RequestBody Long cityId) {

        City updatedCityEntity = new City();
        updatedCityEntity.setCityId(cityId);
        updatedCityEntity.setState(1);

        return cityService.updateCity(updatedCityEntity);

    }

}
