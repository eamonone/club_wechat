package com.club100.project.wechat.utils;

import java.io.*;
import java.util.Random;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/30 17:53:14
 * @Description: I/O 流相关工具类。OutputStreamWriter
 */
public class IOStreamUtils {

    private static final String STRING_VALUE_DOT = ".";
    private static final String STRING_VALUE_AUTO_GENERATED = "AUTO_GENERATED";

    // 私有 IOUtils 构造类，让外界不能创建对象，只能静态调用方法。
    private IOStreamUtils() {

        throw new UnsupportedOperationException("'IOUtils' can't be instantiated~");

    }

    public static String S_outputContentToFile(String contentString, String fileNameString, String fileTypeString, String charsetString) {

        // Initialization.
        OutputStreamWriter osw;

        //
        if (STRING_VALUE_AUTO_GENERATED.equals(fileNameString)) {
            fileNameString = PrimaryKeyIdUtils.S_uuidWithoutHyphenAutoGenerator();
        }

        //
        String outputFileNameString = fileNameString + STRING_VALUE_DOT + fileTypeString;

        try {
            osw = new OutputStreamWriter(new FileOutputStream(outputFileNameString), charsetString);
            osw.write(contentString);
            osw.flush(); // 清空缓冲区，强制输出数据。
            osw.close(); // 关闭输出流。
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputFileNameString;

    }

}
