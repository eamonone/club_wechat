package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.UserSource;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/25 16:01:02
 * @Description: 用户来源服务类。
 */
public interface IUserSourceService {

    /**
     * 插入用户来源信息。
     *
     * @param userSource
     * @return
     */
    int insertUserSource(UserSource userSource);

    /**
     * 通过主键查询用户来源信息。
     *
     * @param userSourceId
     * @return
     */
    UserSource selectUserSourceById(Long userSourceId);

    /**
     * 查询用户来源信息。
     *
     * @param userSource
     * @return
     */
    List<UserSource> selectUserSource(UserSource userSource);

    /**
     * 修改用户来源信息。
     *
     * @param userSource
     * @return
     */
    int updateUserSource(UserSource userSource);

}
