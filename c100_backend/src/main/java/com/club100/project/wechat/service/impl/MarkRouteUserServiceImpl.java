package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;
import com.club100.project.wechat.mapper.MarkRouteUserMapper;
import com.club100.project.wechat.service.IMarkRouteUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/27 15:22:49
 * @Description: 打卡用户服务实现类。
 */
@Slf4j
@Service
public class MarkRouteUserServiceImpl implements IMarkRouteUserService {

    @Autowired
    private MarkRouteUserMapper markRouteUserMapper;

    @Override
    public List<MarkRouteUser> selectMarkedUserList(MarkRoute markRoute) {
        return markRouteUserMapper.selectMarkedUserList(markRoute);
    }

    @Override
    public List<MarkRouteUser> selectMarkedUserAtMostSixList(MarkRoute markRoute) {
        return markRouteUserMapper.selectMarkedUserAtMostSixList(markRoute);
    }

}
