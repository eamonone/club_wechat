package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.PizzaGetterUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 15:37:46
 * @Description: 已获披萨用户服务类。
 */
public interface IPizzaGetterUserService {

    /**
     * 列举已获披萨用户信息。
     *
     * @return
     */
    List<PizzaGetterUser> selectPizzaGetterUser();

}
