package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/08/10 16:14:48
 * @Description: 路线详情图片 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoutePicture extends BaseEntity implements Serializable {

    private Long routePictureId; // Primary Key ID。

    private Integer state; // 收藏路线状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主表的主键 ID。
    private String routePictureUrl; // 路线图片链接 URL。

    public String toJson(RoutePicture routePicture) {

        // RoutePicture 对象转 JSON。
        return JSON.toJSONString(routePicture);

    }

}
