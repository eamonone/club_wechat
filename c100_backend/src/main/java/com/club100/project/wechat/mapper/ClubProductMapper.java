package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ClubProduct;

public interface ClubProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ClubProduct record);

    int insertSelective(ClubProduct record);

    ClubProduct selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ClubProduct record);

    int updateByPrimaryKey(ClubProduct record);
}