package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 14:17:14
 * @Description: 披萨基本情况 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PizzaBackgrounds extends BaseEntity implements Serializable {

    private Long pizzaBackgroundsId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃；3：审核中。）。
    private String pizzaDescription; // 披萨活动介绍。
    private String pizzaPictureUrl; // 披萨图片链接 URL。
    private String qrcodeDescription; // 二维码介绍。
    private String qrcodePictureUrl; // 二维码图片链接 URL。

    public String toJson(PizzaBackgrounds pizzaBackgrounds) {

        // PizzaBackgrounds 对象转 JSON。
        return JSON.toJSONString(pizzaBackgrounds);

    }

}
