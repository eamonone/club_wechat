package com.club100.project.wechat.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * club_refund_policy
 * @author 
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClubRefundPolicyResponse implements Serializable {

  @ApiModelProperty("订单id")
  private Integer orderId;
  @ApiModelProperty("活动id")
  private Long activityId;
  @ApiModelProperty("活动名称")
  private String activityName;
  @ApiModelProperty("退款金额")
  private BigDecimal refundFee;
  @ApiModelProperty("订单金额")
  private BigDecimal orderFee;
}