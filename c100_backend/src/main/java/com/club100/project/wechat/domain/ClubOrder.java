package com.club100.project.wechat.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * club_order
 * @author 
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClubOrder implements Serializable {
    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 用户id(club_user表主键)
     */
    private Long userId;

    /**
     * 订单号
     */
    private String outTradeNo;
    //商户分账单号
    private String outOrderNo;
   //微信订单号
    private String transactionId;
    //子商户号
    private String subMchId;

    /**
     * 订单金额(单位元保留两位小数)
     */
    private BigDecimal orderFee;

    /**
     * 支付金额
     */
    private BigDecimal cashFee;

    //分帐金额
    private BigDecimal profitSharingFee;

    private String profitSharingAccount;

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 商品描述
     */
    private String productDescription;

  /**
   *
   * 车店图片路径
   */
  private String activityPictureUrl;

  private String activityName;

    /**
     * 预支付交易会话标识(微信生成，有效期2小时)
     */
    private String prepayId;

    /**
     * 交易类型
     */
    private String tradeType;

    /**
     * 交易状态
     */
    private String tradeStatus;

    /**
     * 交易状态描述
     */
    private String tradeStateDesc;

    /**
     * 支付完成时间（格式为yyyyMMddHHmmss）
     */
    private String timeEnd;

    /**
     * 返回信息
     */
    private String returnMsg;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态（0：支付中 1：支付成功 2：支付失败）
     */
    private Integer status;

    /**
     * 分帐是否完成（0：否 1：是）
     */
    private Integer isfinish;

    private static final long serialVersionUID = 1L;
}