package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RefundPolicy;
import com.club100.project.wechat.mapper.RefundPolicyMapper;
import com.club100.project.wechat.service.IRefundPolicyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/14 12:26:27
 * @Description: 退款政策服务实现类。
 */
@Slf4j
@Service
public class RefundPolicyServiceImpl implements IRefundPolicyService {

    @Autowired
    private RefundPolicyMapper refundPolicyMapper;

    @Override
    public int insertRefundPolicy(RefundPolicy refundPolicy) {
        return refundPolicyMapper.insertRefundPolicy(refundPolicy);
    }

    @Override
    public int insertRefundPolicyInBatch(List<RefundPolicy> refundPolicyList) {
        return refundPolicyMapper.insertRefundPolicyInBatch(refundPolicyList);
    }

    @Override
    public List<RefundPolicy> selectRefundPolicyList(RefundPolicy refundPolicy) {
        return refundPolicyMapper.selectRefundPolicyList(refundPolicy);
    }

    @Override
    public List<RefundPolicy> selectRefundPolicyListWithPrivilege(RefundPolicy refundPolicy) {
        return refundPolicyMapper.selectRefundPolicyListWithPrivilege(refundPolicy);
    }

    @Override
    public int deleteRefundPolicy(Long... activityId) {
        return refundPolicyMapper.deleteRefundPolicy(activityId);
    }

}
