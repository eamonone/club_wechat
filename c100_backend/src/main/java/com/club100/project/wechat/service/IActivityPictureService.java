package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/12 14:49:27
 * @Description: 活动详情图片服务类。
 */
public interface IActivityPictureService {

    /**
     * 查询活动详情图片信息。
     *
     * @param activityPicture
     * @return
     */
    List<ActivityPicture> selectActivityPicture(ActivityPicture activityPicture);

    /**
     * 查询活动详情图片信息。
     *
     * @param activityPicture
     * @return
     */
    List<ActivityPicture> selectActivityPictureWithPrivilege(ActivityPicture activityPicture);

    /**
     * 插入一条活动详情图片信息。
     *
     * @param activityPicture
     * @return
     */
    int insertActivityPicture(ActivityPicture activityPicture);

    /**
     * 批量插入多条活动详情图片信息。
     *
     * @param activityPictureList
     * @return
     */
    int insertActivityPictureInBatch(List<ActivityPicture> activityPictureList);

    /**
     * 删除活动详情图片信息。
     *
     * @param activityId
     * @return
     */
    int deleteActivityPicture(Long... activityId);

}
