package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Leader;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 16:36:23
 * @Description: 领队服务类。
 */
public interface ILeaderService {

    /**
     * 插入一条领队信息。
     *
     * @param leader
     * @return
     */
    int insertLeader(Leader leader);

    /**
     * 通过主键查询领队信息。
     *
     * @param leaderId
     * @return
     */
    Leader selectLeaderById(long leaderId);

    /**
     * 获取领队单选下拉框选项。
     *
     * @return
     */
    List<Leader> selectLeaderDropdownList();

}
