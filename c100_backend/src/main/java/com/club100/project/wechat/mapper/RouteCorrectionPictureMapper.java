package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RouteCorrectionPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 13:51:23
 * @Description: 路线纠错图片 Mapper 接口。
 */
public interface RouteCorrectionPictureMapper {

    int insertRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture);

    int insertRouteCorrectionPictureInBatch(List<RouteCorrectionPicture> routeCorrectionPictureList);

    RouteCorrectionPicture selectRouteCorrectionPictureById(Long routeCorrectionPictureId);

    List<RouteCorrectionPicture> selectRouteCorrectionPicture(RouteCorrectionPicture routeCorrectionPicture);

}
