package com.club100.project.wechat.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DownloadInsurance implements Serializable {

   @ApiModelProperty("订单id")
   private Integer orderId;
   @ApiModelProperty("邮箱")
   private String email;
}
