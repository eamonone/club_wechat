package com.club100.project.wechat.domain.v3;

import lombok.Data;

import java.util.List;

@Data
public class Detail {

  private String invoice_id;
  private int cost_price;
  private List<GoodsDetail> goods_detail;
}
