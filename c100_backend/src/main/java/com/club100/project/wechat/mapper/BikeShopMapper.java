package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BikeShop;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 10:59:10
 * @Description: 车店 Mapper 接口。
 */
public interface BikeShopMapper {

    int insertBikeShop(BikeShop bikeShop);

    BikeShop selectBikeShopById(Long bikeId);

    List<BikeShop> selectBikeShopList(BikeShop bikeShop);

    List<BikeShop> selectBikeShopListWithSysUserId(BikeShop bikeShop);

    int updateBikeShop(BikeShop bikeShop);

    int deleteByBikeShopIds(Long... bikeId);

    List<BikeShop> selectBikeShopDropdownList(BikeShop bikeShop);

}
