package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Source;
import com.club100.project.wechat.mapper.SourceMapper;
import com.club100.project.wechat.service.ISourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/30 19:52:55
 * @Description: 二维码参数统计服务实现类。
 */
@Slf4j
@Service
public class SourceServiceImpl implements ISourceService {

    @Autowired
    private SourceMapper sourceMapper;

    @Override
    public List<Source> selectSourceList(Source source) {
        return sourceMapper.selectSourceList(source);
    }

    @Override
    public int insertSource(Source source) {
        return sourceMapper.insertSource(source);
    }

    @Override
    public int updateSource(Source source) {
        return sourceMapper.updateSource(source);
    }

    @Override
    public int deleteBySourceIds(Long... sourceId) {
        return sourceMapper.deleteBySourceIds(sourceId);
    }

}
