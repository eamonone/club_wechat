package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Disclaimer;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 21:36:41
 * @Description: 免责声明服务类。
 */
public interface IDisclaimerService {

    /**
     * 插入一条免责声明信息。
     *
     * @param disclaimer
     * @return
     */
    int insertDisclaimer(Disclaimer disclaimer);

    /**
     * 通过主键查询一条免责声明信息。
     *
     * @param disclaimerId
     * @return
     */
    Disclaimer selectDisclaimerById(Long disclaimerId);

}
