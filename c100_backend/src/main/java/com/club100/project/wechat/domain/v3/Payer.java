package com.club100.project.wechat.domain.v3;

import lombok.Data;

@Data
public class Payer {

  private String sp_openid;
  private String sub_openid;
}
