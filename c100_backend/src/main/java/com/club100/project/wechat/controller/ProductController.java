package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.IActivityService;
import com.club100.project.wechat.service.IProductService;
import com.club100.project.wechat.utils.EmptyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/14 14:55:03
 * @Description: 产品控制器。
 */
@RequestMapping("/v1/product")
@RestController
public class ProductController extends BaseController {

    // Activity information.
    @Autowired
    private IActivityService activityService;
    // Product information.
    @Autowired
    private IProductService productService;

    /**
     * 查询产品列表。
     */
    @GetMapping("/listProduct")
    public TableDataInfo listProduct(Product product) {

        startPage();
        List<ReturnProductInfo> returnedProductInfoList = new ArrayList<>();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        product.setSysUserId(sysUserIdLong);
        List<Product> productList = productService.selectProductListWithPrivilege(product);

        // 分组。
        Map<Long, List<Product>> groupByActivityIdMap = productList.stream().collect(Collectors.groupingBy(Product::getActivityId));

        // 遍历分组并处理数据。
        for (Map.Entry<Long, List<Product>> entryProduct : groupByActivityIdMap.entrySet()) {
            Long key = entryProduct.getKey();

            String activityNameString = P_GetActivityNameById(key);
            List<Product> entryProductList = entryProduct.getValue();

            StringBuilder productStringBuilder = new StringBuilder();

            for (Product eachProduct : entryProductList) {

                String productContentString = eachProduct.getProductName() + " 价格: " + eachProduct.getProductPrice() + ";\r\n";

                if (productStringBuilder.length() == 0) {
                    productStringBuilder.append(productContentString);
                } else {
                    productStringBuilder.append(",").append(productContentString);
                }
            }

            String[] productStringArray = String.valueOf(productStringBuilder).split(",");

            ReturnProductInfo returnProductInfo = new ReturnProductInfo(key, activityNameString, productStringArray);
            returnedProductInfoList.add(returnProductInfo);

        }

        return getDataTable(returnedProductInfoList);

    }

    /**
     * 删除产品信息。
     */
    @DeleteMapping("/{returnProductInfoIds}")
    public AjaxResult deleteProduct(@PathVariable Long[] returnProductInfoIds) {

        return toAjax(productService.deleteProduct(returnProductInfoIds));

    }

    /**
     * 根据活动编号获取产品详细信息。
     */
    @RequestMapping(value = "/getProductById")
    public AjaxResult getProductById(@RequestBody Long returnProductInfoId) {

        Product searchedProductEntity = new Product();
        searchedProductEntity.setActivityId(returnProductInfoId);
        ReceivedProductInfo receivedProductInfo = new ReceivedProductInfo();

        receivedProductInfo.setReturnProductInfoId(returnProductInfoId);

        String activityNameString = P_GetActivityNameById(returnProductInfoId);
        receivedProductInfo.setReturnActivityNameInfo(activityNameString);

        List<Product> productsList = productService.selectProductList(searchedProductEntity);

        AtomicInteger count = new AtomicInteger();

        productsList.forEach(eachProductEntity -> {
            count.incrementAndGet();
            switch (count.get()) {
                case 1:
                    receivedProductInfo.setProductName1(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice1(eachProductEntity.getProductPrice());
                    break;
                case 2:
                    receivedProductInfo.setProductName2(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice2(eachProductEntity.getProductPrice());
                    break;
                case 3:
                    receivedProductInfo.setProductName3(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice3(eachProductEntity.getProductPrice());
                    break;
                case 4:
                    receivedProductInfo.setProductName4(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice4(eachProductEntity.getProductPrice());
                    break;
                case 5:
                    receivedProductInfo.setProductName5(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice5(eachProductEntity.getProductPrice());
                    break;
                case 6:
                    receivedProductInfo.setProductName6(eachProductEntity.getProductName());
                    receivedProductInfo.setProductPrice6(eachProductEntity.getProductPrice());
                    break;
                default:
                    break;
            }
        });

        return AjaxResult.success(receivedProductInfo);

    }

    @RequestMapping("/addProduct")
    public int addProduct(@RequestBody ReceivedProductInfo receivedProductInfo) {

        List<Product> insertedProductList = P_InsertProductInBatch(receivedProductInfo, "insert");

        return productService.insertProductInBatch(insertedProductList);

    }

    @RequestMapping("/updateProduct")
    public int updateProduct(@RequestBody ReceivedProductInfo receivedProductInfo) {

        int deletedCount = productService.deleteProduct(receivedProductInfo.getReturnProductInfoId());

        List<Product> insertedProductList;
        int insertRowsInt = 0;
        if (deletedCount > 0) {
            insertedProductList = P_InsertProductInBatch(receivedProductInfo, "update");
            insertRowsInt = productService.insertProductInBatch(insertedProductList);
        }

        return insertRowsInt;

    }

    /**
     * Get activity name by activity Id.
     *
     * @param activityId
     * @return
     */
    private String P_GetActivityNameById(long activityId) {

        Activity searchedActivityEntity = activityService.selectActivityById(activityId);

        return searchedActivityEntity.getActivityName();

    }

    /**
     * Fill in product list.
     *
     * @param productPriceDouble
     * @param insertedProductList
     * @param activityIdLong
     * @param productNameString
     * @return
     */
    private List<Product> P_FillInProductList(Double productPriceDouble, List<Product> insertedProductList, Long activityIdLong, String productNameString) {

        if (EmptyUtils.isNotEmpty(productPriceDouble) && EmptyUtils.isNotEmpty(productNameString)) {
            Product product = new Product();
            product.setActivityId(activityIdLong);
            product.setProductName(productNameString);
            product.setProductPrice(productPriceDouble);
            insertedProductList.add(product);
        }

        return insertedProductList;

    }

    private List<Product> P_InsertProductInBatch(ReceivedProductInfo receivedProductInfo, String actionString) {

        List<Product> insertedProductList = new ArrayList<>();
        Long activityIdLong = null;

        if ("insert".equalsIgnoreCase(actionString)) {
            activityIdLong = Long.valueOf(receivedProductInfo.getReturnActivityNameInfo());
        } else if ("update".equalsIgnoreCase(actionString)) {
            activityIdLong = receivedProductInfo.getReturnProductInfoId();
        }

        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice1(), insertedProductList, activityIdLong, receivedProductInfo.getProductName1());
        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice2(), insertedProductList, activityIdLong, receivedProductInfo.getProductName2());
        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice3(), insertedProductList, activityIdLong, receivedProductInfo.getProductName3());
        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice4(), insertedProductList, activityIdLong, receivedProductInfo.getProductName4());
        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice5(), insertedProductList, activityIdLong, receivedProductInfo.getProductName5());
        insertedProductList = P_FillInProductList(receivedProductInfo.getProductPrice6(), insertedProductList, activityIdLong, receivedProductInfo.getProductName6());

        return insertedProductList;

    }

}
