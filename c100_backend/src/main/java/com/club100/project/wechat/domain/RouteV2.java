package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 15:49:13
 * @Description: 路线V2 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteV2 extends BaseEntity implements Serializable {

    private Long routeId; // Primary Key ID。

    private Double averageClimb; // 多日路线平均爬升（单位：m）。
    private Double averageDistance; // 多日路线平均距离（单位：km）。
    private Double totalClimb; // 多日路线总爬升（单位：m）。
    private Double totalDistance; // 多日路线总距离（单位：km）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer routeClickCount; // 路线点击数（用于统计）。
    private Integer routeWeight; // 路线权重（用于排序）。
    private Integer state; // 路线状态（1：可用；2：废弃。）。
    private String biggestDifficultyLevel; // 多日路线中最大难度（单位：颗星）。
    private String routeDescription; // 路线描述。
    private String routeName; // 路线名。
    private String routePictureUrl; // 路线图片链接 URL。
    private String routeTerrain; // 路线地形（1：平路；2：起伏；3：爬坡。）。
    private String routeType; // 路线类型（1：绕圈；2：休闲；3：进阶；4：高阶；5：挑战。）。

    public String toJson(RouteV2 routeV2) {

        // RouteV2 对象转 JSON。
        return JSON.toJSONString(routeV2);

    }

}
