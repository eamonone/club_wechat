package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Insurance;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/11 21:15:23
 * @Description: 骑行意外险 Mapper 接口。
 */
public interface RideInsuranceMapper {

    int insertInsurance(Insurance insurance);

    Insurance selectInsuranceById(Long insuranceId);

}
