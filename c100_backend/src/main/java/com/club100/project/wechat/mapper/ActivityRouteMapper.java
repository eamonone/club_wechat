package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityRoute;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 21:39:48
 * @Description: 活动路线 Mapper 接口。
 */
public interface ActivityRouteMapper {

    int insertActivityRoute(ActivityRoute activityRoute);

    ActivityRoute selectActivityRouteById(Long activityRouteId);

    List<ActivityRoute> selectActivityRoute(ActivityRoute activityRoute);

    List<ActivityRoute> selectActivityRouteWithPrivilege(ActivityRoute activityRoute);

    int updateActivityRoute(ActivityRoute activityRoute);

    int deleteByActivityRouteIds(Long[] activityRouteIds);

}
