package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.BikeShop;
import com.club100.project.wechat.service.IBikeShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/29 18:56:04
 * @Description: 车店控制器。
 */
@RequestMapping("/v1/bikeshop")
@RestController
public class BikeShopController extends BaseController {

    // Bike shop information.
    @Autowired
    private IBikeShopService bikeShopService;

    /**
     * 获取车店列表。
     */
    @GetMapping("/list")
    public TableDataInfo list(BikeShop bikeShop) {

        startPage();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        bikeShop.setSysUserId(sysUserIdLong);

        List<BikeShop> bikeShopsList = bikeShopService.selectBikeShopListWithSysUserId(bikeShop);

        return getDataTable(bikeShopsList);

    }

    /**
     * 添加一条车店信息。
     *
     * @param bikeShop
     * @return
     */
    @RequestMapping("/addBikeShop")
    public int addBikeShop(@RequestBody BikeShop bikeShop) {

        Long sysUserIdLong = P_GetLoginSystemUserId();
        bikeShop.setSysUserId(sysUserIdLong);

        return bikeShopService.insertBikeShop(bikeShop);

    }

    /**
     * 根据车店编号获取详细信息。
     */
    @RequestMapping(value = "/getBikeShopById")
    public AjaxResult getBikeShopById(@RequestBody Long bikeShopId) {

        return AjaxResult.success(bikeShopService.selectBikeShopById(bikeShopId));

    }

    /**
     * 修改车店信息。
     */
    @RequestMapping(value = "/updateBikeShop")
    public int updateBikeShop(@RequestBody BikeShop bikeShop) {

        return bikeShopService.updateBikeShop(bikeShop);

    }

    /**
     * 删除车店信息。
     */
    @DeleteMapping("/{bikeShopIds}")
    public AjaxResult deleteById(@PathVariable Long[] bikeShopIds) {

        return toAjax(bikeShopService.deleteByBikeShopIds(bikeShopIds));

    }

    /**
     * 获取车店信息单选下拉框选项。
     *
     * @return
     */
    @GetMapping("/selectBikeShopDropdownList")
    public AjaxResult selectBikeShopDropdownList() {

        BikeShop bikeShop = new BikeShop();
        Long sysUserIdLong = P_GetLoginSystemUserId();
        bikeShop.setSysUserId(sysUserIdLong);

        return AjaxResult.success(bikeShopService.selectBikeShopDropdownList(bikeShop));

    }

}
