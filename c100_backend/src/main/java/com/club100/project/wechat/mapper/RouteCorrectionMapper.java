package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RouteCorrection;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 22:22:07
 * @Description: 路线纠错 Mapper 接口。
 */
public interface RouteCorrectionMapper {

    int insertRouteCorrection(RouteCorrection routeCorrection);

    RouteCorrection selectRouteCorrectionById(Long routeCorrectionId);

    List<RouteCorrection> selectRouteCorrection(RouteCorrection routeCorrection);

}
