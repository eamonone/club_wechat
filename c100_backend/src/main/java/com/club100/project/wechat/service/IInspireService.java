package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Inspire;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 15:41:53
 * @Description: 激励服务类。
 */
public interface IInspireService {
    /**
     * 插入激励信息。
     *
     * @param inspire
     * @return
     */
    int insert(Inspire inspire);

    /**
     * 列举激励信息（最多两条）。
     *
     * @param inspire
     * @return
     */
    List<Inspire> selectInspireAtMostTwoList(Inspire inspire);

    /**
     * 列举激励信息。
     *
     * @param
     * @return
     */
    List<Inspire> selectInspireList();

    /**
     * 删除激励信息。
     *
     * @param id(s)
     * @return
     */
    int deleteByInspireIds(Long... id);

}
