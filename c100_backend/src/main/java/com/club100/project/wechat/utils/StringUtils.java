package com.club100.project.wechat.utils;

import com.alibaba.fastjson.JSON;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/8/10 17:30:41
 * @Description: String 相关工具类。
 */
public class StringUtils {

    // 私有 StringUtils 构造类，让外界不能创建对象，只能静态调用方法。
    private StringUtils() {

        throw new UnsupportedOperationException("'StringUtils' can't be instantiated~");

    }

}
