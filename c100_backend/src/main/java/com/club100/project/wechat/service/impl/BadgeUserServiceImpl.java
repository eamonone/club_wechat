package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.BadgeUser;
import com.club100.project.wechat.mapper.BadgeUserMapper;
import com.club100.project.wechat.service.IBadgeUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 16:44:12
 * @Description: 徽章获得用户服务实现类。
 */
@Slf4j
@Service
public class BadgeUserServiceImpl implements IBadgeUserService {

    @Autowired
    private BadgeUserMapper badgeUserMapper;

    @Override
    public int insertBadgeUser(BadgeUser badgeUser) {
        return badgeUserMapper.insertBadgeUser(badgeUser);
    }

    @Override
    public BadgeUser selectOneBadgeUser(BadgeUser badgeUser) {
        return badgeUserMapper.selectOneBadgeUser(badgeUser);
    }

}
