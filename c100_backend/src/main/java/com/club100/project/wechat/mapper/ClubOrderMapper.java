package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.domain.ClubOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ClubOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClubOrder record);

    int insertSelective(ClubOrder record);

    ClubOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClubOrder record);

    int updateByPrimaryKey(ClubOrder record);

    @Select("select * from club_order where product_id = #{productId} and user_id = #{userId} limit 1 ")
    @ResultMap("BaseResultMap")
    ClubOrder selectOrderInfoByProductId(@Param("productId") Long productId, @Param("userId") Long userId);

  Activity selectActiveInfoByOutTradeNo(@Param("outTradeNo") String outTradeNo);


  ClubOrder selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);

  /**
   * 支付中和支付成功，失败的不记录
   *
   * @param productId
   * @return
   */
    @Select("select id,user_id,product_id,product_description,order_fee,create_time,status from club_order where product_id = #{productId} and status != 2")
  @ResultMap("BaseResultMap")
    List<ClubOrder> selectOrderListProductId(@Param("productId") Long productId);

  /**
   * 支付中和支付成功，失败的不记录
   *
   * @param activityId
   * @return
   */
  @Select("select id,user_id,product_id,product_description,order_fee,create_time,status from club_order where product_id in (select distinct id from " +
          " club_product where activityId = #{activityId}) and status != 2")
  @ResultMap("BaseResultMap")
  List<ClubOrder> selectOrderListActivityId(@Param("activityId") Long activityId);

  List<ClubOrder> selectFinishOrder(@Param("list") List<Long> activityIds);
}