package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.SignUp;
import com.club100.project.wechat.mapper.SignUpMapper;
import com.club100.project.wechat.service.ISignUpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 15:13:58
 * @Description: 报名服务实现类。
 */
@Slf4j
@Service
public class SignUpService implements ISignUpService {

    @Autowired
    private SignUpMapper signUpMapper;

    @Override
    public int insertSignUp(SignUp signUp) {
        return signUpMapper.insertSignUp(signUp);
    }

    @Override
    public SignUp selectSignUp(SignUp signUp) {
        return signUpMapper.selectSignUp(signUp);
    }

}
