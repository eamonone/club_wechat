package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.PizzaGetter;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 13:14:55
 * @Description: 披萨获得者服务类。
 */
public interface IPizzaGetterService {

    /**
     * 插入一条披萨获得者信息。
     *
     * @param pizzaGetter
     * @return
     */
    int insertPizzaGetter(PizzaGetter pizzaGetter);

    /**
     * 修改一条披萨获得者信息。
     *
     * @param pizzaGetter
     * @return
     */
    int updatePizzaGetter(PizzaGetter pizzaGetter);

    /**
     * 获得披萨获得者的数量。
     *
     * @return
     */
    int selectPizzaGetterCount();

}
