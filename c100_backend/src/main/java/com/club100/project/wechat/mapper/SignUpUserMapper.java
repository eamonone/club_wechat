package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.SignUpUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/19 22:53:25
 * @Description: 报名用户 Mapper 接口。
 */
public interface SignUpUserMapper {

    List<SignUpUser> selectSignUpUser(SignUpUser signUpUser);

}
