package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityRoute;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/23 21:52:21
 * @Description: 活动路线服务类。
 */
public interface IActivityRouteService {

    /**
     * 插入活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    int insertActivityRoute(ActivityRoute activityRoute);

    /**
     * 通过主键查询活动路线信息。
     *
     * @param activityRouteId
     * @return
     */
    ActivityRoute selectActivityRouteById(Long activityRouteId);

    /**
     * 查询活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    List<ActivityRoute> selectActivityRoute(ActivityRoute activityRoute);

    /**
     * 带权限查询活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    List<ActivityRoute> selectActivityRouteWithPrivilege(ActivityRoute activityRoute);

    /**
     * 修改活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    int updateActivityRoute(ActivityRoute activityRoute);

    /**
     * 删除活动路线信息。
     *
     * @param activityRouteIds
     * @return
     */
    int deleteByActivityRouteIds(Long[] activityRouteIds);

}
