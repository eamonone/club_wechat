package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.PizzaGetterUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 15:26:12
 * @Description: 已获披萨用户 Mapper 接口。
 */
public interface PizzaGetterUserMapper {

    List<PizzaGetterUser> selectPizzaGetterUser();

}
