package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.City;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 20:57:58
 * @Description: 城市 Mapper 接口。
 */
public interface CityMapper {

    int insertCity(City city);

    List<City> selectCityList(City city);

    List<City> selectCityListOrderById();

    List<City> selectCityById(Long cityId);

    int updateCity(City city);

    int deleteByCityIds(Long... cityId);

    List<City> selectCityDropdownList();

}
