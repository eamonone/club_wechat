package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:40:58
 * @Description: 打卡路线服务类。
 */
public interface IMarkRouteService {

    /**
     * @Description 插入（一条）打卡路线信息。
     * @param markRoute
     * @return
     */
    int insertMarkRoute(MarkRoute markRoute);

    /**
     * @Description 列举打卡路线信息。
     * @param markRoute
     * @return
     */
    List<MarkRoute> selectMarkRouteByUserIDList(MarkRoute markRoute);

    /**
     * @Description 根据条件查询打卡路线个数。
     * @param markRoute
     * @return
     */
    Integer selectMarkRouteCountList(MarkRoute markRoute);

    /**
     * @Description 根据条件查询打卡路线个数。
     *
     * @param userId
     * @return
     */
    int selectMarkRouteCountByUserId(Long userId);

    /**
     * @Description 根据条件查询一条打卡路线。
     * @param markRoute
     * @return
     */
    MarkRoute selectMarkRouteOne(MarkRoute markRoute);

    /**
     * @Description 根据条件查询打卡路线。
     * @param markRoute
     * @return
     */
    List<MarkRoute> selectMarkRouteList(MarkRoute markRoute);

    /**
     * @Description 根据条件更新打卡路线。
     * @param markRoute
     * @return
     */
    Integer updateMarkRouteById(MarkRoute markRoute);

}
