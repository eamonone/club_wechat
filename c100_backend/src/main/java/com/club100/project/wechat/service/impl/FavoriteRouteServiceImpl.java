package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.FavoriteRoute;
import com.club100.project.wechat.mapper.FavoriteRouteMapper;
import com.club100.project.wechat.service.IFavoriteRouteService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:08:09
 * @Description: 收藏路线服务实现类。
 */
@Slf4j
@Service
public class FavoriteRouteServiceImpl implements IFavoriteRouteService {

    @Autowired
    private FavoriteRouteMapper favoriteRouteMapper;

    @Override
    public int insertFavoriteRoute(FavoriteRoute favoriteRoute) {
        return favoriteRouteMapper.insertFavoriteRoute(favoriteRoute);
    }

    @Override
    public List<FavoriteRoute> selectFavoriteRouteList(FavoriteRoute favoriteRoute) {
        return favoriteRouteMapper.selectFavoriteRouteList(favoriteRoute);
    }

    @Override
    public List<FavoriteRoute> selectFavoriteRouteByPageList(@Param("userId") Long userId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize) {
        return favoriteRouteMapper.selectFavoriteRouteByPageList(userId, startIndex, pageSize);
    }

    @Override
    public int selectFavoriteRouteCountList(FavoriteRoute favoriteRoute) {
        return favoriteRouteMapper.selectFavoriteRouteCountList(favoriteRoute);
    }

    @Override
    public int deleteFavoriteRoute(FavoriteRoute favoriteRoute) {
        return favoriteRouteMapper.deleteFavoriteRoute(favoriteRoute);
    }

}
