package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.service.IActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/30 14:30:41
 * @Description: 活动控制器。
 */
@RequestMapping("/v1/activity")
@RestController
public class ActivityController extends BaseController {

    // Activity information.
    @Autowired
    private IActivityService activityService;

    /**
     * 获取活动列表。
     */
    @GetMapping("/listActivities")
    public TableDataInfo listActivities(Activity activity) {

        startPage();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activity.setSysUserId(sysUserIdLong);

        List<Activity> activitiesList = activityService.selectActivityListWithSysUserId(activity);

        return getDataTable(activitiesList);

    }

    /**
     * 删除活动信息。
     */
    @DeleteMapping("/{activityIds}")
    public AjaxResult deleteActivities(@PathVariable Long[] activityIds) {

        return toAjax(activityService.deleteActivityByIds(activityIds));

    }

    /**
     * 根据活动编号获取详细信息。
     */
    @RequestMapping(value = "/getActivityById")
    public AjaxResult getActivityById(@RequestBody Long activityIds) {

        return AjaxResult.success(activityService.selectActivityById(activityIds));

    }

    /**
     * 修改活动信息。
     */
    @RequestMapping(value = "/updateActivity")
    public int updateActivity(@RequestBody Activity activity) {

        // If the activity were weekly activity.
        activity = ifWeeklyActivity(activity);

        activity.setState(4);
        activity.setActivityAudit("正在审核中。");
        return activityService.updateActivity(activity);

    }

    /**
     * 添加一条活动信息。
     *
     * @param activity
     * @return
     */
    @RequestMapping("/addActivity")
    public int addActivity(@RequestBody Activity activity) {
        // 单次活动。
//        if (0 == activity.getActivityDay() && "00:00".equals(activity.getActivityTime().toString())) {
//        }

        // If the activity were weekly activity.
        activity = ifWeeklyActivity(activity);

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activity.setSysUserId(sysUserIdLong);
        activity.setState(1);

        return activityService.insertActivity(activity);

    }

    /**
     * 添加一条活动草稿信息。
     *
     * @param activity
     * @return
     */
    @RequestMapping("/addActivityDraft")
    public int addActivityDraft(@RequestBody Activity activity) {

        // If the activity were weekly activity.
        activity = ifWeeklyActivity(activity);

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activity.setSysUserId(sysUserIdLong);
        activity.setState(3);

        return activityService.insertActivity(activity);

    }

    private Activity ifWeeklyActivity(Activity activity) {

        // 每周活动。
        int dayOfWeekValue = LocalDateTime.now().getDayOfWeek().getValue();
        LocalDateTime chosenTime = LocalDateTime.now();
        if ("2991-12-24T00:00".equals(activity.getActivityBeginTime().toString()) && "2991-12-25T00:00".equals(activity.getActivityEndTime().toString())) {
            if (1 == activity.getActivityDay()) {
                if (dayOfWeekValue > 1) {
                    // 下周周一。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.MONDAY);
                } else {
                    // 本周周一。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.MONDAY);
                }
            } else if (2 == activity.getActivityDay()) {
                if (dayOfWeekValue > 2) {
                    // 下周周二。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.TUESDAY);
                } else {
                    // 本周周二。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.TUESDAY);
                }
            } else if (3 == activity.getActivityDay()) {
                if (dayOfWeekValue > 3) {
                    // 下周周三。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.WEDNESDAY);
                } else {
                    // 本周周三。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.WEDNESDAY);
                }
            } else if (4 == activity.getActivityDay()) {
                if (dayOfWeekValue > 4) {
                    // 下周周四。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.THURSDAY);
                } else {
                    // 本周周四。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.THURSDAY);
                }
            } else if (5 == activity.getActivityDay()) {
                if (dayOfWeekValue > 5) {
                    // 下周周五。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.FRIDAY);
                } else {
                    // 本周周五。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.FRIDAY);
                }
            } else if (6 == activity.getActivityDay()) {
                if (dayOfWeekValue > 6) {
                    // 下周周六。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.SATURDAY);
                } else {
                    // 本周周六。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.SATURDAY);
                }
            } else if (7 == activity.getActivityDay()) {
                if (dayOfWeekValue > 7) {
                    // 下周周日。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).plusWeeks(1).with(DayOfWeek.SUNDAY);
                } else {
                    // 本周周日。
                    chosenTime = LocalDateTime.of(LocalDate.now(), LocalTime.MIN).with(DayOfWeek.SUNDAY);
                }
            }

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String activityTimeString = activity.getActivityTime().toString();
            if (5 == activity.getActivityTime().toString().length()) {
                activityTimeString += ":00";
            }
            String modifiedBeginDateTimeString = chosenTime.toLocalDate().toString() + " " + activityTimeString;
            String modifiedEndDateTimeString = chosenTime.toLocalDate().toString() + " " + "23:59:59";
            LocalDateTime activityBeginTime = LocalDateTime.parse(modifiedBeginDateTimeString, dateTimeFormatter);
            LocalDateTime activityEndTime = LocalDateTime.parse(modifiedEndDateTimeString, dateTimeFormatter);
            activity.setActivityBeginTime(activityBeginTime);
            activity.setActivityEndTime(activityEndTime);

        }

        return activity;

    }

    /**
     * 获取活动单选下拉框选项。
     *
     * @return
     */
    @GetMapping("/selectActivityDropdownList")
    public AjaxResult selectActivityDropdownList() {

        Activity activity = new Activity();
        Long sysUserIdLong = P_GetLoginSystemUserId();
        activity.setSysUserId(sysUserIdLong);

        return AjaxResult.success(activityService.selectActivityDropdownList(activity));

    }

    /**
     * 审核成功操作。
     */
    @RequestMapping(value = "/activityAuditSuccess")
    public int activityAuditSuccess(@RequestBody Long activityId) {

        Activity updatedActivityEntity = new Activity();
        updatedActivityEntity.setActivityId(activityId);
        updatedActivityEntity.setState(1);
        updatedActivityEntity.setActivityAudit("审核通过。");

        return activityService.updateActivity(updatedActivityEntity);

    }

    /**
     * 审核失败操作。
     */
    @RequestMapping(value = "/activityAuditFailure")
    public int activityAuditFailure(@RequestBody Activity activity) {

        activity.setState(2);
        activity.setActivityAudit(activity.getActivityAudit());
        return activityService.updateActivity(activity);

    }

}
