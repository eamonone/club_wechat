package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:24:27
 * @Description: 路线 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Route extends BaseEntity implements Serializable {

    private Long routeId; // Primary Key ID。

    private Boolean ifBadgeExist; // 是否有徽章。
    private Double climb; // 爬升（单位：m）。
    private Double difficultyLevel; // 难度（单位：颗星）。
    private Double distance; // 距离（单位：km）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer routeClickCount; // 路线点击数。
    private Integer routeTerrain; // 路线地形（1：平路；2：起伏；3：爬坡。）。
    private Integer routeType; // 路线类型（1：绕圈；2：休闲；3：进阶；4：高阶；5：挑战。）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private String routeDescription; // 路线描述。
    private String routeName; // 路线名。
    private String routePicUrl; // 路线图片 URL。

    public String toJson(Route route) {

        // Route 对象转 JSON。
        return JSON.toJSONString(route);

    }

}
