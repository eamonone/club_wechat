package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Activity;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 20:04:29
 * @Description: 活动服务类。
 */
public interface IActivityService {

    /**
     * 插入活动信息。
     *
     * @param activity
     * @return
     */
    int insertActivity(Activity activity);

    /**
     * 查询活动列表。
     *
     * @param activity
     * @param singleDay
     * @param multiDays
     * @param notStarted
     * @param inProgress
     * @param hasEnded
     * @param currentDate
     * @param free
     * @param charge
     * @return
     */
    List<Activity> selectActivityListWithConditions(Activity activity, Integer singleDay, Integer multiDays, Integer notStarted, Integer inProgress, Integer hasEnded, LocalDate currentDate, Integer free, Integer charge, Integer userCity);

    /**
     * 通过主键查询活动。
     *
     * @param activityId
     * @return
     */
    Activity selectActivityById(Long activityId);

    /**
     * 查询活动列表。
     *
     * @param tabSequence
     * @param currentDate
     * @param bikeShopId
     * @return
     */
    List<Activity> selectActivityListWithOrders(Integer tabSequence, LocalDate currentDate, Long bikeShopId);

    /**
     * 查询活动列表。
     *
     * @param activity
     * @return
     */
    List<Activity> selectActivityList(Activity activity);

    /**
     * 带权限查询活动列表。
     *
     * @param activity
     * @return
     */
    List<Activity> selectActivityListWithSysUserId(Activity activity);

    /**
     * 修改一条活动信息。
     *
     * @param activity
     * @return
     */
    int updateActivity(Activity activity);

    /**
     * 通过主键删除活动。
     *
     * @param activityId(s)
     * @return
     */
    int deleteActivityByIds(Long... activityId);

    /**
     * 查询活动单选下拉框选项。
     *
     * @param activity
     * @return
     */
    List<Activity> selectActivityDropdownList(Activity activity);

    /**
     * 列举活动信息（最多两条）。
     *
     * @param activity
     * @return
     */
    List<Activity> selectActivityAtMostTwoList(Activity activity);

    /**
     * 通过主键查询活动二维码。
     *
     * @param activityId
     * @return
     */
    Activity selectActivityQrcodeUrl(Long activityId);

    /**
     * 通过活动开始时间筛选活动。
     * @return
     */
    List<Activity> selectActivityByBeginTime();

}
