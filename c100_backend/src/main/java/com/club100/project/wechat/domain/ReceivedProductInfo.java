package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/14 16:16:18
 * @Description: 产品接收 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceivedProductInfo implements Serializable {

    private Double productPrice1;
    private Double productPrice2;
    private Double productPrice3;
    private Double productPrice4;
    private Double productPrice5;
    private Double productPrice6;
    private Long returnProductInfoId;
    private String productName1;
    private String productName2;
    private String productName3;
    private String productName4;
    private String productName5;
    private String productName6;
    private String returnActivityNameInfo;

    public String toJson(ReceivedProductInfo receivedProductInfo) {

        // ReceivedProductInfo 对象转 JSON。
        return JSON.toJSONString(receivedProductInfo);

    }

}
