package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Badge;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 15:28:37
 * @Description: 徽章 Mapper 接口。
 */
public interface BadgeMapper {

    List<Badge> selectBadgeList(Badge badge);

    Badge selectBadgeById(Long badgeId);

    Badge selectBadgeByRouteId(Long routeId);

    int insertBadge(Badge badge);

}
