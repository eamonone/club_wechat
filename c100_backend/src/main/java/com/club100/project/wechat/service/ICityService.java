package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.City;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 21:01:03
 * @Description: 城市服务类。
 */
public interface ICityService {
    /**
     * 插入城市信息。
     *
     * @param city
     * @return
     */
    int insert(City city);

    /**
     * 列举城市信息。
     *
     * @param city
     * @return
     */
    List<City> page(City city);

    /**
     * 列举城市信息。
     *
     * @return
     */
    List<City> selectCityListOrderById();

    /**
     * 列举选定城市信息。
     *
     * @param cityId
     * @return
     */
    List<City> selectCityById(Long cityId);

    /**
     * 修改选定城市信息。
     *
     * @param city
     * @return
     */
    int updateCity(City city);

    /**
     * 删除城市信息。
     *
     * @param cityId(s)
     * @return
     */
    int deleteByCityIds(Long... cityId);

    /**
     * 获取下拉框城市信息。
     *
     * @return
     */
    List<City> selectCityDropdownList();

}
