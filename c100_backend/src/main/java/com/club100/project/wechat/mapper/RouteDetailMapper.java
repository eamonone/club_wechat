package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.RouteDetail;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/28 16:26:21
 * @Description: 路线详情 Mapper 接口。
 */
public interface RouteDetailMapper {

    int insertOneRouteDetail(RouteDetail routeDetail);

    RouteDetail selectRouteDetail(RouteDetail routeDetail);

    List<RouteDetail> selectRouteDetailList(RouteDetail routeDetail);

    int updateRouteDetail(RouteDetail routeDetail);

}
