package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/28 14:24:24
 * @Description: 打卡图片 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkPicture extends BaseEntity implements Serializable {

    private Long picId; // Primary Key ID。

    private Integer markPictureSequence; // 打卡图片序列号。
    private Long routeId; // 路线主表的主键 ID。
    private Long userId; // 用户主键 ID。
    private String markPictureUrl; // 打卡图片链接 URL。
    private String markTime; // 打卡返回时间。

    public String toJson(MarkPicture markPicture) {

        // MarkPicture 对象转 JSON。
        return JSON.toJSONString(markPicture);

    }

}
