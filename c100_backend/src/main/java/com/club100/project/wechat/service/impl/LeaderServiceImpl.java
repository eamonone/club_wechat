package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Leader;
import com.club100.project.wechat.mapper.LeaderMapper;
import com.club100.project.wechat.service.ILeaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/11 16:38:59
 * @Description: 领队服务实现类。
 */
@Slf4j
@Service
public class LeaderServiceImpl implements ILeaderService {

    @Autowired
    private LeaderMapper leaderMapper;

    @Override
    public int insertLeader(Leader leader) {
        return leaderMapper.insertLeader(leader);
    }

    @Override
    public Leader selectLeaderById(long leaderId) {
        return leaderMapper.selectLeaderById(leaderId);
    }

    @Override
    public List<Leader> selectLeaderDropdownList() {
        return leaderMapper.selectLeaderDropdownList();
    }

}
