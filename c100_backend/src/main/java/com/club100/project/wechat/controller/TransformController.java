package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.GpxFile;
import com.club100.project.wechat.utils.IOStreamUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/17 16:57:52
 * @Description: GPX 文件转换控制器。
 */
@RequestMapping("/v1/transform")
@RestController
@Slf4j
public class TransformController extends BaseController {

    private final String STRING_VALUE_AUTO_GENERATED = "AUTO_GENERATED";
    private final String STRING_CHARSET_VALUE_UTF8 = "UTF-8";
    private final String STRING_FILE_TYPE_VALUE_JSON = "json";

    @Autowired
    private QiniuService qiniuService;

    @GetMapping("/transformGpxFileToJson")
    public int transformGpxFileToJson(GpxFile gpxFile) {

        // Get GPX file's content via url.
        String gpxFileContentString = _GetGpxFileContentViaUrl(gpxFile.getGpxFileUrl());
//        log.info("gpxFileContentString: {}", gpxFileContentString);

        // Transform gpx into json and create json file.
        String outputFileNameString = _GenerateJsonFileFromGpxFile(gpxFileContentString);
        log.info("outputFileNameString: {}", outputFileNameString);

        /*// Make sure the gpx file contains the appropriate labels(<trk>/<trkseg>/<trkpt>).
        gpxFileContentString = _checkGpxFileLabel(gpxFileContentString);
        System.out.println("gpxFileContentString2: " + gpxFileContentString);

        // Make sure the gpx file contains elevation label(<ele>).
        gpxFileContentString = _addElevationLabel(gpxFileContentString); // TODO...
        System.out.println("gpxFileContentString3: " + gpxFileContentString);*/

        // Upload to 'Qiniu' cloud server.
        String qiniuCloudReturnedFileName = _UploadToQiNiu(outputFileNameString);
        log.info("qiniuCloudReturnedFileName: {}", qiniuCloudReturnedFileName);

        // Save json file's url in our database(route detail table).
//        _SaveJsonFileUrl();

        return 1;

    }

    /**
     * @param outputFileNameString
     * @return qiniuCloudReturnedFileName
     * @Description Upload to 'Qiniu' cloud server.
     */
    private String _UploadToQiNiu(String outputFileNameString) {

        return qiniuService.upload(outputFileNameString);

    }

    /**
     * @param gpxFileContentString
     * @return gpxFileContentString
     * @Description Transform gpx into json and create json file.
     */
    private String _GenerateJsonFileFromGpxFile(String gpxFileContentString) {

        // Initialization.
        String beforeString = "{\"segments\":[[";
        String afterString = "]]}";
        String openString = "[";
        String closeString = "]";

        String jsonString = XML.toJSONObject(gpxFileContentString).toString();
//        log.info("jsonString: {}", jsonString);

        String modifiedJsonString = beforeString + StringUtils.substringBetween(jsonString, openString, closeString) + afterString;
//        log.info("modifiedJsonString: {}", modifiedJsonString);

        return IOStreamUtils.S_outputContentToFile(modifiedJsonString, STRING_VALUE_AUTO_GENERATED, STRING_FILE_TYPE_VALUE_JSON, STRING_CHARSET_VALUE_UTF8);

    }

    /**
     * @param gpxFileContentString
     * @return gpxFileContentString
     * @Description Make sure the gpx file contains elevation label(<ele>).
     */
    private String _addElevationLabel(String gpxFileContentString) {

        String subStringGpxFileContentString = StringUtils.substringBetween(gpxFileContentString, "<trkseg>", "</trkseg>");
        System.out.println("subStringGpxFileContentString: " + subStringGpxFileContentString);

        if (!subStringGpxFileContentString.contains("ele"))
            System.out.println("Fuck!");

        return gpxFileContentString;

    }

    /**
     * @param gpxFileContentString
     * @return gpxFileContentString
     * @Description Make sure the gpx file contains the appropriate labels(<trk>/<trkseg>/<trkpt>).
     */
    private String _checkGpxFileLabel(String gpxFileContentString) {

        if (gpxFileContentString.contains("rte"))
            gpxFileContentString = gpxFileContentString.replace("rtept", "trkpt").replace("</rte>", "</trkseg></trk>").replace("<rte>", "<trk><trkseg>").replace("null", "");

        return gpxFileContentString;

    }

    /**
     * @param gpxFileUrlString
     * @return gpxFileContentString
     * @Description Get GPX file's content via url.
     */
    private String _GetGpxFileContentViaUrl(String gpxFileUrlString) {

        StringBuilder gpxFileContentStringBuilder = new StringBuilder();

        try {
            //建立连接
            URL url = new URL(gpxFileUrlString);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setRequestMethod("GET");
            httpUrlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            //获取输入流
            InputStream input = httpUrlConn.getInputStream();
            //将字节输入流转换为字符输入流
            InputStreamReader read = new InputStreamReader(input, "utf-8");
            //为字符输入流添加缓冲
            BufferedReader br = new BufferedReader(read);
            // 读取返回结果
            String data = br.readLine();
            while (data != null) {
                data = br.readLine();
                gpxFileContentStringBuilder.append(data + "\r\n");
            }
            // 释放资源
            br.close();
            read.close();
            input.close();
            httpUrlConn.disconnect();
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
            log.error("UnsupportedEncodingException");
        } catch (ProtocolException e) {
            e.printStackTrace();
            log.error("ProtocolException");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            log.error("MalformedURLException");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("IOException");
        }

        return gpxFileContentStringBuilder.toString();

    }

}
