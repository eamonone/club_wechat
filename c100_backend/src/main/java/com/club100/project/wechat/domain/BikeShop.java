package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 10:28:36
 * @Description: 车店 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BikeShop extends BaseEntity implements Serializable {

    private Long bikeShopId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long sysUserId; // 管理后台登录用户主键。
    private String bikeShopName; // 车店名称。
    private String bikeShopAvatarUrl; // 车店头像链接 URL。
    private String bikeShopPictureUrl; // 车店图片链接 URL。
    private String bikeShopAddress; // 车店地址。
    private String bikeShopContact; // 车店联系方式。
    private String bikeShopProfile; // 车店简介。
    private String subMchId; // 子商户号（特约商户绑定后生成）。

    public String toJson(BikeShop bikeShop) {

        // BikeShop 对象转 JSON。
        return JSON.toJSONString(bikeShop);

    }

}
