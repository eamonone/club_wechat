package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.OrderActivity;
import com.club100.project.wechat.mapper.OrderActivityMapper;
import com.club100.project.wechat.service.IOrderActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 18:33:55
 * @Description: 我的活动服务实现类。
 */
@Slf4j
@Service
public class OrderActivityServiceImpl implements IOrderActivityService {

    @Autowired
    private OrderActivityMapper orderActivityMapper;

    @Override
    public List<OrderActivity> selectOrderActivityList(OrderActivity orderActivity) {
        return orderActivityMapper.selectOrderActivityList(orderActivity);
    }

}
