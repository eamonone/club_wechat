package com.club100.project.wechat.utils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

import com.ijpay.core.kit.PayKit;
import okhttp3.HttpUrl;


public class Auth {

	// Authorization: <schema> <token>
// GET - getToken("GET", httpurl, "")
// POST - getToken("POST", httpurl, json)
//	String schema = "WECHATPAY2-SHA256-RSA2048";
//	HttpUrl httpurl = HttpUrl.parse(url);

	String getToken(String method, HttpUrl url, String body) throws Exception {
		String nonceStr = "your nonce string";
		long timestamp = System.currentTimeMillis() / 1000;
		String message = buildMessage(method, url, timestamp, nonceStr, body);
		String signature = sign(message.getBytes("utf-8"));

		return "mchid=\"" + "1601325618" + "\","
			+ "nonce_str=\"" + nonceStr + "\","
			+ "timestamp=\"" + timestamp + "\","
			+ "serial_no=\"" + "7DA28B0997C1EA25C4943595A5CD1D2C3022C50C" + "\","
			+ "signature=\"" + signature + "\"";
	}

	String sign(byte[] message) throws Exception {
		Signature sign = Signature.getInstance("SHA256withRSA");
    PrivateKey privateKey = PayKit.getPrivateKey("/Users/hujiwei/Desktop/keystore/apiclient_key.pem");
		sign.initSign(privateKey);
		sign.update(message);

		return Base64.getEncoder().encodeToString(sign.sign());
	}

	String buildMessage(String method, HttpUrl url, long timestamp, String nonceStr, String body) {
		String canonicalUrl = url.encodedPath();
		if (url.encodedQuery() != null) {
			canonicalUrl += "?" + url.encodedQuery();
		}

		return method + "\n"
			+ canonicalUrl + "\n"
			+ timestamp + "\n"
			+ nonceStr + "\n"
			+ body + "\n";
	}

  public static void main(String[] args) throws Exception {
    //
    String result = new Auth().getToken("get", HttpUrl.parse("https://api.mch.weixin.qq.com/v3/certificates"),"");
    System.out.println(result);
  }
}
