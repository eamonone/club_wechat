package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:28:04
 * @Description: 用户 Entity。
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity implements Serializable {

    private Long userId; // Primary Key ID。

    private Integer markTimes; // 用户打卡次数。
    private Integer state; // 用户状态（1：可用；2：废弃。）。
    private Integer userCity; // 用户选择城市（-1：默认、未选择；0：Other Locations；1：上海；2：北京；3：广州。）。
    private Integer wxUserGender; // 用户微信性别。
    private String wxNickName; // 用户微信名。
    private String wxOpenId; // 用户微信小程序 ID。
    private String wxUnionId; // 用户微信唯一 ID。
    private String wxUserAvatarUrl; // 用户微信头像 URL。
    private String wxUserCity; // 用户微信城市。
    private String wxUserCountry; // 用户微信国家。
    private String wxUserProvince; // 用户微信省份。

    public String toJson(User user) {

        // User 对象转 JSON。
        return JSON.toJSONString(user);

    }

}
