package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RouteAndDetail;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 19:40:14
 * @Description: 路线和路线详情服务类。
 */
public interface IRouteAndDetailService {

    /**
     * 列举路线和路线详情信息。
     *
     * @param routeAndDetail
     * @return
     */
    RouteAndDetail selectRouteAndDetail(RouteAndDetail routeAndDetail);

    /**
     * 列举路线和路线详情信息。
     *
     * @param routeAndDetail
     * @return
     */
    List<RouteAndDetail> selectRouteAndDetail2(RouteAndDetail routeAndDetail);

}
