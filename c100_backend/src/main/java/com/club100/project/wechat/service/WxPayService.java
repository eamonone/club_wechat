package com.club100.project.wechat.service;

import com.club100.common.exception.CustomException;
import com.club100.project.wechat.domain.ClubOrder;
import com.club100.project.wechat.domain.ClubRefund;
import com.club100.project.wechat.domain.ClubRefundPolicyResponse;
import com.club100.project.wechat.wxpay.sdk.vo.WxPayNotifyV0;

import java.util.Map;

public interface WxPayService {

  Map<String,String> wxPay(Long productId, Long userId, String ipAddress) throws Exception;

  Map<String,String> orderQuery(String outTradeNo, String subMchId) throws Exception;

  void orderStatus(Integer id, Integer status, Long insuranceId) throws Exception;

  ClubOrder orderDetailQuery(Long orderId) throws Exception;

  Map<String,String> closeOrder(String outTradeNo) throws Exception;

  ClubRefundPolicyResponse getRefundMoney(Integer orderId) throws CustomException;

  void refund(Integer orderId) throws Exception;

  ClubRefund refundDetail(Integer orderId) throws Exception;

  Map<String,String> downloadBill(String billDate) throws Exception;

  String paySuccessNotify(WxPayNotifyV0 param) throws Exception;
}
