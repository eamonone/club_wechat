package com.club100.project.wechat.controller;

import com.club100.common.utils.ServletUtils;
import com.club100.framework.security.LoginUser;
import com.club100.framework.security.service.TokenService;
import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.system.domain.SysUser;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.IActivityService;
import com.club100.project.wechat.service.IRefundPolicyService;
import com.club100.project.wechat.utils.EmptyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/10 11:36:14
 * @Description: 退款政策控制器。
 */
@RequestMapping("/v1/refundpolicy")
@RestController
public class RefundPolicyController extends BaseController {

    // Activity information.
    @Autowired
    private IActivityService activityService;
    // Refund policy information.
    @Autowired
    private IRefundPolicyService refundPolicyService;

    /**
     * 查询退款政策列表。
     */
    @GetMapping("/listRefundPolicy")
    public TableDataInfo listRefundPolicy(RefundPolicy refundPolicy) {

//        startPage();
        List<ReturnRefundPolicyInfo> returnedRefundPolicyInfoList = new ArrayList<>();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        refundPolicy.setSysUserId(sysUserIdLong);
        List<RefundPolicy> refundPoliciesList = refundPolicyService.selectRefundPolicyListWithPrivilege(refundPolicy);

        // 分组。
        Map<Long, List<RefundPolicy>> groupByActivityIdMap = refundPoliciesList.stream().collect(Collectors.groupingBy(RefundPolicy::getActivityId));

        // 遍历分组并处理数据。
        for (Map.Entry<Long, List<RefundPolicy>> entryRefundPolicy : groupByActivityIdMap.entrySet()) {
            Long key = entryRefundPolicy.getKey();

            String activityNameString = P_GetActivityNameById(key);
            List<RefundPolicy> entryRefundPolicyList = entryRefundPolicy.getValue();

            StringBuilder refundPolicyStringBuilder = new StringBuilder();

            for (RefundPolicy eachRefundPolicy : entryRefundPolicyList) {

                String refundPolicyContentString = eachRefundPolicy.getPolicyDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 前退款: " + (100 * eachRefundPolicy.getRefundPercentage()) + "%;\r\n";

                if (refundPolicyStringBuilder.length() == 0) {
                    refundPolicyStringBuilder.append(refundPolicyContentString);
                } else {
                    refundPolicyStringBuilder.append(",").append(refundPolicyContentString);
                }
            }

            String[] refundPolicyStringArray = String.valueOf(refundPolicyStringBuilder).split(",");

            ReturnRefundPolicyInfo returnRefundPolicyInfo = new ReturnRefundPolicyInfo(key, activityNameString, refundPolicyStringArray);
            returnedRefundPolicyInfoList.add(returnRefundPolicyInfo);

        }

        return getDataTable(returnedRefundPolicyInfoList);

    }

    /**
     * 删除退款政策信息。
     */
    @DeleteMapping("/{returnRefundPolicyInfoIds}")
    public AjaxResult deleteRefundPolicy(@PathVariable Long[] returnRefundPolicyInfoIds) {

        return toAjax(refundPolicyService.deleteRefundPolicy(returnRefundPolicyInfoIds));

    }

    /**
     * 根据活动编号获取退款政策详细信息。
     */
    @RequestMapping(value = "/getRefundPolicyById")
    public AjaxResult getRefundPolicyById(@RequestBody Long returnRefundPolicyInfoId) {

        RefundPolicy searchedRefundPolicyEntity = new RefundPolicy();
        searchedRefundPolicyEntity.setActivityId(returnRefundPolicyInfoId);
        ReceivedRefundPolicyInfo receivedRefundPolicyInfo = new ReceivedRefundPolicyInfo();

        receivedRefundPolicyInfo.setReturnRefundPolicyInfoId(returnRefundPolicyInfoId);
        String activityNameString = P_GetActivityNameById(returnRefundPolicyInfoId);
        receivedRefundPolicyInfo.setReturnActivityNameInfo(activityNameString);

        List<RefundPolicy> refundPoliciesList = refundPolicyService.selectRefundPolicyList(searchedRefundPolicyEntity);

        AtomicInteger count = new AtomicInteger();

        refundPoliciesList.forEach(eachRefundPolicyEntity -> {
            count.incrementAndGet();
            if (count.get() == refundPoliciesList.size()) {
                receivedRefundPolicyInfo.setRefundPercentage6((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                receivedRefundPolicyInfo.setRefundTime6(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
            } else {
                switch (count.get()) {
                    case 1:
                        receivedRefundPolicyInfo.setRefundPercentage1((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                        receivedRefundPolicyInfo.setRefundTime1(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
                        break;
                    case 2:
                        receivedRefundPolicyInfo.setRefundPercentage2((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                        receivedRefundPolicyInfo.setRefundTime2(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
                        break;
                    case 3:
                        receivedRefundPolicyInfo.setRefundPercentage3((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                        receivedRefundPolicyInfo.setRefundTime3(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
                        break;
                    case 4:
                        receivedRefundPolicyInfo.setRefundPercentage4((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                        receivedRefundPolicyInfo.setRefundTime4(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
                        break;
                    case 5:
                        receivedRefundPolicyInfo.setRefundPercentage5((int) (eachRefundPolicyEntity.getRefundPercentage() * 100));
                        receivedRefundPolicyInfo.setRefundTime5(P_LocalDateTimeToLocalDate(eachRefundPolicyEntity.getPolicyDate()));
                        break;
                    default:
                        break;
                }
            }
        });

        return AjaxResult.success(receivedRefundPolicyInfo);

    }

    @RequestMapping("/addRefundPolicy")
    public int addRefundPolicy(@RequestBody ReceivedRefundPolicyInfo receivedRefundPolicyInfo) {

        List<RefundPolicy> insertedRefundPolicyList = P_InsertRefundPolicyInBatch(receivedRefundPolicyInfo, "insert");

        return refundPolicyService.insertRefundPolicyInBatch(insertedRefundPolicyList);

    }

    @RequestMapping("/updateRefundPolicy")
    public int updateRefundPolicy(@RequestBody ReceivedRefundPolicyInfo receivedRefundPolicyInfo) {

        int deletedCount = refundPolicyService.deleteRefundPolicy(Long.valueOf(receivedRefundPolicyInfo.getReturnRefundPolicyInfoId()));

        List<RefundPolicy> insertedRefundPolicyList;
        int insertRowsInt = 0;
        if (deletedCount > 0) {
            insertedRefundPolicyList = P_InsertRefundPolicyInBatch(receivedRefundPolicyInfo, "update");
            insertRowsInt = refundPolicyService.insertRefundPolicyInBatch(insertedRefundPolicyList);
        }

        return insertRowsInt;

    }

    /**
     * Get activity name by activity Id.
     *
     * @param activityId
     * @return
     */
    private String P_GetActivityNameById(long activityId) {

        Activity searchedActivityEntity = activityService.selectActivityById(activityId);

        return searchedActivityEntity.getActivityName();

    }

    /**
     * Transform LocalDate into LocalDateTime.
     *
     * @param transformedLocalDate
     * @param pattern
     * @return
     */
    private LocalDateTime P_LocalDateToLocalDateTime(LocalDate transformedLocalDate, String pattern) {

        return LocalDateTime.parse(transformedLocalDate + " 00:00:00", DateTimeFormatter.ofPattern(pattern));

    }

    /**
     * Transform LocalDateTime into LocalDate.
     *
     * @param transformedLocalDateTime
     * @return
     */
    private LocalDate P_LocalDateTimeToLocalDate(LocalDateTime transformedLocalDateTime) {

        return transformedLocalDateTime.toLocalDate();

    }

    /**
     * Fill in refund policy list.
     *
     * @param refundPercentageInteger
     * @param insertedRefundPolicyList
     * @param refundDateLocalDate
     * @param activityIdLong
     * @param patternString
     * @return
     */
    private List<RefundPolicy> P_FillInRefundPolicyList(Integer refundPercentageInteger, List<RefundPolicy> insertedRefundPolicyList, LocalDate refundDateLocalDate, Long activityIdLong, String patternString) {

        if (EmptyUtils.isNotEmpty(refundDateLocalDate) && EmptyUtils.isNotEmpty(refundPercentageInteger)) {
            RefundPolicy refundPolicy = new RefundPolicy();
            refundPolicy.setActivityId(activityIdLong);
            refundPolicy.setPolicyDate(P_LocalDateToLocalDateTime(refundDateLocalDate, patternString));
            refundPolicy.setRefundPercentage(Double.valueOf(refundPercentageInteger) / 100.0);
            insertedRefundPolicyList.add(refundPolicy);
        }

        return insertedRefundPolicyList;

    }

    private List<RefundPolicy> P_InsertRefundPolicyInBatch(ReceivedRefundPolicyInfo receivedRefundPolicyInfo, String actionString) {

        List<RefundPolicy> insertedRefundPolicyList = new ArrayList<>();
        Long activityIdLong = null;

        if ("insert".equalsIgnoreCase(actionString)) {
            activityIdLong = Long.valueOf(receivedRefundPolicyInfo.getReturnActivityNameInfo());
        } else if ("update".equalsIgnoreCase(actionString)) {
            activityIdLong = receivedRefundPolicyInfo.getReturnRefundPolicyInfoId();
        }

        String patternString = "yyyy-MM-dd HH:mm:ss";

        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage1(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime1(), activityIdLong, patternString);
        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage2(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime2(), activityIdLong, patternString);
        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage3(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime3(), activityIdLong, patternString);
        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage4(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime4(), activityIdLong, patternString);
        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage5(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime5(), activityIdLong, patternString);
        insertedRefundPolicyList = P_FillInRefundPolicyList(receivedRefundPolicyInfo.getRefundPercentage6(), insertedRefundPolicyList, receivedRefundPolicyInfo.getRefundTime6(), activityIdLong, patternString);

        return insertedRefundPolicyList;

    }

}
