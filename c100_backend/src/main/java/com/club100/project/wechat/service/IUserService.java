package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.User;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 20:18:28
 * @Description: 用户服务类。
 */
public interface IUserService {
    /**
     * 插入用户信息。
     *
     * @param user
     * @return
     */
    int insert(User user);

    /**
     * 使用主键查询用户信息。
     *
     * @param user
     * @return
     */
    User selectOneById(User user);

    /**
     * 列举一条用户信息。
     *
     * @param user
     * @return
     */
    User selectUser(User user);

    /**
     * 修改用户城市信息。
     *
     * @param user
     * @return
     */
    int updateUserCity(User user);

    /**
     * 修改用户打卡次数信息。
     *
     * @param user
     * @return
     */
    int updateUserMarkTimes(User user);

    /**
     * 删除用户信息。
     *
     * @param id(s)
     * @return
     */
    int deleteByUserIds(Long... id);

}
