package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Card;
import com.club100.project.wechat.domain.Route;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:34:47
 * @Description: 路线 Mapper 接口。
 */
public interface RouteMapper {

    int insertRoute(Route route);

    int insertOneRoute(Route route);

    List<Route> selectRouteAtMostTwoList(Route route);

    List<Route> selectRouteList(@Param("cityType") Integer cityType, @Param("routeDistance1Begin") Integer routeDistance1Begin, @Param("routeDistance1End") Integer routeDistance1End, @Param("routeDistance2Begin") Integer routeDistance2Begin, @Param("routeDistance2End") Integer routeDistance2End, @Param("routeDistance3Begin") Integer routeDistance3Begin, @Param("routeDistance3End") Integer routeDistance3End, @Param("routeDistance4Begin") Integer routeDistance4Begin, @Param("routeDistance4End") Integer routeDistance4End, @Param("routeDistance5Begin") Integer routeDistance5Begin, @Param("routeDistance5End") Integer routeDistance5End, @Param("routeDistance6Begin") Integer routeDistance6Begin, @Param("routeDistance6End") Integer routeDistance6End, @Param("routeTerrain") Integer[] routeTerrain, @Param("routeType") Integer[] routeType);

    List<Route> selectByRouteIds(RowBounds rowBounds, Long... routeIds);

    int deleteByRouteIds(Long... id);

    Route getLatestRouteId();

    int updateRoute(Route route);

}
