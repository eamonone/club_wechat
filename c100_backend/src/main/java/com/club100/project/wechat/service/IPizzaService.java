package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Pizza;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/01 15:03:32
 * @Description: 披萨服务类。
 */
public interface IPizzaService {

    List<Pizza> selectPizzaList(Pizza pizza);

}
