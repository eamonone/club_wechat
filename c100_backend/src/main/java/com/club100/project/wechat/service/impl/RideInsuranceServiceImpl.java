package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Insurance;
import com.club100.project.wechat.mapper.RideInsuranceMapper;
import com.club100.project.wechat.service.IRideInsuranceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/11 21:38:44
 * @Description: 骑行意外险服务实现类。
 */
@Slf4j
@Service
public class RideInsuranceServiceImpl implements IRideInsuranceService {

    @Autowired
    private RideInsuranceMapper rideInsuranceMapper;

    @Override
    public int insertInsurance(Insurance insurance) {
        return rideInsuranceMapper.insertInsurance(insurance);
    }

    @Override
    public Insurance selectInsuranceById(Long insuranceId) {
        return rideInsuranceMapper.selectInsuranceById(insuranceId);
    }

}
