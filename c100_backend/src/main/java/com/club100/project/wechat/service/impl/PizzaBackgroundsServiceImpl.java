package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.PizzaBackgrounds;
import com.club100.project.wechat.mapper.PizzaBackgroundsMapper;
import com.club100.project.wechat.service.IPizzaBackgroundsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 14:33:44
 * @Description: 披萨基本情况服务实现类。
 */
@Slf4j
@Service
public class PizzaBackgroundsServiceImpl implements IPizzaBackgroundsService {

    @Autowired
    private PizzaBackgroundsMapper pizzaBackgroundsMapper;

    @Override
    public int insertPizzaBackgrounds(PizzaBackgrounds pizzaBackgrounds) {
        return pizzaBackgroundsMapper.insertPizzaBackgrounds(pizzaBackgrounds);
    }

    @Override
    public PizzaBackgrounds selectPizzaBackgroundsById(Long pizzaBackgroundsId) {
        return pizzaBackgroundsMapper.selectPizzaBackgroundsById(pizzaBackgroundsId);
    }

}
