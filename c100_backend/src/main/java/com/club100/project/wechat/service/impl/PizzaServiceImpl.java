package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Pizza;
import com.club100.project.wechat.mapper.PizzaMapper;
import com.club100.project.wechat.service.IPizzaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/01 15:05:31
 * @Description: 披萨服务实现类。
 */
@Slf4j
@Service
public class PizzaServiceImpl implements IPizzaService {

    @Autowired
    private PizzaMapper pizzaMapper;

    @Override
    public List<Pizza> selectPizzaList(Pizza pizza) {
        return pizzaMapper.selectPizzaList(pizza);
    }

}
