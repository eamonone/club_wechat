package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Inspire;
import com.club100.project.wechat.mapper.InspireMapper;
import com.club100.project.wechat.service.IInspireService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 15:44:56
 * @Description: 激励服务实现类。
 */
@Slf4j
@Service
public class InspireServiceImpl implements IInspireService {

    @Autowired
    private InspireMapper inspireMapper;

    @Override
    public List<Inspire> selectInspireList() {
        return inspireMapper.selectInspireList();
    }

    @Override
    public int insert(Inspire inspire) {
        return inspireMapper.insertInspire(inspire);
    }

    @Override
    public List<Inspire> selectInspireAtMostTwoList(Inspire inspire) {
        return inspireMapper.selectInspireAtMostTwoList(inspire);
    }

    @Override
    @Transactional
    public int deleteByInspireIds(Long... id) {
        return inspireMapper.deleteByInspireIds(id);
    }

}
