package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.ActivityRoute;
import com.club100.project.wechat.service.IActivityRouteService;
import com.club100.project.wechat.utils.EmptyUtils;
import com.club100.project.wechat.utils.IOStreamUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/19 11:42:02
 * @Description: 活动路线控制器。
 */
@RequestMapping("/v1/activityroute")
@RestController
@Slf4j
public class ActivityRouteController extends BaseController {

    private final String STRING_VALUE_AUTO_GENERATED = "AUTO_GENERATED";
    private final String STRING_CHARSET_VALUE_UTF8 = "UTF-8";
    private final String STRING_FILE_TYPE_VALUE_JSON = "json";

    // QiNiu cloud server information.
    @Autowired
    private QiniuService qiniuService;
    // Activity route information.
    @Autowired
    private IActivityRouteService activityRouteService;

    /**
     * 获取活动路线列表。
     */
    @GetMapping("/listActivityRoute")
    public TableDataInfo listActivityRoute(ActivityRoute activityRoute) {

        startPage();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activityRoute.setSysUserId(sysUserIdLong);

        List<ActivityRoute> activityRoutesList = activityRouteService.selectActivityRouteWithPrivilege(activityRoute);

        return getDataTable(activityRoutesList);

    }

    /**
     * 删除活动路线信息。
     *
     * @param activityRouteIds
     * @return
     */
    @DeleteMapping("/{activityRouteIds}")
    public AjaxResult deleteByActivityRouteIds(@PathVariable Long[] activityRouteIds) {

        return toAjax(activityRouteService.deleteByActivityRouteIds(activityRouteIds));

    }

    /**
     * 添加活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    @RequestMapping("/addActivityRoute")
    public int addActivityRoute(@RequestBody ActivityRoute activityRoute) {

        String routeGpxFileUrlString = activityRoute.getRouteGpxFileUrl();

        // Get GPX file's content via url.
        String gpxFileContentString = _GetGpxFileContentViaUrl(routeGpxFileUrlString);
//        log.info("gpxFileContentString: {}", gpxFileContentString);

        // Transform gpx into json and create json file.
        String outputFileNameString = _GenerateJsonFileFromGpxFile(gpxFileContentString);
//        log.info("outputFileNameString: {}", outputFileNameString);

        // Upload to 'Qiniu' cloud server.
        String qiniuCloudReturnedFileName = _UploadToQiNiu(outputFileNameString);
//        log.info("qiniuCloudReturnedFileName: {}", qiniuCloudReturnedFileName);

        String routeJsonFileUrlString = "https://images.club100.cn/" + qiniuCloudReturnedFileName;
        activityRoute.setRouteJsonFileUrl(routeJsonFileUrlString);

        if (EmptyUtils.isNotEmpty(activityRoute.getRouteClimb()) && EmptyUtils.isNotEmpty(activityRoute.getRouteDistance())) {
            Double difficultyLevelDouble = P_CalculateDifficultyLevel(activityRoute.getRouteClimb(), activityRoute.getRouteDistance());
            activityRoute.setRouteDifficultyLevel(difficultyLevelDouble);
        }

        return activityRouteService.insertActivityRoute(activityRoute);

    }

    /**
     * 根据活动路线编号获取详细信息。
     *
     * @param activityRouteId
     * @return
     */
    @RequestMapping(value = "/getActivityRouteById")
    public AjaxResult getActivityRouteById(@RequestBody Long activityRouteId) {

        return AjaxResult.success(activityRouteService.selectActivityRouteById(activityRouteId));

    }

    /**
     * 修改活动路线信息。
     *
     * @param activityRoute
     * @return
     */
    @RequestMapping(value = "/updateActivityRoute")
    public int updateActivityRoute(@RequestBody ActivityRoute activityRoute) {

        if (EmptyUtils.isNotEmpty(activityRoute.getRouteClimb()) && EmptyUtils.isNotEmpty(activityRoute.getRouteDistance())) {
            Double difficultyLevelDouble = P_CalculateDifficultyLevel(activityRoute.getRouteClimb(), activityRoute.getRouteDistance());
            activityRoute.setRouteDifficultyLevel(difficultyLevelDouble);
        }

        return activityRouteService.updateActivityRoute(activityRoute);

    }

    /**
     * @param gpxFileContentString
     * @return gpxFileContentString
     * @Description Transform gpx into json and create json file.
     */
    private String _GenerateJsonFileFromGpxFile(String gpxFileContentString) {

        // Initialization.
        String beforeString = "{\"segments\":[[";
        String afterString = "]]}";
        String openString = "[";
        String closeString = "]";

        String jsonString = XML.toJSONObject(gpxFileContentString).toString();
//        log.info("jsonString: {}", jsonString);

        String modifiedJsonString = beforeString + StringUtils.substringBetween(jsonString, openString, closeString) + afterString;
//        log.info("modifiedJsonString: {}", modifiedJsonString);

        return IOStreamUtils.S_outputContentToFile(modifiedJsonString, STRING_VALUE_AUTO_GENERATED, STRING_FILE_TYPE_VALUE_JSON, STRING_CHARSET_VALUE_UTF8);

    }

    /**
     * @param gpxFileUrlString
     * @return gpxFileContentString
     * @Description Get GPX file's content via url.
     */
    private String _GetGpxFileContentViaUrl(String gpxFileUrlString) {

        StringBuilder gpxFileContentStringBuilder = new StringBuilder();

        try {
            //建立连接
            URL url = new URL(gpxFileUrlString);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setRequestMethod("GET");
            httpUrlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            //获取输入流
            InputStream input = httpUrlConn.getInputStream();
            //将字节输入流转换为字符输入流
            InputStreamReader read = new InputStreamReader(input, "utf-8");
            //为字符输入流添加缓冲
            BufferedReader br = new BufferedReader(read);
            // 读取返回结果
            String data = br.readLine();
            while (data != null) {
                data = br.readLine();
                gpxFileContentStringBuilder.append(data + "\r\n");
            }
            // 释放资源
            br.close();
            read.close();
            input.close();
            httpUrlConn.disconnect();
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
            log.error("UnsupportedEncodingException");
        } catch (ProtocolException e) {
            e.printStackTrace();
            log.error("ProtocolException");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            log.error("MalformedURLException");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("IOException");
        }

        return gpxFileContentStringBuilder.toString();

    }

    /**
     * @param outputFileNameString
     * @return qiniuCloudReturnedFileName
     * @Description Upload to 'Qiniu' cloud server.
     */
    private String _UploadToQiNiu(String outputFileNameString) {

        return qiniuService.upload(outputFileNameString);

    }

    /**
     * @param climbDouble
     * @param distanceDouble
     * @return difficultyLevel
     * @Description Calculate the difficulty level with climb and distance.
     */
    private Double P_CalculateDifficultyLevel(Double climbDouble, Double distanceDouble) {

        Double difficultyLevelDouble;

        if (EmptyUtils.isEmpty(climbDouble) || EmptyUtils.isEmpty(distanceDouble)) {
            difficultyLevelDouble = -1.00;
        } else {
            if ((0 <= climbDouble && climbDouble <= 200) && (0 <= distanceDouble && distanceDouble <= 35)) {
                difficultyLevelDouble = 1.00;
            } else if ((0 <= climbDouble && climbDouble <= 200) && (35 < distanceDouble && distanceDouble <= 90) || (200 < climbDouble && climbDouble <= 300) && (0 <= distanceDouble && distanceDouble <= 35)) {
                difficultyLevelDouble = 2.00;
            } else if ((0 <= climbDouble && climbDouble <= 300) && (90 < distanceDouble && distanceDouble <= 120) || (300 < climbDouble && climbDouble <= 800) && (0 <= distanceDouble && distanceDouble <= 90) || (200 < climbDouble && climbDouble <= 300) && (35 < distanceDouble && distanceDouble <= 90)) {
                difficultyLevelDouble = 3.00;
            } else if ((0 <= climbDouble && climbDouble <= 500) && (120 < distanceDouble && distanceDouble <= 170) || (300 < climbDouble && climbDouble <= 800) && (90 < distanceDouble && distanceDouble <= 120) || (800 < climbDouble && climbDouble <= 1500) && (0 <= distanceDouble && distanceDouble <= 90)) {
                difficultyLevelDouble = 4.00;
            } else {
                difficultyLevelDouble = 5.00;
            }
        }

        return difficultyLevelDouble;

    }

}