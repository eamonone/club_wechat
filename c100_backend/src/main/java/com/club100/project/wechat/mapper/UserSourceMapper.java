package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.UserSource;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/25 14:12:32
 * @Description: 用户来源 Mapper 接口。
 */
public interface UserSourceMapper {

    int insertUserSource(UserSource userSource);

    UserSource selectUserSourceById(Long userSourceId);

    List<UserSource> selectUserSource(UserSource userSource);

    int updateUserSource(UserSource userSource);

}
