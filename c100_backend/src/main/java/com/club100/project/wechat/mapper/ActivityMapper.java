package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Activity;
import com.club100.project.wechat.domain.ActivityExt;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 15:28:20
 * @Description: 活动 Mapper 接口。
 */
public interface ActivityMapper {

    int insertActivity(Activity activity);

    List<Activity> selectActivityListWithConditions(Activity activity, Integer singleDay, Integer multiDays, Integer notStarted, Integer inProgress, Integer hasEnded, LocalDate currentDate, Integer free, Integer charge, Integer userCity);

    Activity selectActivityById(Long activityId);

    Activity selectByPrimaryKey(Long id);

    ActivityExt selectActivityByProductId(@Param("productId") Long productId);

    List<Activity> selectActivityListWithOrders(Integer tabSequence, LocalDate currentDate, Long bikeShopId);

    List<Activity> selectActivityList(Activity activity);

    List<Activity> selectActivityListWithSysUserId(Activity activity);

    int updateActivity(Activity activity);

    int deleteActivityByIds(Long... activityId);

    List<Activity> selectActivityDropdownList(Activity activity);

    List<Activity> selectActivityAtMostTwoList(Activity activity);

    Activity selectActivityQrcodeUrl(Long activityId);

    List<Activity> selectActivityByBeginTime();

}
