package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.MarkPicture;
import com.club100.project.wechat.mapper.MarkPictureMapper;
import com.club100.project.wechat.service.IMarkPictureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/29 15:01:09
 * @Description: 打卡图片服务实现类。
 */
@Slf4j
@Service
public class MarkPictureServiceImpl implements IMarkPictureService {

    @Autowired
    private MarkPictureMapper markPictureMapper;

    @Override
    public MarkPicture selectLastPicture(MarkPicture markPicture) {
        return markPictureMapper.selectLastPicture(markPicture);
    }

    @Override
    public int insertMarkPicture(MarkPicture markPicture) {
        return markPictureMapper.insertMarkPicture(markPicture);
    }

    @Override
    public int insertMarkPictureInBatch(List<MarkPicture> markPictureList) {
        return markPictureMapper.insertMarkPictureInBatch(markPictureList);
    }

}
