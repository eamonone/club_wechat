package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Route;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 16:24:23
 * @Description: 路线服务类。
 */
public interface IRouteService {
    /**
     * 插入路线信息。
     *
     * @param route
     * @return
     */
    int insert(Route route);

    int insertOneRoute(Route route);

    /**
     * 列举路线信息（最多两条）。
     *
     * @param route
     * @return
     */
    List<Route> selectRouteAtMostTwoList(Route route);

    /**
     * 列举路线信息。
     *
     * @param cityType
     * @param routeDistance1Begin
     * @param routeDistance1End
     * @param routeDistance2Begin
     * @param routeDistance2End
     * @param routeDistance3Begin
     * @param routeDistance3End
     * @param routeDistance4Begin
     * @param routeDistance4End
     * @param routeDistance5Begin
     * @param routeDistance5End
     * @param routeDistance6Begin
     * @param routeDistance6End
     * @param routeTerrain
     * @param routeType
     * @return
     */
    List<Route> selectRouteList(Integer cityType, Integer routeDistance1Begin, Integer routeDistance1End, Integer routeDistance2Begin, Integer routeDistance2End, Integer routeDistance3Begin, Integer routeDistance3End, Integer routeDistance4Begin, Integer routeDistance4End, Integer routeDistance5Begin, Integer routeDistance5End, Integer routeDistance6Begin, Integer routeDistance6End, Integer[] routeTerrain, Integer[] routeType);

    /**
     * 删除路线信息。
     *
     * @param routeIds
     * @return
     */
    List<Route> selectByRouteIds(RowBounds rowBounds, Long... routeIds);

    /**
     * 删除路线信息。
     *
     * @param id
     * @return
     */
    int deleteByRouteIds(Long... id);

    Route getLatestRouteId();

    int updateRoute(Route route);

}
