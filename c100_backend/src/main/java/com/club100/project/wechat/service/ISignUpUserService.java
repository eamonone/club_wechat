package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.SignUpUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/19 22:59:44
 * @Description: 报名用户服务类。
 */
public interface ISignUpUserService {

    /**
     * 列举报名用户信息。
     *
     * @param signUpUser
     * @return
     */
    List<SignUpUser> selectSignUpUser(SignUpUser signUpUser);

}
