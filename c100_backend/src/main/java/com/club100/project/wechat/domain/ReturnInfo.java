package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/22 12:19:40
 * @Description: 返回值 Entity。
 */
public class ReturnInfo implements Serializable {

    public Integer code;
    public String message;
    public String data;

    public String toJson(ReturnInfo returnInfo) {

        // ReturnInfo 对象转 JSON。
        return JSON.toJSONString(returnInfo);

    }

}
