package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Source;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/30 19:33:08
 * @Description: 二维码参数统计 Mapper 接口。
 */
public interface SourceMapper {

    List<Source> selectSourceList(Source source);

    int insertSource(Source source);

    int updateSource(Source source);

    int deleteBySourceIds(Long... sourceId);

}
