package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 21:26:27
 * @Description: 免责声明 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Disclaimer extends BaseEntity implements Serializable {

    private Long disclaimerId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private String disclaimerContent; // 免责声明内容。

    public String toJson(Disclaimer disclaimer) {

        // Disclaimer 对象转 JSON。
        return JSON.toJSONString(disclaimer);

    }

}
