package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.SignUpActivity;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/13 09:51:51
 * @Description: 报名活动 Mapper 接口。
 */
public interface SignUpActivityMapper {

    List<SignUpActivity> selectSignUpActivity(SignUpActivity signUpActivity);

}
