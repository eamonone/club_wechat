package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.ActivityDetail;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/21 18:03:37
 * @Description: 活动详情服务类。
 */
public interface IActivityDetailService {

    /**
     * 根据主键 ID 查询活动详情。
     *
     * @param activityDetail
     * @return
     */
    ActivityDetail selectActivityDetailById(ActivityDetail activityDetail);

}
