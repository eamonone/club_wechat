package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Logistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 11:18:31
 * @Description: 后勤保障服务类。
 */
public interface ILogisticsService {

    /**
     * 插入一条后勤保障信息。
     *
     * @param logistics
     * @return
     */
    int insertLogistics(Logistics logistics);

    /**
     * 根据主键查询一条后勤保障信息。
     *
     * @param logisticsId
     * @return
     */
    Logistics selectLogisticsById(Long logisticsId);

    /**
     * 根据后勤保障名称查询一条后勤保障信息。
     *
     * @param logisticsName
     * @return
     */
    Logistics selectLogisticsIdByName(String logisticsName);

    /**
     * 查询后勤保障信息。
     *
     * @param logistics
     * @return
     */
    List<Logistics> selectLogisticsList(Logistics logistics);

    /**
     * 修改后勤保障信息。
     *
     * @param logistics
     * @return
     */
    int updateLogistics(Logistics logistics);

    /**
     * 删除后勤保障信息。
     *
     * @param logisticsId(s)
     * @return
     */
    int deleteByLogisticsIds(Long... logisticsId);

    /**
     * 查询后勤保障多选下拉框选项。
     *
     * @return
     */
    List<Logistics> selectLogisticsDropdownList();

}
