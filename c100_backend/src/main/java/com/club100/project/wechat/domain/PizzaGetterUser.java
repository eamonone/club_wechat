package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 15:17:15
 * @Description: 已获披萨用户 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PizzaGetterUser extends BaseEntity implements Serializable {

    private Long pizzaGetterUserId; // Primary Key ID。

    private Long userId; // 用户主键。
    private String weChatId; // 微信 ID。
    private String wxUserAvatarUrl; // 用户微信头像 URL。

    public String toJson(PizzaGetterUser pizzaGetterUser) {

        // PizzaGetterUser 对象转 JSON。
        return JSON.toJSONString(pizzaGetterUser);

    }

}
