package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.OrderActivity;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 18:13:41
 * @Description: 我的活动 Mapper 接口。
 */
public interface OrderActivityMapper {

    List<OrderActivity> selectOrderActivityList(OrderActivity orderActivity);

}
