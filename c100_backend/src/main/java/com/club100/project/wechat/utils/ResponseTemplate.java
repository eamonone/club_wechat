package com.club100.project.wechat.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplate<T> {

  @ApiModelProperty("返回标志（0：成功 -1：失败)）")
  public Integer code = 0;
  @JsonInclude(JsonInclude.Include.ALWAYS)
  @ApiModelProperty("返回正确信息")
  public String message = "操作成功";
  @ApiModelProperty("返回数据")
  public T data;


  public void setErrorResponse(String errorMessage) {
    this.code = -1;
    this.message = errorMessage;
  }

  public void setSuccessResponse(T data, String message) {
    this.message = message;
    this.code = 0;
    this.data = data;
  }

  public void setSuccessResponse(T data) {
    this.message = "操作成功";
    this.code = 0;
    this.data = data;
  }
}
