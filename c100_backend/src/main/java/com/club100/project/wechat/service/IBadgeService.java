package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Badge;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 15:43:46
 * @Description: 徽章服务类。
 */
public interface IBadgeService {

    /**
     * 查询徽章信息。
     *
     * @param badge
     * @return
     */
    List<Badge> selectBadgeList(Badge badge);

    /**
     * 根据主键查询徽章信息。
     *
     * @param badgeId
     * @return
     */
    Badge selectBadgeById(Long badgeId);

    /**
     * 根据路线主键 ID 查询徽章信息。
     *
     * @param routeId
     * @return
     */
    Badge selectBadgeByRouteId(Long routeId);

    /**
     * 插入一条徽章信息。
     *
     * @param badge
     * @return
     */
    int insertBadge(Badge badge);

}
