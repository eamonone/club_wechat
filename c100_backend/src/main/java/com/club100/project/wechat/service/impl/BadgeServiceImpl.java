package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.Badge;
import com.club100.project.wechat.mapper.BadgeMapper;
import com.club100.project.wechat.service.IBadgeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 15:45:03
 * @Description: 徽章服务实现类。
 */
@Slf4j
@Service
public class BadgeServiceImpl implements IBadgeService {

    @Autowired
    private BadgeMapper badgeMapper;

    @Override
    public List<Badge> selectBadgeList(Badge badge) {
        return badgeMapper.selectBadgeList(badge);
    }

    @Override
    public Badge selectBadgeById(Long badgeId) {
        return badgeMapper.selectBadgeById(badgeId);
    }

    @Override
    public Badge selectBadgeByRouteId(Long routeId) {
        return badgeMapper.selectBadgeByRouteId(routeId);
    }

    @Override
    public int insertBadge(Badge badge) {
        return badgeMapper.insertBadge(badge);
    }

}
