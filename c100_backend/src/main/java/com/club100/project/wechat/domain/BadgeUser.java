package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 16:29:05
 * @Description: 徽章获得用户 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeUser extends BaseEntity implements Serializable {

    private Long badgeUserId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long badgeId; // 徽章主键 ID。
    private Long userId; // 用户主键 ID。

    public String toJson(BadgeUser badgeUser) {

        // BadgeUser 对象转 JSON。
        return JSON.toJSONString(badgeUser);

    }

}
