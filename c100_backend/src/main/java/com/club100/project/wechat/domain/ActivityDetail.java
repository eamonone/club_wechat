package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/21 16:21:04
 * @Description: 活动详情 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDetail extends BaseEntity implements Serializable {

    private Long activityId; // Primary Key ID。

    private Double rideDistance; // 骑行距离（单位：km）。
    private Double rideTimeLong; // 骑行时长（单位：小时）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Integer activityDays; // 活动天数。
    private Integer activityFeeType; // 费用类型（1：免费；2：收费。）。
    private Integer activityFrequency; // 活动频率。
    private Integer activityType; // 活动类型（）。
    private Integer limitPeopleNumber; // 人数上限。
    private LocalDateTime activityBeginTime; // 活动开始时间。
    private LocalDateTime activityEndTime; // 活动结束时间。
    private LocalDateTime assembleTime; // 集合时间。
    private LocalDateTime releaseTime; // 发布时间。
    private LocalDateTime signUpEndTime; // 报名截止时间。
    private Long bikeShopId; // 车店主键。
    private Long leaderId; // 领队主键。
    private String activityDescription; // 活动详情介绍。
    private String activityDetailPictureUrls; // 活动详情图片。
    private String activityLocation; // 活动地点。
    private String activityName; // 活动名称。
    private String activityPictureUrl; // 活动图片。
    private String assembleLocation; // 集合地点。
    private String bikeShopName; // 车店名称。
    private String bikeShopAvatarUrl; // 车店头像链接 URL。
    private String contactName; // 领队姓名。
    private String contactPhone; // 领队电话。
    private String contactWeChat; // 领队微信号。
    private String leaderName; // 领队姓名。
    private String leaderPhone; // 领队号码。
    private String leaderWechat; // 领队微信号。
    private String rideBikeType; // 骑行车型（1：公路；2：山地；3：小折；4：不限。）。
    private String rideTerrain; // 骑行地形（1：平路；2：起伏；3：爬坡。）。

    public String toJson(ActivityDetail activityDetail) {

        // ActivityDetail 对象转 JSON。
        return JSON.toJSONString(activityDetail);

    }

}
