package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.monitor.service.ISysOperLogService;
import com.club100.project.wechat.domain.Card;
import com.club100.project.wechat.service.ICardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/16 14:24:34
 * @Description: 主页卡片控制器。
 */
@RequestMapping("/v1/card")
@RestController
public class CardController extends BaseController {

//    private String PREFIX = "/v1/card/";

    @Autowired
    private ICardService cardService;

    @PostMapping
    public AjaxResult insert(@RequestBody Card card) {

        cardService.insert(card);
        return AjaxResult.success();

    }

    @GetMapping
    public TableDataInfo page(Card card) {

        startPage();
        return getDataTable(cardService.page(card));

    }

    @GetMapping("mp")
    public AjaxResult mpPage(Card card) {

        startPage();
        return AjaxResult.success(getPageTable(cardService.page(card)));

    }

    @DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(cardService.deleteByCardIds(id));

    }

    @RequestMapping(value = "/addCard")
    public Integer addCard(@RequestBody Card card) {

        return cardService.insert(card);

    }

    @RequestMapping(value = "/updateCard")
    public Integer updateCard(@RequestBody Card card) {

        return cardService.updateCard(card);

    }

}
