package com.club100.project.wechat.domain.v3;

import lombok.Data;

@Data
public class StoreInfo {

  private int total;
  private String currency;
}
