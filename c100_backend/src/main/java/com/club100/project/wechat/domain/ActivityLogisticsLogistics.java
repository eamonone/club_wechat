package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 15:29:13
 * @Description: 活动后勤保障 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityLogisticsLogistics extends BaseEntity implements Serializable {

    private Long activityLogisticsLogisticsId; // Primary Key ID。

    private Long activityId; // 活动主键。
    private Long logisticsId; // 后勤保障主键。
    private String logisticsName; // 后勤保障名称。
    private String logisticsPictureUrl; // 后勤保障图片链接 URL。

    public String toJson(ActivityLogisticsLogistics activityLogisticsLogistics) {

        // ActivityLogisticsLogistics 对象转 JSON。
        return JSON.toJSONString(activityLogisticsLogistics);

    }

}
