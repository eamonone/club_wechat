package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RouteDetail;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/28 16:38:16
 * @Description: 路线详情服务类。
 */
public interface IRouteDetailService {

    int insertOneRouteDetail(RouteDetail routeDetail);

    /**
     * 列举路线详情信息。
     *
     * @param routeDetail
     * @return
     */
    RouteDetail selectRouteDetail(RouteDetail routeDetail);

    /**
     * 列举路线详情信息。
     *
     * @param routeDetail
     * @return
     */
    List<RouteDetail> selectRouteDetailList(RouteDetail routeDetail);

    /**
     * 修改路线详情信息。
     *
     * @param routeDetail
     * @return
     */
    int updateRouteDetail(RouteDetail routeDetail);

}
