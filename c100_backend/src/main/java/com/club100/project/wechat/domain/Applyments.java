package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Applyments extends BaseEntity implements Serializable {


  /**
   * organization_type : 2
   * business_license_info : {"business_license_copy":"47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2 \t  XAUf-4KGprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4","business_license_number":"123456789012345678","merchant_name":"腾讯科技有限公司","legal_person":"张三"}
   * organization_cert_info : {"organization_copy":"47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf-4K \t  GprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4","organization_time":"[\"2014-01-01\",\"长期\"]","organization_number":"12345679-A"}
   * id_card_info : {"id_card_copy":"jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_PvWkfSCJ3zVIn001D8daLC-eh \t  Euo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ","id_card_national":"47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf-4KGpr \t  rKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4","id_card_name":"pVd1HJ6zyvPedzGaV+X3qtmrq9bb9tPROvwia4ibL+F6mfjb \t  zQIzfb3HHLEjZ4YiR/cJiCrZxnAqi+pjeKIEdkwzXRAI7FUhr fPK3SNjaBTEu9GmsugMIA9r3x887Q+ODuC8HH2nzAn7NGpE/e3yiHgWhk0ps5k5DP/2qIdG dONoDzZelrxCl/NWWNUyB93K9F+jC1JX2IMttdY+aQ6zBlw0 xnOiNW6Hzy7UtC+xriudjD5APomty7/mYNxLMpRSvWKIjOv/69bDnuC4EL5Kz4jBHLiCyOb+t I0m2qhZ9evAM+Jv1z0NVa8MRtelw/wDa4SzfeespQO/0kjiwfqdfg==","id_card_number":"UZFETyabYFFlgvGh6R4vTzDELiPas3jC94/srAbSaPaWSM+1rh \t  GLcsybPLeJdVOpCeDxNBMDK+/N0nOiZZ4ka9+5RgzvA2rJx+NztYUbN   \t  209rq0Y/NP50T9yk0m6A4xUoFWgk/8qteRHtP5VHahNhSh8nHo31V33t8edSlN9HJG6diRj7 \t  p5JPImDyM1q56+p25edl3+cOtuZtj8TJDl/hB+GaWve9X1WUpkZbKlJgB \t  xp+XhaW707k9XrILvfD+rSGTOeU/ev4/OiEb5W4WPGJ+3iLoQvhnz3+aQZX9+gn9uRz \t  WcHu2Kr17fhsM+MRkgVcwzI2UqhR9iuGTunRPRVFg==","id_card_valid_time":"2026-06-06"}
   * need_account_info : true
   * account_info : {"bank_account_type":"74","account_name":"fTA4TXc8yMOwFCYeGPktOOSbOBei3KA8RAMO9h/5Y0ZM \t  R46viedrDurCbQTC/sCC9BWuG5oeR8flymK/Z4dN0/7XDSDfqT5Nggq9WwTL+O   \t  ZdMorqTE6Z0G3f3Bi3c+GVvYOCZyVdoinksPEUwyosEtwupM3ufXSZT36DvsS8K9jOBnI \t  XjGaQtP94FFlh58WR0GuEkMt9KT2NuA+fE4KDScRzSZn3TL5Izdt+7anPS6 \t  Uh4K7wPnUWHGxCQdZu0G4B4YjP7ync8UeLu4jAkCziq4lngnU3rKfFiqwMtyOA1 \t  79x15za7+kWmo2hfaC7xumTqXR7/NyRHpFKXURQFcmmw==","account_bank":"工商银行","bank_address_code":"110000","bank_branch_id":"402713354941","bank_name":"施秉县农村信用合作联社城关信用社","account_number":"d+xT+MQCvrLHUVDWv/8MR/dB7TkXM2YYZlokmXzFsWs35NXUot7C0N \t  cxIrUF5FnxqCJHkNgKtxa6RxEYyba1+VBRLnqKG2fSy/Y5qDN08Ej9zHCwJjq52Wg1VG8MR\t   \t  ugli9YMI1fI83KGBxhuXyemgS/hqFKsfYGiOkJqjTUpgY5VqjtL2N4l4z11T0E \t  CB/aSyVXUysOFGLVfSrUxMPZy6jWWYGvT1+4P633f+R+ki1gT4WF/2KxZOYmli38 5ZgVhcR30mr4/G3HBcxi13zp7FnEeOsLlvBmI1PHN4C7Rsu3WL8sPndjXTd75kPkyjqnoMRrEEaYQE8ZRGYoeorwC+w=="}
   * contact_info : {"contact_type":"65","contact_name":"pVd1HJ6zyvPedzGaV+X3qtmrq9bb9tPROvwia4ibL+F6mfjbzQIzfb3HHL \t  EjZ4YiR/cJiCrZxnAqi+pjeKIEdkwzXRAI7FUhrfPK3SNjaBTEu9GmsugMIA9r3x8\t   \t  87Q+ODuC8HH2nzAn7NGpE/e3yiHgWhk0ps5k5DP/2qIdGdONoDzZelrxCl/NWWNUyB93K9F+ \t  jC1JX2IMttdY+aQ6zBlw0xnOiNW6Hzy7UtC+xriudjD5APomty7 /mYNxLMpRSvWKIjO \t  v/69bDnuC4EL5Kz4jBHLiCyOb+tI0m2qhZ9evAM+Jv1z0NVa8MRtelw/wDa4SzfeespQO/0kjiwfqdfg==","contact_id_card_number":"UZFETyabYFFlgvGh6R4vTzDELiPas3jC94/srAbSaPa \t  WSM+1rhGLcsybPLeJdVOpCeDxNBMDK+/N0nOiZZ4ka9+5RgzvA2rJx+NztYUb   \t  N209rq0Y/NP50T9yk0m6A4xUoFWgk/8qteRHtP5VHahNhSh8nHo31V33t8edSlN9HJG6diRj7p \t  5JPImDyM1q56+p25edl3+cOtuZtj8TJDl/hB+GaWve9X1WUpkZbKlJgBxp+Xha W707k9XrILvfD+rSGTOeU/ev4/OiEb5W4WPGJ+3iLoQvhnz3+aQZX9+gn9uR zWcHu2Kr17fhsM+MRkgVcwzI2UqhR9iuGTunRPRVFg==","mobile_phone":"Uy5Hb0c5Se/orEbrWze/ROHu9EPAs/CigDlJ2fnyzC1ppJNBOYGyc \t  89xUgZZoPIRnPWsvJ5oevXNdBK3IUz9WHs9iQKpeUksvoLQMsykc8LDu\t   \t  7MMpayKWNVozldcRugH++MltTBKWTkv/oOcwkZattMGgP4CtpbN6djDK1PcAmIDgdFD2ZvCIDCtJ \t  g1V/YafUBJdBTvNLXa/jNzjZaypsUn1BRO6fx8aaNn7XyTv7JrfQZE4 \t  UDH4gfMFOj8YDqQ+IvDbkuNhaLZExOEz/UcnxeN5mfGr2MdkPr \t  OzF+xJmUZUn1nafZxENrqcBszhYQUlu5zn6o2uZpBhAsQwd3QAjw==","contact_email":"Uy5Hb0c5Se/orEbrWze/ROHu9EPAs/CigDlJ2fnyzC1ppJNBOY \t  Gyc89xUgZZoPIRnPWsvJ5oevXNdBK3IUz9WHs9iQKpeUksvoLQMsykc8LDu7MMpay\t   \t  KWNVozldcRugH++MltTBKWTkv/oOcwkZattMGgP4CtpbN6djDK1PcAmIDgdFD2ZvCIDCtJg1V \t  /YafUBJdBTvNLXa/jNzjZaypsUn1BRO6fx8aaNn7XyTv7JrfQZE4UDH4gfMFOj8YD qQ+IvDbkuNhaLZExOEz/UcnxeN5mfGr2MdkPrOzF+xJmUZUn1nafZxENrqcBszhYQUlu5zn6o2uZpBhAsQwd3QAjw=="}
   * sales_scene_info : {"store_name":"爱烧烤","store_url":"http://www.qq.com","store_qr_code":"jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_PvWkfSCJ3z \t  VIn001D8daLC-ehEuo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ"}
   * merchant_shortname : 爱烧烤
   * out_request_no : APPLYMENT_00000000001
   * qualifications : ["jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_PvWkfSCJ3zV    In001D8daLC-ehEuo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ","47 ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf- 4KGprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4"]
   * business_addition_pics : ["jTpGmxUX3FBWVQ5NJTZvlKX_gdU4cRz7z5NxpnFuAxhBTEO_Pv        WkfSCJ3zVIn001D8daLC-ehEuo0BJqRTvDujqhThn4ReFxikqJ5YW6zFQ","47ZC6 GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf-4 KGprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4"]
   * business_addition_desc : 特殊情况，说明原因
   */

  private String organization_type;
  private BusinessLicenseInfo business_license_info;
  private OrganizationCertInfo organization_cert_info;
  private String id_doc_type;
  private boolean need_account_info;
  private AccountInfo account_info;
  private ContactInfo contact_info;
  private SalesSceneInfo sales_scene_info;
  private String merchant_shortname;
  private String out_request_no;
  private String qualifications;
  private String business_addition_pics;
  private String business_addition_desc;

}
