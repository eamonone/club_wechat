package com.club100.project.wechat.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * club_order
 * @author 
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Receiver implements Serializable {
    /**
     * 分账接收方类型
     */
    private String type;

    /**
     * 分账接收方帐号
     */
    private String account;
    //分账接收方全称
    private String name;

    /**
     * 分账金额(单位分)
     */
    private int amount;

    /**
     * 分账描述
     */
    private String description;
    //与分账方的关系类型
    private String relation_type;
}