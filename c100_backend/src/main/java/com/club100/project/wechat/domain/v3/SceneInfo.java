package com.club100.project.wechat.domain.v3;

import lombok.Data;

@Data
public class SceneInfo {

  private StoreInfo store_info;
  private String device_id;
  private String payer_client_ip;
}
