package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RefundPolicy;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/14 12:24:31
 * @Description: 退款政策服务类。
 */
public interface IRefundPolicyService {

    /**
     * 插入一条退款政策信息。
     *
     * @param refundPolicy
     * @return
     */
    int insertRefundPolicy(RefundPolicy refundPolicy);

    /**
     * 批量插入退款政策信息。
     *
     * @param refundPolicyList
     * @return
     */
    int insertRefundPolicyInBatch(List<RefundPolicy> refundPolicyList);

    /**
     * 查询退款政策信息。
     *
     * @param refundPolicy
     * @return
     */
    List<RefundPolicy> selectRefundPolicyList(RefundPolicy refundPolicy);

    /**
     * 查询退款政策信息。
     *
     * @param refundPolicy
     * @return
     */
    List<RefundPolicy> selectRefundPolicyListWithPrivilege(RefundPolicy refundPolicy);

    /**
     * 删除退款政策关联信息。
     *
     * @param activityId
     * @return
     */
    int deleteRefundPolicy(Long... activityId);

}
