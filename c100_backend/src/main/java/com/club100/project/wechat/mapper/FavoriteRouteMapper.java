package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.FavoriteRoute;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:05:01
 * @Description: 收藏路线 Mapper 接口。
 */
public interface FavoriteRouteMapper {

    int insertFavoriteRoute(FavoriteRoute favoriteRoute);

    List<FavoriteRoute> selectFavoriteRouteList(FavoriteRoute favoriteRoute);

    List<FavoriteRoute> selectFavoriteRouteByPageList(@Param("userId")Long userId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    int selectFavoriteRouteCountList(FavoriteRoute favoriteRoute);

    int deleteFavoriteRoute(FavoriteRoute favoriteRoute);

}
