package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.SignUpUser;
import com.club100.project.wechat.mapper.SignUpUserMapper;
import com.club100.project.wechat.service.ISignUpUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/19 23:01:52
 * @Description: 报名用户服务实现类。
 */
@Slf4j
@Service
public class SignUpUserServiceImpl implements ISignUpUserService {

    @Autowired
    private SignUpUserMapper signUpUserMapper;

    @Override
    public List<SignUpUser> selectSignUpUser(SignUpUser signUpUser) {
        return signUpUserMapper.selectSignUpUser(signUpUser);
    }

}
