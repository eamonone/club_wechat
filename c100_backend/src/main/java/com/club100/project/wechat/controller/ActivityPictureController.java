package com.club100.project.wechat.controller;

import com.club100.common.utils.ServletUtils;
import com.club100.framework.security.LoginUser;
import com.club100.framework.security.service.TokenService;
import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.system.domain.SysUser;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.service.IActivityPictureService;
import com.club100.project.wechat.service.IActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/16 12:16:47
 * @Description: 活动图片控制器。
 */
@RequestMapping("/v1/activitypicture")
@RestController
public class ActivityPictureController extends BaseController {

    // Activity information.
    @Autowired
    private IActivityService activityService;
    // Activity picture information.
    @Autowired
    private IActivityPictureService activityPictureService;

    /**
     * 查询活动图片列表。
     */
    @GetMapping("/listActivityPicture")
    public TableDataInfo listActivityPicture(ActivityPicture activityPicture) {

        startPage();
        List<ReturnActivityPictureInfo> returnedActivityPictureInfoList = new ArrayList<>();

        Long sysUserIdLong = P_GetLoginSystemUserId();
        activityPicture.setSysUserId(sysUserIdLong);

        List<ActivityPicture> activityPicturesList = activityPictureService.selectActivityPictureWithPrivilege(activityPicture);

        // 分组。
        Map<Long, List<ActivityPicture>> groupByActivityIdMap = activityPicturesList.stream().collect(Collectors.groupingBy(ActivityPicture::getActivityId));

        // 遍历分组并处理数据。
        for (Map.Entry<Long, List<ActivityPicture>> entryActivityPicture : groupByActivityIdMap.entrySet()) {
            Long key = entryActivityPicture.getKey();

            String activityNameString = P_GetActivityNameById(key);
            List<ActivityPicture> entryActivityPictureList = entryActivityPicture.getValue();

            StringBuilder activityPictureStringBuilder = new StringBuilder();

            for (ActivityPicture eachActivityPicture : entryActivityPictureList) {

                String activityPictureUrlString = eachActivityPicture.getActivityPictureUrl();

                if (activityPictureStringBuilder.length() == 0) {
                    activityPictureStringBuilder.append(activityPictureUrlString);
                } else {
                    activityPictureStringBuilder.append(",").append(activityPictureUrlString);
                }
            }

            String[] activityPictureStringArray = String.valueOf(activityPictureStringBuilder).split(",");

            ReturnActivityPictureInfo returnActivityPictureInfo = new ReturnActivityPictureInfo(key, activityNameString, activityPictureStringArray);
            returnedActivityPictureInfoList.add(returnActivityPictureInfo);

        }

        return getDataTable(returnedActivityPictureInfoList);

    }

    /**
     * 删除活动图片信息。
     */
    @DeleteMapping("/{returnActivityPictureInfoIds}")
    public AjaxResult deleteActivityPicture(@PathVariable Long[] returnActivityPictureInfoIds) {

        return toAjax(activityPictureService.deleteActivityPicture(returnActivityPictureInfoIds));

    }

    /**
     * 添加活动图片信息。
     *
     * @param returnActivityPictureInfo
     * @return
     */
    @RequestMapping("/addActivityPicture")
    public int addActivityPicture(@RequestBody ReturnActivityPictureInfo returnActivityPictureInfo) {

        ActivityPicture insertedActivityPictureEntity = new ActivityPicture();
        insertedActivityPictureEntity.setActivityId(Long.parseLong(returnActivityPictureInfo.returnActivityNameInfo));
        insertedActivityPictureEntity.setActivityPictureUrl(String.valueOf(returnActivityPictureInfo.returnActivityPictureInfo[0]));

        return activityPictureService.insertActivityPicture(insertedActivityPictureEntity);

    }

    /**
     * Get activity name by activity Id.
     *
     * @param activityId
     * @return
     */
    private String P_GetActivityNameById(long activityId) {

        Activity searchedActivityEntity = activityService.selectActivityById(activityId);

        return searchedActivityEntity.getActivityName();

    }

}
