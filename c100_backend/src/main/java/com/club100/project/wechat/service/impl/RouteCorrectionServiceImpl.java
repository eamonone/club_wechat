package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.RouteCorrection;
import com.club100.project.wechat.mapper.RouteCorrectionMapper;
import com.club100.project.wechat.service.IRouteCorrectionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 22:33:18
 * @Description: 路线纠错服务实现类。
 */
@Slf4j
@Service
public class RouteCorrectionServiceImpl implements IRouteCorrectionService {

    @Autowired
    private RouteCorrectionMapper routeCorrectionMapper;

    @Override
    public int insertRouteCorrection(RouteCorrection routeCorrection) {
        return routeCorrectionMapper.insertRouteCorrection(routeCorrection);
    }

    @Override
    public RouteCorrection selectRouteCorrectionById(Long routeCorrectionId) {
        return routeCorrectionMapper.selectRouteCorrectionById(routeCorrectionId);
    }

    @Override
    public List<RouteCorrection> selectRouteCorrection(RouteCorrection routeCorrection) {
        return routeCorrectionMapper.selectRouteCorrection(routeCorrection);
    }

}
