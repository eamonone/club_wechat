package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/29 19:13:08
 * @Description: 路线和路线详情 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteAndDetail extends BaseEntity implements Serializable {

    private Long routeId; // Primary Key ID。

    private Boolean ifFavorite; // 用户是否收藏过（true：已收藏；false：未收藏。）。
    private Double climb; // 爬升（单位：m）。
    private Double difficultyLevel; // 难度（单位：颗星）。
    private Double distance; // 距离（单位：km）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer routeClickCount; // 路线点击数。
    private Integer routeTerrain; // 路线地形（1：平路；2：起伏；3：爬坡。）。
    private Integer routeType; // 路线类型（1：绕圈；2：休闲；3：进阶；4：高阶；5：挑战。）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private List<MarkRouteUser> recentlyRider; // 最近骑过的人（返回最多 6 个用户信息）。
    private String gpxUrl; // GPX 文件的下载 URL。
    private String jsonUrl; // json 文件的下载 URL。
    private String headPictureUrl; // 路线详情头部图片的 URL。
    private String routeBadgePictureUrl; // 徽章图片链接 URL。
    private String routeDescription; // 路线描述。
    private String routeDistrict; // 路线地区名称。
    private String routeName; // 路线名。
    private String routePictureUrl; // 路线图片 URL。
    private String routeProvider; // 路线提供车店名称。
    private String routeVideoUrl; // 路线视频 URL。

    public String toJson(RouteAndDetail routeAndDetail) {

        // RouteAndDetail 对象转 JSON。
        return JSON.toJSONString(routeAndDetail);

    }

}
