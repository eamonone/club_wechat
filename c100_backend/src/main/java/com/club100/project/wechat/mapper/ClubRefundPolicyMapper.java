package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ClubRefundPolicy;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ClubRefundPolicyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ClubRefundPolicy record);

    int insertSelective(ClubRefundPolicy record);

    ClubRefundPolicy selectByPrimaryKey(Long id);

    ClubRefundPolicy selectRefundPolicyByActivityId(@Param("activityId") Long activityId);

    int updateByPrimaryKeySelective(ClubRefundPolicy record);

    int updateByPrimaryKey(ClubRefundPolicy record);

  /**
   * 查询已经超过退款期限的活动id
   *
   * @return
   */
  @Select("select id from club_activity where DATE_ADD(activityEndTime, INTERVAL 1 DAY) <= current_date ")
  List<Long> getFinishActivityId();
}