package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.qiniu.QiniuService;
import com.club100.project.wechat.domain.Route;
import com.club100.project.wechat.domain.RouteDetail;
import com.club100.project.wechat.service.IRouteDetailService;
import com.club100.project.wechat.service.IRouteService;
import com.club100.project.wechat.utils.IOStreamUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/02 15:10:44
 * @Description: 路线详情控制器。
 */
@RequestMapping("/v1/routeDetail")
@RestController
@Slf4j
public class RouteDetailController extends BaseController {

//    private String PREFIX = "/v1/routeDetail/";

    // QiNiu cloud server information.
    @Autowired
    private QiniuService qiniuService;
    @Autowired
    private IRouteService routeService;
    @Autowired
    private IRouteDetailService routeDetailService;

    private final String STRING_VALUE_AUTO_GENERATED = "AUTO_GENERATED";
    private final String STRING_CHARSET_VALUE_UTF8 = "UTF-8";
    private final String STRING_FILE_TYPE_VALUE_JSON = "json";

    /*@PostMapping
    public AjaxResult insert(@RequestBody Route route) {

        routeService.insert(route);
        return AjaxResult.success();

    }*/

    @RequestMapping(value = "/insertOneRouteDetail")
    @ResponseBody
    public int insertOneRouteDetail(@RequestBody RouteDetail routeDetail) {

        routeDetail.setRouteId(routeService.getLatestRouteId().getRouteId());

        String gpxFileUrlString = routeDetail.getGpxUrl();

        // Get GPX file's content via url.
        String gpxFileContentString = _GetGpxFileContentViaUrl(gpxFileUrlString);
        log.info("gpxFileContentString: {}", gpxFileContentString);

        // Transform gpx into json and create json file.
        String outputFileNameString = _GenerateJsonFileFromGpxFile(gpxFileContentString);
        log.info("outputFileNameString: {}", outputFileNameString);

        // Upload to 'Qiniu' cloud server.
        String qiniuCloudReturnedFileName = _UploadToQiNiu(outputFileNameString);
        log.info("qiniuCloudReturnedFileName: {}", qiniuCloudReturnedFileName);

        String gpxJsonFileUrlString = "https://images.club100.cn/" + qiniuCloudReturnedFileName;
        routeDetail.setJsonUrl(gpxJsonFileUrlString);

        return routeDetailService.insertOneRouteDetail(routeDetail);

    }

    @GetMapping
    public TableDataInfo selectRouteDetailList(RouteDetail routeDetail) {

        startPage();
        return getDataTable(routeDetailService.selectRouteDetailList(routeDetail));

    }

    /*@GetMapping("mp")
    public AjaxResult mpPage(Route route) {

        startPage();
        return AjaxResult.success(getPageTable(routeService.selectRouteList2(route)));

    }*/

    /*@DeleteMapping("{id}")
    public AjaxResult deleteById(@PathVariable Long id) {

        return toAjax(routeService.deleteByRouteIds(id));

    }*/

    @RequestMapping(value = "/updateRouteDetail")
    public Integer updateRouteDetail(@RequestBody RouteDetail routeDetail) {

        return routeDetailService.updateRouteDetail(routeDetail);

    }

    /**
     * @param gpxFileContentString
     * @return gpxFileContentString
     * @Description Transform gpx into json and create json file.
     */
    private String _GenerateJsonFileFromGpxFile(String gpxFileContentString) {

        // Initialization.
        String beforeString = "{\"segments\":[[";
        String afterString = "]]}";
        String openString = "[";
        String closeString = "]";

        String jsonString = XML.toJSONObject(gpxFileContentString).toString();
//        log.info("jsonString: {}", jsonString);

        String modifiedJsonString = beforeString + StringUtils.substringBetween(jsonString, openString, closeString) + afterString;
//        log.info("modifiedJsonString: {}", modifiedJsonString);

        return IOStreamUtils.S_outputContentToFile(modifiedJsonString, STRING_VALUE_AUTO_GENERATED, STRING_FILE_TYPE_VALUE_JSON, STRING_CHARSET_VALUE_UTF8);

    }

    /**
     * @param gpxFileUrlString
     * @return gpxFileContentString
     * @Description Get GPX file's content via url.
     */
    private String _GetGpxFileContentViaUrl(String gpxFileUrlString) {

        StringBuilder gpxFileContentStringBuilder = new StringBuilder();

        try {
            //建立连接
            URL url = new URL(gpxFileUrlString);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setRequestMethod("GET");
            httpUrlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            //获取输入流
            InputStream input = httpUrlConn.getInputStream();
            //将字节输入流转换为字符输入流
            InputStreamReader read = new InputStreamReader(input, "utf-8");
            //为字符输入流添加缓冲
            BufferedReader br = new BufferedReader(read);
            // 读取返回结果
            String data = br.readLine();
            while (data != null) {
                data = br.readLine();
                gpxFileContentStringBuilder.append(data + "\r\n");
            }
            // 释放资源
            br.close();
            read.close();
            input.close();
            httpUrlConn.disconnect();
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
            log.error("UnsupportedEncodingException");
        } catch (ProtocolException e) {
            e.printStackTrace();
            log.error("ProtocolException");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            log.error("MalformedURLException");
        } catch (IOException e) {
            e.printStackTrace();
            log.error("IOException");
        }

        return gpxFileContentStringBuilder.toString();

    }

    /**
     * @param outputFileNameString
     * @return qiniuCloudReturnedFileName
     * @Description Upload to 'Qiniu' cloud server.
     */
    private String _UploadToQiNiu(String outputFileNameString) {

        return qiniuService.upload(outputFileNameString);

    }

}
