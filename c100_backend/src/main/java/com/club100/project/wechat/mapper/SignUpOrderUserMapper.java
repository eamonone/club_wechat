package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.SignUpOrderUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/08 20:58:51
 * @Description: 报名人数 Mapper 接口。
 */
public interface SignUpOrderUserMapper {

    List<SignUpOrderUser> selectSignUpOrderUser(SignUpOrderUser signUpOrderUser);

    int selectSignUpOrderUserCount(SignUpOrderUser signUpOrderUser);

}
