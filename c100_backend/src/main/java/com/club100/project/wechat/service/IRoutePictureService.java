package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RoutePicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/8/10 17:03:03
 * @Description: 路线详情图片服务类。
 */
public interface IRoutePictureService {

    /**
     * 列举路线详情图片信息。
     *
     * @param routePicture
     * @return
     */
    List<RoutePicture> selectRoutePicture(RoutePicture routePicture);

}
