package com.club100.project.wechat.controller;

import com.alibaba.fastjson.JSON;
import com.club100.common.utils.ip.IpUtils;
import com.club100.framework.web.controller.BaseController;
import com.club100.project.wechat.domain.ClubOrder;
import com.club100.project.wechat.domain.ClubRefund;
import com.club100.project.wechat.domain.ClubRefundPolicyResponse;
import com.club100.project.wechat.domain.NullDataTo;
import com.club100.project.wechat.service.WxPayService;
import com.club100.project.wechat.utils.ResponseTemplate;
import com.club100.project.wechat.wxpay.sdk.WXPayUtil;
import com.club100.project.wechat.wxpay.sdk.vo.WxPayNotifyV0;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


/**
 * @author
 */
@Api(tags = "微信支付接口")
@RestController
@RequestMapping("/v1/wxPay")
public class WxPayController extends BaseController {

  @Autowired
  private WxPayService wxPayService;

  @ApiOperation("微信支付接口->发起订单")
  @PostMapping("pay/{productId}/{userId}")
  public ResponseTemplate<Map<String,String>> pay(@ApiIgnore HttpServletRequest request,
                                                  @ApiParam("产品id") @PathVariable("productId")Long productId,
                                                  @ApiParam("用户id") @PathVariable("userId")Long  userId,
                                                  @ApiIgnore ResponseTemplate<Map<String,String>> response) {
    try {
      String ipAddress = IpUtils.getIpAddr(request);
      response.setData(wxPayService.wxPay(productId,userId,ipAddress));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @ApiOperation("微信支付接口->查询订单详情")
  @GetMapping("order/getOrderDetail/{orderId}")
  public ResponseTemplate<ClubOrder> getOrderDetail(@ApiParam("订单id") @PathVariable("orderId")Long orderId,
                                                    @ApiIgnore ResponseTemplate<ClubOrder> response) {
    try {
      response.setData(wxPayService.orderDetailQuery(orderId));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }


  @ApiOperation("微信支付接口->支付成功后修改订单状态")
  @GetMapping("payStatus/{orderId}/{status}/{insuranceId}")
  public ResponseTemplate<NullDataTo> paySuccess(@ApiIgnore HttpServletRequest request,
                                                 @ApiParam("订单id") @PathVariable("orderId")Integer orderId,
                                                 @ApiParam("订单状态（1:成功 2：失败）") @PathVariable("status")Integer status,
                                                 @ApiParam("保单id") @PathVariable("insuranceId")Long insuranceId,
                                                 @ApiIgnore ResponseTemplate<NullDataTo> response){
    try {
      wxPayService.orderStatus(orderId,status,insuranceId);
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }


  @ApiOperation("微信支付接口->查询退款金额")
  @GetMapping("getRefundMoney/{orderId}")
  public ResponseTemplate<ClubRefundPolicyResponse> getRefundMoney(@ApiIgnore HttpServletRequest request,
                                                           @ApiParam("订单id") @PathVariable("orderId")Integer orderId,
                                                           @ApiIgnore ResponseTemplate<ClubRefundPolicyResponse> response) {
    try {
      response.setData(wxPayService.getRefundMoney(orderId));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @ApiOperation("微信支付接口->发起退款")
  @GetMapping("refund/{orderId}")
  public ResponseTemplate<NullDataTo> refund(@ApiIgnore HttpServletRequest request,
                                             @ApiParam("订单id") @PathVariable("orderId")Integer orderId,
                                             @ApiIgnore ResponseTemplate<NullDataTo> response) {
    try {
      wxPayService.refund(orderId);
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }


  @ApiOperation("微信支付接口->退款详情")
  @GetMapping("refundDetail/{orderId}")
  public ResponseTemplate<ClubRefund> refundDetail(@ApiIgnore HttpServletRequest request,
                                                   @ApiParam("订单id") @PathVariable("orderId")Integer orderId,
                                                   @ApiIgnore ResponseTemplate<ClubRefund> response) {
    try {
      response.setData(wxPayService.refundDetail(orderId));
    } catch (Exception e) {
      response.setErrorResponse(e.getMessage());
    }
    return response;
  }

  @RequestMapping(value = "success", produces = MediaType.TEXT_PLAIN_VALUE)//xml
  public String success(@ApiIgnore HttpServletRequest request,
                        @RequestBody WxPayNotifyV0 param) throws Exception {
    Map<String,String> result = new HashMap<>();
    if("SUCCESS".equals(param.getReturn_code())){
      // db order.update  status success
      System.out.println("支付成功回调：" + JSON.toJSONString(param));
      result.put("return_code","SUCCESS");
      result.put("return_msg","OK");
    }
    return WXPayUtil.mapToXml(result);
  }


}

