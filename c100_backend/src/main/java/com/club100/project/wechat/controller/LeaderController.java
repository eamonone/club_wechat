package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.project.wechat.service.ILeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/09 21:12:42
 * @Description: 领队控制器。
 */
@RequestMapping("/v1/leader")
@RestController
public class LeaderController extends BaseController {

    // Leader information.
    @Autowired
    private ILeaderService leaderService;

    /**
     * 获取领队单选下拉框选项。
     *
     * @return
     */
    @GetMapping("/selectLeaderDropdownList")
    public AjaxResult selectLeaderDropdownList() {

        return AjaxResult.success(leaderService.selectLeaderDropdownList());

    }

}
