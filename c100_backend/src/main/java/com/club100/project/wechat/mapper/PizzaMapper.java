package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.Pizza;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/01 12:38:09
 * @Description: 披萨 Mapper 接口。
 */
public interface PizzaMapper {

    /**
     * 查询披萨审核信息。
     *
     * @param pizza
     * @return
     */
    List<Pizza> selectPizzaList(Pizza pizza);

}
