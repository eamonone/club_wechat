package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.FavoriteRoute;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/23 15:06:43
 * @Description: 收藏路线服务类。
 */
public interface IFavoriteRouteService {

    /**
     * 插入（一条）收藏路线信息。
     *
     * @param favoriteRoute
     * @return
     */
    int insertFavoriteRoute(FavoriteRoute favoriteRoute);

    /**
     * 列举收藏路线信息（不含分页）。
     *
     * @param favoriteRoute
     * @return
     */
    List<FavoriteRoute> selectFavoriteRouteList(FavoriteRoute favoriteRoute);

    /**
     * 列举收藏路线信息（分页）。
     *
     * @param userId
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<FavoriteRoute> selectFavoriteRouteByPageList(@Param("userId")Long userId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件查询收藏路线个数。
     *
     * @param favoriteRoute
     * @return
     */
    int selectFavoriteRouteCountList(FavoriteRoute favoriteRoute);

    /**
     * 根据条件删除收藏路线。
     *
     * @param favoriteRoute
     * @return
     */
    int deleteFavoriteRoute(FavoriteRoute favoriteRoute);

}
