package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/01 17:09:53
 * @Description: 披萨图片返回值。
 */
@AllArgsConstructor
public class ReturnPizzaInfo implements Serializable {

    public Long pizzaGetterId; // Primary Key ID。

    public Integer state; // 状态（1：可用；2：废弃；3：审核中。）。
    public Long userId; // 用户主键 ID。
    public String weChatId; // 微信 ID。
    public String[] returnPizzaPictureInfo; // 组合后返回的披萨图片 URL。

    public String toJson(ReturnPizzaInfo returnPizzaInfo) {

        // ReturnPizzaInfo 对象转 JSON。
        return JSON.toJSONString(returnPizzaInfo);

    }

}
