package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.PizzaBackgrounds;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 14:24:11
 * @Description: 披萨基本情况 Mapper 接口。
 */
public interface PizzaBackgroundsMapper {

    int insertPizzaBackgrounds(PizzaBackgrounds pizzaBackgrounds);

    PizzaBackgrounds selectPizzaBackgroundsById(Long pizzaBackgroundsId);

}
