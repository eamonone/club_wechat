package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.Source;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/30 19:50:50
 * @Description: 二维码参数统计服务类。
 */
public interface ISourceService {

    /**
     * 查找二维码参数统计信息。
     *
     * @param source
     * @return
     */
    List<Source> selectSourceList(Source source);

    /**
     * 插入一条二维码参数统计信息。
     *
     * @param source
     * @return
     */
    int insertSource(Source source);

    /**
     * 修改一条二维码参数统计信息。
     *
     * @param source
     * @return
     */
    int updateSource(Source source);

    /**
     * （批量）删除二维码参数统计信息。
     *
     * @param sourceId
     * @return
     */
    int deleteBySourceIds(Long... sourceId);

}
