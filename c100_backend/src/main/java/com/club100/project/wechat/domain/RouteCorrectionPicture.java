package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 22:47:41
 * @Description: 路线纠错图片 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteCorrectionPicture extends BaseEntity implements Serializable {

    private Long routeCorrectionPictureId; // Primary Key ID。

    private Integer correctionPictureSequence; // 路线纠错图片序列号。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long routeCorrectionId; // 路线纠错主键。
    private String correctionPictureUrl; // 路线纠错图片链接 URL。

    public String toJson(RouteCorrectionPicture routeCorrectionPicture) {

        // RouteCorrectionPicture 对象转 JSON。
        return JSON.toJSONString(routeCorrectionPicture);

    }

}
