package com.club100.project.wechat.wxpay.V3.vo;

import java.util.List;

public class PlatformCert {


  private List<DataBean> data;

  public List<DataBean> getData() {
    return data;
  }

  public void setData(List<DataBean> data) {
    this.data = data;
  }

  public static class DataBean {
    /**
     * effective_time : 2020-10-19T21:46:21+08:00
     * encrypt_certificate : {"algorithm":"AEAD_AES_256_GCM","associated_data":"certificate","ciphertext":"jlJAYrKS5eRvj52k98uPfBIaVdEsmY5MBNyZQ4Jl+CimU7Md58pYNxSj3aYZNtPUFXP6QjNOA/uzYeY+pOR0C8ebq5zXThkSNUJpehDjtRfYHAp6T2m950CSf7N3MX+Jq7/QjVi6WnScQelhWrr9HpgssnoOgpwmk2AtRcOxiIaQ9GgeDZkjyaHv7hzjl84xoSYQDFs/d7sZiYfRESWx/zHZlao17g713mJq/qnfCXt10Gvz6jAVSkkpp9wFlfgT438IZvQdJ4NxTAkEBSX4hiNGNq6xpazT6o0DvYtdPM5oxcvUDfmypAf+Fi4Q9xp1OHATYsxeCEkTXgwDLkSevlWcvB+MGq6eK/uZS0h1ITjnq4QHG03DPK07uyqJZMtW0TOtmsBFcHg9HoosDj+AC3fuj7yo4n+DcEVrfuSpnDZb9Vc0lvAXiTYXYI6lrJ6ApaMfp2H5RrE3uvS5tqYWxs0sdzbDFCjGrXnsX5D1IQj5VH7US1JwvUBSfWsxxAdJerLrHzCkNfvOHvFC/NMRs/8IGwj9ZI6vRjfQH4XSgIUNyAjQkXVVXwlFw4fXU8cpoFzT7iyCouVnIWrItCrzi/GzXTpiOzbpvOlmlBY596Ok3u5H3WYVcNruV6sNxWmAbHftXxbPdDPWL6yZufRqvEvvdTg0kvUI2qWOaJsdbFB/VAxeBU/nTvsRm9xzREvuptnUN7vmH7KDJ7PPepewq+RXj8u7QMJZ88Tk4uznvgbkI0OR5mVvja4EqGRTm4CZozfR7xU6kzDANZXuKH0697onzaO8+D5E6EuB4yR1QRvwZHlt2KOfYd4JutJ8Sv6w810+uUqEBD2fMk3SB5GU2le+Ls2kf0tUocAGV41u+FTU5jma1SEQNDypsOhZxi8fprUpfAyuz1VRyVeTNXuTKgm8MXRZ4PYrqueks3msmCJEtgBFpfZthsfBIpmLU1Y1ZfZLGMV9WF9dNV6fhRPqQGaFcMJ9PzyfKefLtJ1db2hghvaKq/SNtsWD5pC4uhYIUS4toPeECtNiCo7+vPaFAKwT3kBLJ6rX1Lg57IU4FdWboJk/hk+nuWwxNsfCseXwnCkqFlOQryqad/E4HALaRpaky4LrG+T6km9fUsI4xLUZFT/MEY7JXT+Qo/HL7qOWysjs4t6w1LQm4Solo0XVGe3HMxZLlQz6hFXspAGNDX28JCHHCZdfGnZixaO4ujpXeUYBUZLv/Oog1nUcw9cuVIiE6vG9s1giCJVkQlwMgnXRcw0PwJUGJegLtqq22KOUtUrag0S/eHINmDtIqXxrw6VSu3G98OkKnP+itGbswJ8rzt5VIg2eACTJtBShfUwUDKXdCwZwrdp9YlmQDYMLwpoAN7hDOz4EQASKU69k5mrQmtlBiXH5bNnG9ycP9MdeqyFB1YhY4vakPHc6G58pgHImdwvxaYxpyfSzbvFG4fvUud6WBBGEHawoijkj4PDXQhMstQcZp5Dwpk5tGsQ2ZcYfuBelgH3bDs7dE4SjbEJNVru08QyNrAOTRSfRSKKnGVHpzHFo8HTksprCOCTMHLPvsAQgf7NuHmIVji1i2stVUI+G5wk+UtLENPR87bf3MGbkf6IgYRoF+UsO0FM0V7chOXHHSjg6etQdYtDHlPbJg1sDtxx+si/vncsTyFdZPVvWBrCMIf7mOrxSAWlTbQD8ZFvo6lSHkgMlx/mqwnaDxjejotVCEJRIX1MRu46/ISKAH1clHhxHJXH5GlimrX1Zr9MM+Sla5iNKyL0am1z66CDKy+FVGG7XhBd6/MZHwuUIb5F4anHg6MtYZunhhw3H7vyEhnSd/dvYTEXKm/equVzcV9oNfNwQ4kmyqEy0Qt0p8CIor+evNcUCxXyVr4NzgP0TOA==","nonce":"874b0b4db7a7"}
     * expire_time : 2025-10-18T21:46:21+08:00
     * serial_no : 1317EBDC375493E59E1A46A797722C470F5CEE42
     */

    private String effective_time;
    private EncryptCertificateBean encrypt_certificate;
    private String expire_time;
    private String serial_no;

    public String getEffective_time() {
      return effective_time;
    }

    public void setEffective_time(String effective_time) {
      this.effective_time = effective_time;
    }

    public EncryptCertificateBean getEncrypt_certificate() {
      return encrypt_certificate;
    }

    public void setEncrypt_certificate(EncryptCertificateBean encrypt_certificate) {
      this.encrypt_certificate = encrypt_certificate;
    }

    public String getExpire_time() {
      return expire_time;
    }

    public void setExpire_time(String expire_time) {
      this.expire_time = expire_time;
    }

    public String getSerial_no() {
      return serial_no;
    }

    public void setSerial_no(String serial_no) {
      this.serial_no = serial_no;
    }

    public static class EncryptCertificateBean {
      /**
       * algorithm : AEAD_AES_256_GCM
       * associated_data : certificate
       * ciphertext : jlJAYrKS5eRvj52k98uPfBIaVdEsmY5MBNyZQ4Jl+CimU7Md58pYNxSj3aYZNtPUFXP6QjNOA/uzYeY+pOR0C8ebq5zXThkSNUJpehDjtRfYHAp6T2m950CSf7N3MX+Jq7/QjVi6WnScQelhWrr9HpgssnoOgpwmk2AtRcOxiIaQ9GgeDZkjyaHv7hzjl84xoSYQDFs/d7sZiYfRESWx/zHZlao17g713mJq/qnfCXt10Gvz6jAVSkkpp9wFlfgT438IZvQdJ4NxTAkEBSX4hiNGNq6xpazT6o0DvYtdPM5oxcvUDfmypAf+Fi4Q9xp1OHATYsxeCEkTXgwDLkSevlWcvB+MGq6eK/uZS0h1ITjnq4QHG03DPK07uyqJZMtW0TOtmsBFcHg9HoosDj+AC3fuj7yo4n+DcEVrfuSpnDZb9Vc0lvAXiTYXYI6lrJ6ApaMfp2H5RrE3uvS5tqYWxs0sdzbDFCjGrXnsX5D1IQj5VH7US1JwvUBSfWsxxAdJerLrHzCkNfvOHvFC/NMRs/8IGwj9ZI6vRjfQH4XSgIUNyAjQkXVVXwlFw4fXU8cpoFzT7iyCouVnIWrItCrzi/GzXTpiOzbpvOlmlBY596Ok3u5H3WYVcNruV6sNxWmAbHftXxbPdDPWL6yZufRqvEvvdTg0kvUI2qWOaJsdbFB/VAxeBU/nTvsRm9xzREvuptnUN7vmH7KDJ7PPepewq+RXj8u7QMJZ88Tk4uznvgbkI0OR5mVvja4EqGRTm4CZozfR7xU6kzDANZXuKH0697onzaO8+D5E6EuB4yR1QRvwZHlt2KOfYd4JutJ8Sv6w810+uUqEBD2fMk3SB5GU2le+Ls2kf0tUocAGV41u+FTU5jma1SEQNDypsOhZxi8fprUpfAyuz1VRyVeTNXuTKgm8MXRZ4PYrqueks3msmCJEtgBFpfZthsfBIpmLU1Y1ZfZLGMV9WF9dNV6fhRPqQGaFcMJ9PzyfKefLtJ1db2hghvaKq/SNtsWD5pC4uhYIUS4toPeECtNiCo7+vPaFAKwT3kBLJ6rX1Lg57IU4FdWboJk/hk+nuWwxNsfCseXwnCkqFlOQryqad/E4HALaRpaky4LrG+T6km9fUsI4xLUZFT/MEY7JXT+Qo/HL7qOWysjs4t6w1LQm4Solo0XVGe3HMxZLlQz6hFXspAGNDX28JCHHCZdfGnZixaO4ujpXeUYBUZLv/Oog1nUcw9cuVIiE6vG9s1giCJVkQlwMgnXRcw0PwJUGJegLtqq22KOUtUrag0S/eHINmDtIqXxrw6VSu3G98OkKnP+itGbswJ8rzt5VIg2eACTJtBShfUwUDKXdCwZwrdp9YlmQDYMLwpoAN7hDOz4EQASKU69k5mrQmtlBiXH5bNnG9ycP9MdeqyFB1YhY4vakPHc6G58pgHImdwvxaYxpyfSzbvFG4fvUud6WBBGEHawoijkj4PDXQhMstQcZp5Dwpk5tGsQ2ZcYfuBelgH3bDs7dE4SjbEJNVru08QyNrAOTRSfRSKKnGVHpzHFo8HTksprCOCTMHLPvsAQgf7NuHmIVji1i2stVUI+G5wk+UtLENPR87bf3MGbkf6IgYRoF+UsO0FM0V7chOXHHSjg6etQdYtDHlPbJg1sDtxx+si/vncsTyFdZPVvWBrCMIf7mOrxSAWlTbQD8ZFvo6lSHkgMlx/mqwnaDxjejotVCEJRIX1MRu46/ISKAH1clHhxHJXH5GlimrX1Zr9MM+Sla5iNKyL0am1z66CDKy+FVGG7XhBd6/MZHwuUIb5F4anHg6MtYZunhhw3H7vyEhnSd/dvYTEXKm/equVzcV9oNfNwQ4kmyqEy0Qt0p8CIor+evNcUCxXyVr4NzgP0TOA==
       * nonce : 874b0b4db7a7
       */

      private String algorithm;
      private String associated_data;
      private String ciphertext;
      private String nonce;

      public String getAlgorithm() {
        return algorithm;
      }

      public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
      }

      public String getAssociated_data() {
        return associated_data;
      }

      public void setAssociated_data(String associated_data) {
        this.associated_data = associated_data;
      }

      public String getCiphertext() {
        return ciphertext;
      }

      public void setCiphertext(String ciphertext) {
        this.ciphertext = ciphertext;
      }

      public String getNonce() {
        return nonce;
      }

      public void setNonce(String nonce) {
        this.nonce = nonce;
      }
    }
  }
}
