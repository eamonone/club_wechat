package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/10 16:37:59
 * @Description: 退款政策接收 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceivedRefundPolicyInfo implements Serializable {

    private Integer refundPercentage1;
    private Integer refundPercentage2;
    private Integer refundPercentage3;
    private Integer refundPercentage4;
    private Integer refundPercentage5;
    private Integer refundPercentage6;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime1;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime2;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime3;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime4;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime5;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private LocalDate refundTime6;
    private Long returnRefundPolicyInfoId;
    private String returnActivityNameInfo;

    public String toJson(ReceivedRefundPolicyInfo receivedRefundPolicyInfo) {

        // ReceivedRefundPolicyInfo 对象转 JSON。
        return JSON.toJSONString(receivedRefundPolicyInfo);

    }

}
