package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityLogistics;
import com.club100.project.wechat.mapper.ActivityLogisticsMapper;
import com.club100.project.wechat.service.IActivityLogisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 12:16:10
 * @Description: 活动后勤保障关联服务实现类。
 */
@Slf4j
@Service
public class ActivityLogisticsServiceImpl implements IActivityLogisticsService {

    @Autowired
    private ActivityLogisticsMapper activityLogisticsMapper;

    @Override
    public int insertActivityLogistics(ActivityLogistics activityLogistics) {
        return activityLogisticsMapper.insertActivityLogistics(activityLogistics);
    }

    @Override
    public int insertActivityLogisticsInBatch(List<ActivityLogistics> activityLogisticsList) {
        return activityLogisticsMapper.insertActivityLogisticsInBatch(activityLogisticsList);
    }

    @Override
    public List<ActivityLogistics> selectActivityLogisticsList(ActivityLogistics activityLogistics) {
        return activityLogisticsMapper.selectActivityLogisticsList(activityLogistics);
    }

    @Override
    public List<ActivityLogistics> selectActivityLogisticsListWithPrivilege(ActivityLogistics activityLogistics) {
        return activityLogisticsMapper.selectActivityLogisticsListWithPrivilege(activityLogistics);
    }

    @Override
    public int deleteActivityLogistics(Long... activityId) {
        return activityLogisticsMapper.deleteActivityLogistics(activityId);
    }

}
