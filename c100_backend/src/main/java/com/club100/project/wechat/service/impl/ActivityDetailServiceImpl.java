package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityDetail;
import com.club100.project.wechat.mapper.ActivityDetailMapper;
import com.club100.project.wechat.service.IActivityDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/21 18:05:54
 * @Description: 活动详情服务实现类。
 */
@Slf4j
@Service
public class ActivityDetailServiceImpl implements IActivityDetailService {

    @Autowired
    private ActivityDetailMapper activityDetailMapper;

    @Override
    public ActivityDetail selectActivityDetailById(ActivityDetail activityDetail) {
        return activityDetailMapper.selectActivityDetailById(activityDetail);
    }

}
