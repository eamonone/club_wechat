package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.RouteCorrection;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 22:29:07
 * @Description: 路线纠错服务类。
 */
public interface IRouteCorrectionService {

    /**
     * 插入路线纠错信息。
     *
     * @param routeCorrection
     * @return
     */
    int insertRouteCorrection(RouteCorrection routeCorrection);

    /**
     * 通过主键查询路线纠错信息。
     *
     * @param routeCorrectionId
     * @return
     */
    RouteCorrection selectRouteCorrectionById(Long routeCorrectionId);

    /**
     * 查询路线纠错信息。
     *
     * @param routeCorrection
     * @return
     */
    List<RouteCorrection> selectRouteCorrection(RouteCorrection routeCorrection);

}
