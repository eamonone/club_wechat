package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.MarkRoute;
import com.club100.project.wechat.domain.MarkRouteUser;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/27 15:22:05
 * @Description: 打卡用户服务类。
 */
public interface IMarkRouteUserService {

    /**
     * 列举打卡用户信息。
     *
     * @param markRoute
     * @return
     */
    List<MarkRouteUser> selectMarkedUserList(MarkRoute markRoute);

    /**
     * 列举打卡用户信息（最多 6 条）。
     *
     * @param markRoute
     * @return
     */
    List<MarkRouteUser> selectMarkedUserAtMostSixList(MarkRoute markRoute);

}
