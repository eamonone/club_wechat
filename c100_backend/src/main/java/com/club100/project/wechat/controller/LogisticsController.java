package com.club100.project.wechat.controller;

import com.club100.framework.web.controller.BaseController;
import com.club100.framework.web.domain.AjaxResult;
import com.club100.framework.web.page.TableDataInfo;
import com.club100.project.wechat.domain.Logistics;
import com.club100.project.wechat.service.ILogisticsService;
import com.club100.project.wechat.utils.EmptyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/20 20:43:44
 * @Description: 后勤保障控制器。
 */
@RequestMapping("/v1/logistics")
@RestController
public class LogisticsController extends BaseController {

    //    private String PREFIX = "/v1/city/";
    private String defaultLogisticsPictureUrl = "https://images.club100.cn/default_picture.png";

    @Autowired
    private ILogisticsService logisticsService;

    /**
     * 获取后勤保障列表。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:list')")
    @GetMapping("/list")
    public TableDataInfo list(Logistics logistics) {

        startPage();
        List<Logistics> list = logisticsService.selectLogisticsList(logistics);
        return getDataTable(list);

    }

    @RequestMapping(value = "/addLogistics")
    public int addLogistics(@RequestBody Logistics logistics) {

        if (EmptyUtils.isEmpty(logistics.getLogisticsPictureUrl()))
            logistics.setLogisticsPictureUrl(defaultLogisticsPictureUrl);
        return logisticsService.insertLogistics(logistics);

    }

    @GetMapping("/selectLogisticsDropdownList")
    public AjaxResult selectLogisticsDropdownList() {

        return AjaxResult.success(logisticsService.selectLogisticsDropdownList());

    }

    /*@GetMapping
    public TableDataInfo page(City city) {

        startPage();
        return getDataTable(cityService.page(city));

    }*/

    /**
     * 根据后勤保障编号获取详细信息。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:query')")
    @RequestMapping(value = "/getLogisticsById")
    public AjaxResult getLogisticsById(@RequestBody Long logisticsId) {

        return AjaxResult.success(logisticsService.selectLogisticsById(logisticsId));

    }

    @RequestMapping(value = "/updateLogistics")
    public int updateLogistics(@RequestBody Logistics logistics) {

        return logisticsService.updateLogistics(logistics);

    }

    /**
     * 删除后勤保障。
     */
//    @PreAuthorize("@ss.hasPermi('system:post:remove')")
//    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{logisticsIds}")
    public AjaxResult deleteById(@PathVariable Long[] logisticsIds) {

        return toAjax(logisticsService.deleteByLogisticsIds(logisticsIds));

    }

}
