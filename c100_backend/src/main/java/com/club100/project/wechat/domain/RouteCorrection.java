package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/22 22:19:53
 * @Description: 路线纠错 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteCorrection extends BaseEntity implements Serializable {

    private Long routeCorrectionId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long userId; // 用户主键。
    private String correctionContent; // 路线报错内容。

    public String toJson(RouteCorrection routeCorrection) {

        // RouteCorrection 对象转 JSON。
        return JSON.toJSONString(routeCorrection);

    }

}
