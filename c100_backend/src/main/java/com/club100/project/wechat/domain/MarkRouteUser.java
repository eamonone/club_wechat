package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/27 11:33:20
 * @Description: 打卡用户 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkRouteUser extends BaseEntity implements Serializable {

    private LocalDateTime markDate; // 用户打卡时间。
    private Long routeId; // 路线主表的主键 ID。
    private Long userId; // 用户主键 ID。
    private String userAvatar; // 用户微信头像 URL。
    private String username; // 用户微信名。
//    private String markTimes; // 用户打卡次数。

    public String toJson(MarkRouteUser markRouteUser) {

        // MarkRouteUser 对象转 JSON。
        return JSON.toJSONString(markRouteUser);

    }

}
