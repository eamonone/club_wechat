package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.ActivityLogisticsLogistics;
import com.club100.project.wechat.mapper.ActivityLogisticsLogisticsMapper;
import com.club100.project.wechat.service.IActivityLogisticsLogisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/17 11:20:02
 * @Description: 活动后勤保障服务实现类。
 */
@Slf4j
@Service
public class ActivityLogisticsLogisticsServiceImpl implements IActivityLogisticsLogisticsService {

    @Autowired
    private ActivityLogisticsLogisticsMapper activityLogisticsLogisticsMapper;

    @Override
    public List<ActivityLogisticsLogistics> selectActivityLogisticsLogisticsList(ActivityLogisticsLogistics activityLogisticsLogistics) {
        return activityLogisticsLogisticsMapper.selectActivityLogisticsLogisticsList(activityLogisticsLogistics);
    }

}
