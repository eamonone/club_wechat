package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/13 09:37:14
 * @Description: 报名活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpActivity extends BaseEntity implements Serializable {

    private Long signUpId; // Primary Key ID。

    private LocalDateTime assembleTime; // 集合时间。
    private Long activityId; // 活动主键。
    private Long userId; // 用户主键。
    private String activityName; // 活动名称。
    private String activityPictureUrl; // 活动图片。

    public String toJson(SignUpActivity signUpActivity) {

        // SignUpActivity 对象转 JSON。
        return JSON.toJSONString(signUpActivity);

    }

}
