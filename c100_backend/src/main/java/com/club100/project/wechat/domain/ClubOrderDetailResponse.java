package com.club100.project.wechat.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * club_order
 * @author 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClubOrderDetailResponse extends ClubOrder {

  private Long bikeShopId; // 车店id。
  private String bikeShopName; // 车店地点。
  private String bikeShopAvatarUrl; // 车店头像
  private String bikeShopPictureUrl; // 车店图片

}