package com.club100.project.wechat.service.impl;

import com.alibaba.fastjson.JSON;
import com.club100.common.constant.WechatConstant;
import com.club100.common.exception.CustomException;
import com.club100.common.utils.StringUtils;
import com.club100.common.utils.http.HttpUtils;
import com.club100.project.insurance.domain.InsuranceOrder;
import com.club100.project.insurance.mapper.InsuranceOrderMapper;
import com.club100.project.insurance.service.InsuranceService;
import com.club100.project.wechat.domain.*;
import com.club100.project.wechat.mapper.*;
import com.club100.project.wechat.service.WxPayService;
import com.club100.project.wechat.utils.OrderCoderUtil;
import com.club100.project.wechat.wxpay.sdk.*;
import com.club100.project.wechat.wxpay.sdk.vo.WxPayNotifyV0;
import com.github.pagehelper.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WxPayServiceImpl implements WxPayService {
  private static final Logger log = LoggerFactory.getLogger(WxPayServiceImpl.class);

  @Autowired private UserMapper userMapper;
  @Autowired private ClubOrderMapper clubOrderMapper;
  @Autowired private ClubRefundMapper clubRefundMapper;
  @Autowired private ClubProductMapper clubProductMapper;
  @Autowired private ClubRefundPolicyMapper clubRefundPolicyMapper;
  @Autowired private ActivityMapper activityMapper;
  @Autowired private InsuranceService insuranceService;
  @Autowired private InsuranceOrderMapper insuranceOrderMapper;

  /** 1.商品表 2.订单表 订单状态。 3.退款表 */

  /**
   * 统一下单接口
   *
   * @param userId
   * @param ipAddress
   * @return
   * @throws Exception
   */
  @Override
  @Transactional(
      propagation = Propagation.REQUIRED,
      isolation = Isolation.DEFAULT,
      timeout = 36000,
      rollbackFor = Exception.class)
  public Map<String, String> wxPay(Long productId, Long userId, String ipAddress) throws Exception {
    String openId =
        Optional.ofNullable(userMapper.selectOneById(User.builder().userId(userId).build()))
            .orElseThrow(() -> new CustomException("用户id不存在"))
            .getWxOpenId();

    // 查询活动信息，校验活动人数
    ClubProduct clubProduct =
        Optional.ofNullable(clubProductMapper.selectByPrimaryKey(productId))
            .orElseThrow(() -> new CustomException("产品id错误"));

    List<ClubOrder> clubOrderList = clubOrderMapper.selectOrderListProductId(productId);
    if (clubOrderList
            .parallelStream()
            .filter(m -> userId.equals(m.getUserId()) && m.getStatus() == 1)
            .count()
        > 0) {
      throw new CustomException("用户已经报名");
    }
    // 当前用户未支付的订单废弃
    if (clubOrderList
            .parallelStream()
            .filter(m -> userId.equals(m.getUserId()) && m.getStatus() == 0)
            .count()
        > 0) {
      for (Integer id :
          clubOrderList
              .parallelStream()
              .filter(m -> userId.equals(m.getUserId()) && m.getStatus() == 0)
              .map(m -> m.getId())
              .collect(Collectors.toList())) {
        clubOrderMapper.updateByPrimaryKeySelective(ClubOrder.builder().id(id).status(2).build());
      }
    }
    List<ClubOrder> list = clubOrderMapper.selectOrderListActivityId(clubProduct.getActivityid());
    Activity activity = activityMapper.selectByPrimaryKey(clubProduct.getActivityid());
    if (list.size() >= activity.getLimitPeopleNumber()) {
      throw new CustomException("名额已满");
    }
    if (StringUtil.isEmpty(activity.getSubMchId())) {
      throw new CustomException("商户号未绑定");
    }

    // 拼接统一下单地址
    Map<String, String> paramMap = new HashMap<>();
    paramMap.put("body", clubProduct.getProductname());
    paramMap.put("sub_appid", WechatConstant.SUB_APPID);
    // 子商户号
    paramMap.put("sub_mch_id", activity.getSubMchId());
    // 支付类型为JSAPI，此参数必传
    paramMap.put("sub_openid", openId);
    // 订单号
    paramMap.put("out_trade_no", OrderCoderUtil.getOrderCode());
    paramMap.put("spbill_create_ip", ipAddress);
    // 支付金额 单位分，即0.01元
    paramMap.put("total_fee", getWxFee(BigDecimal.valueOf(clubProduct.getProductprice())));
    paramMap.put("notify_url", WechatConstant.PAY_NOTIFY_URL);
    paramMap.put("trade_type", "JSAPI");
    // 是否分帐
    paramMap.put("profit_sharing", "Y");

    Map<String, String> map = unifiedOrder(paramMap);

    // 下单失败重试两次
    int reTry = 0;
    while (map.get("return_code").equals(WXPayConstants.FAIL) && reTry < 2) {
      map = unifiedOrder(paramMap);
      reTry++;
    }
    if (reTry == 2) {
      throw new CustomException("支付失败，请重试");
    }
    String prePayId = map.get("prepay_id");

    ClubOrder clubOrder =
        ClubOrder.builder()
            .outTradeNo(paramMap.get("out_trade_no"))
            .userId(userId)
            .orderFee(BigDecimal.valueOf(clubProduct.getProductprice()))
            .prepayId(prePayId)
            .productId(productId)
            .productDescription(clubProduct.getProductname())
            .tradeType(paramMap.get("trade_type"))
            .updateTime(new Date())
            .createTime(new Date())
            .status(0)
            .build();
    clubOrderMapper.insert(clubOrder);

    Map<String, String> payMap = new HashMap<>();
    // 小程序的appId 不是服务商的appId
    payMap.put("appId", WechatConstant.SUB_APPID);
    payMap.put("timeStamp", WXPayUtil.getCurrentTimestamp() + "");
    payMap.put("nonceStr", WXPayUtil.generateNonceStr());

    payMap.put("signType", WXPayConstants.HMACSHA256);
    payMap.put("package", "prepay_id=" + prePayId);

    String paySign =
        WXPayUtil.generateSignature(
            payMap, WechatConstant.MCH_KEY, WXPayConstants.SignType.HMACSHA256);
    payMap.put("paySign", paySign);
    payMap.put("outTradeNo", paramMap.get("out_trade_no"));
    payMap.put("packageStr", payMap.get("package"));
    payMap.put("orderId", clubOrder.getId().toString());
    return payMap;
  }

  private String getWxFee(BigDecimal fee) {
    String wxFee = String.valueOf(fee.multiply(BigDecimal.valueOf(100)));
    if (wxFee.contains(".")) {
      return wxFee.substring(0, wxFee.indexOf("."));
    }
    return wxFee;
  }

  private Map<String, String> unifiedOrder(Map<String, String> paramMap) throws Exception {
    // 支付成功后微信后台回调接口
    WXPayConfig config = new MyWXPayConfig();
    WXPay wxPay = new WXPay(config, WechatConstant.PAY_NOTIFY_URL, false);
    return wxPay.unifiedOrder(paramMap);
  }

  /**
   * 查询服务商查询订单
   *
   * @param outTradeNo 商户订单号
   * @param subMchId 子商户号
   * @return 订单状态等信息
   * @throws Exception
   */
  @Override
  public Map<String, String> orderQuery(String outTradeNo, String subMchId) throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    // 商户订单号  out_trade_no
    paramMap.put("out_trade_no", outTradeNo);
    paramMap.put("sub_mch_id", subMchId);

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);

    Map<String, String> response = wxPay.orderQuery(paramMap);
    log.info("交易状态:" + response.get("trade_state"));
    log.info("交易response:" + JSON.toJSONString(response));
    // todo 保存订单交易状态
    return response;
  }

  @Override
  public void orderStatus(Integer id, Integer status, Long insuranceId) throws Exception {
    if (!Arrays.asList(1, 2).contains(status)) {
      throw new CustomException("状态信息错误");
    }
    ClubOrder clubOrder =
        Optional.ofNullable(clubOrderMapper.selectByPrimaryKey(id))
            .orElseThrow(() -> new CustomException("订单id错误"));
    if (status == 2) {
      clubOrderMapper.updateByPrimaryKeySelective(ClubOrder.builder().id(id).status(2).build());
      return;
    }

    if (StringUtil.isEmpty(clubOrder.getSubMchId())) {
      throw new CustomException("商户号未绑定");
    }

    InsuranceOrder insuranceOrder = insuranceOrderMapper.selectByPrimaryKey(insuranceId);
    if (insuranceOrder == null || insuranceOrder.getStatus() == 2) {
      throw new CustomException("用户保单id错误");
    }

    if(insuranceOrder.getSinglePrice() >= clubOrder.getOrderFee().multiply(BigDecimal.valueOf(100)).longValue()){
      throw new CustomException("保单金额不能大于订单金额");
    }

    if (clubOrder.getTransactionId() == null) {
      Map<String, String> orderMap = orderQuery(clubOrder.getOutTradeNo(), clubOrder.getSubMchId());
      clubOrder.setTransactionId(orderMap.get("transaction_id"));

    }
    if (clubOrder.getProfitSharingFee() == null) {
      // 发起分帐
      Map<String, String> response = profitsharing(insuranceOrder.getSinglePrice().intValue(),clubOrder.getSubMchId(),clubOrder.getTransactionId());

      if (StringUtil.isEmpty(response.get("out_order_no"))) {
        clubOrder.setReturnMsg(response.get("err_code_des"));
      } else {
        clubOrder.setOutOrderNo(response.get("out_order_no"));
      }
      if (insuranceOrder.getStatus() == 0) {
        try {
          insuranceService.payInsurance(insuranceOrder, clubOrder.getOutTradeNo());
        } catch (Exception e) {
          insuranceService.payInsurance(insuranceOrder, clubOrder.getOutTradeNo());
        }
      }
      //记录分帐金额
      clubOrder.setProfitSharingFee(new BigDecimal(insuranceOrder.getSinglePrice()/100));
    }

    clubOrder.setId(id);
    clubOrder.setStatus(status);
    clubOrder.setProfitSharingAccount(WechatConstant.MCH_ID);

    clubOrderMapper.updateByPrimaryKeySelective(clubOrder);
  }

  /**
   * 发起分帐
   *
   * @param amount
   * @param sub_mch_id
   * @param transaction_id
   * @throws Exception
   */
  public Map<String, String> profitsharing(int amount, String sub_mch_id, String transaction_id)
      throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    Receiver receiver =
        Receiver.builder()
            .name("上海克勒布百体育发展有限公司")
            .type("MERCHANT_ID")
            // 分帐到服务商账户
            .account(WechatConstant.MCH_ID)
            .relation_type("SERVICE_PROVIDER")
            .build();

    Map<String, String> result = profitsharingaddreceiver(sub_mch_id, receiver);
    receiver.setAmount(amount);
    receiver.setDescription("分帐到服务商");
    List<Receiver> receivers = Arrays.asList(receiver);

    paramMap.put("sub_mch_id", sub_mch_id);
    // 微信订单号
    paramMap.put("transaction_id", transaction_id);
    // 商户分帐单号
    paramMap.put("out_order_no", OrderCoderUtil.getProfitsharingCode());
    paramMap.put("receivers", JSON.toJSONString(receivers));

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);

    Map<String, String> response = wxPay.profitsharing(paramMap);

    log.info("分帐response:" + JSON.toJSONString(response));
    return response;
  }

  public static void main(String[] args) {
    try {
      //
//            new WxPayServiceImpl().orderQuery("12020112520474756245292","1603624031");
//     new WxPayServiceImpl().profitsharingquery("1603624031", "4200000814202011061059194897", "12020110620575783074839");

//      Map<String, String> paramMap = new HashMap<>();
//      // 商户订单号  out_trade_no
//      paramMap.put("out_trade_no", "12020112520474756245292");
//      // 退款单号 唯一，重复提交只会退一次
//      paramMap.put("out_refund_no", "32020112520491713141792");
//      // 单位为分，只能为整数
//      paramMap.put("total_fee", "500");
//
//      paramMap.put("refund_fee", "500");
//      paramMap.put("sub_mch_id", "1603624031");
//      paramMap.put("refund_account","REFUND_SOURCE_RECHARGE_FUNDS");
//      WXPay wxPay = new WXPay(new MyWXPayConfig(), null, false);
//      Map<String, String> response = wxPay.refund(paramMap);
//      log.info("退款response:" + JSON.toJSONString(response));

//      new WxPayServiceImpl().refundQuery("12020112520474756245292");
      new WxPayServiceImpl().orderQuery("12020111720321012086513","1603624031");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Map<String, String> profitsharingaddreceiver(String sub_mch_id, Receiver receiver)
      throws Exception {
    Map<String, String> paramMap = new HashMap<>();

    paramMap.put("sub_mch_id", sub_mch_id);
    // 分帐方信息
    paramMap.put("receiver", JSON.toJSONString(receiver));

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);
    Map<String, String> response = wxPay.profitsharingaddreceiver(paramMap);

    log.info("新增分帐方response:" + JSON.toJSONString(response));
    return response;
  }

  // 分帐结果查询
  public void profitsharingquery(String sub_mch_id, String transaction_id, String out_order_no)
      throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    paramMap.put("sub_mch_id", sub_mch_id);
    // 微信订单号
    paramMap.put("transaction_id", transaction_id);
    // 商户分帐单号
    paramMap.put("out_order_no", out_order_no);

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);
    Map<String, String> response = wxPay.profitsharingquery(paramMap);

    log.info("分帐查询response:" + JSON.toJSONString(response));
  }

  // 分帐完成
  // 每天1点执行
//  @Scheduled(cron = "0 0 1 * * ?")
  public void profitsharingfinish() {
    log.info("========分帐完成start=======");
    List<Long> activityIds = clubRefundPolicyMapper.getFinishActivityId();
    if (activityIds.size() > 0) {
      List<ClubOrder> clubOrderList = clubOrderMapper.selectFinishOrder(activityIds);
      for (ClubOrder clubOrder : clubOrderList) {
        try {
          profitsharingfinish(
              clubOrder.getSubMchId(), clubOrder.getTransactionId(), clubOrder.getOutOrderNo());
          clubOrderMapper.updateByPrimaryKeySelective(
              ClubOrder.builder().id(clubOrder.getId()).isfinish(1).build());
        } catch (Exception e) {
          log.error("分帐完成报错,订单id:" + clubOrder.getId() + e.getMessage());
        }
      }
    }
    log.info("========分帐完成end=======");
  }

  private boolean responseIsOk(Map<String,String> response){
    if(WXPayConstants.SUCCESS.equals(response.get("return_code")) && WXPayConstants.SUCCESS.equals(response.get("result_code"))){
      return true;
    }
    return false;
  }

  /** 退款失败的话，定时任务执行重新退款 每天两点执行 */
  @Scheduled(cron = "0 0 2 * * ?")
  public void refundFailFix() {
    log.info("========重新退款定时任务start=======");
    List<ClubRefund> refundList = clubRefundMapper.selectRefundFailInfo();

    if (refundList.size() > 0) {
      for (ClubRefund clubRefund : refundList) {
        try {
          InsuranceOrder insuranceOrder =
              insuranceOrderMapper.selectByOutTradeNo(clubRefund.getOutTradeNo());
          if(insuranceOrder != null){
              if (insuranceOrder.getStatus() == null || insuranceOrder.getStatus() != 2) {
                insuranceService.surrenderPolicy(insuranceOrder);
              }
          }
          ClubOrder clubOrder = clubOrderMapper.selectByOutTradeNo(clubRefund.getOutTradeNo());
          if (clubOrder == null) {
            return;
          } else{
            Map<String, String> orderDetailMap = orderQuery(clubOrder.getOutTradeNo(),clubOrder.getSubMchId());
            if ("NOTPAY".equals(orderDetailMap.get("trade_state"))){
              clubOrder.setStatus(2);
              clubOrder.setReturnMsg(orderDetailMap.get("trade_state_desc"));
              clubOrder.setUpdateTime(new Date());
              clubOrderMapper.updateByPrimaryKey(clubOrder);
              clubRefundMapper.deleteByPrimaryKey(clubRefund.getId());
              return;
            }
          }
          if (clubRefund.getProfitRefundStatus() == null
              || clubRefund.getProfitRefundStatus() != 1) {
            profitsharingreturn(
                clubRefund,
                clubOrder.getSubMchId(),
                clubOrder.getOutOrderNo(),
                clubRefund.getProfitRefundFee());
          }

          Map<String, String> paramMap = new HashMap<>();
          // 商户订单号  out_trade_no
          paramMap.put("out_trade_no", clubRefund.getOutTradeNo());
          // 退款单号 唯一，重复提交只会退一次
          paramMap.put("out_refund_no", OrderCoderUtil.getRefundCode());
          // 单位为分，只能为整数
          paramMap.put("total_fee", getWxFee(clubRefund.getTotalFee()));

          paramMap.put("refund_fee", getWxFee(clubRefund.getRefundFee()));
          paramMap.put("sub_mch_id", clubOrder.getSubMchId());
          WXPay wxPay = new WXPay(new MyWXPayConfig(), null, false);
          Map<String, String> response = wxPay.refund(paramMap);
          log.info("退款response:" + JSON.toJSONString(response));

          if (response.get("return_code").equals(WXPayConstants.SUCCESS)
              && (response.get("result_code").equals(WXPayConstants.SUCCESS)
                  || response.get("err_code_des").equals("订单已全额退款"))) {
            clubRefund.setStatus(1);
            // 更新订单状态
            clubOrderMapper.updateByPrimaryKeySelective(
                ClubOrder.builder().id(clubOrder.getId()).status(2).build());
            clubRefund.setReturnMsg(response.get("return_msg"));
          } else {
            clubRefund.setReturnMsg(response.get("return_msg"));
          }
          if(response.get("result_code").equals(WXPayConstants.FAIL)){
            clubRefund.setReturnMsg(response.get("err_code_des"));
          }
          clubRefundMapper.updateByPrimaryKeySelective(clubRefund);

        } catch (Exception e) {
          log.error("定时任务重新退款报错，退款单id:" + clubRefund.getId() + e.getMessage());
        }
      }
    }
    log.info("========重新退款定时任务end=======");
  }

  // 分帐完成
  public void profitsharingfinish(String sub_mch_id, String transaction_id, String out_order_no)
      throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    paramMap.put("sub_mch_id", sub_mch_id);
    // 微信订单号
    paramMap.put("transaction_id", transaction_id);
    // 商户分帐单号
    paramMap.put("out_order_no", out_order_no);
    paramMap.put("description", "分帐已完成");

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);
    Map<String, String> response = wxPay.profitsharingfinish(paramMap);

    log.info("分帐完成response:" + JSON.toJSONString(response));
  }

  /**
   * 分账回退
   *
   * @param subMachId 子商户id
   * @param out_order_no 商户分账单号(分帐后返回)
   * @param return_amount 退款金额
   * @throws Exception
   */
  public void profitsharingreturn(
      ClubRefund clubRefund, String subMachId, String out_order_no, String return_amount)
      throws Exception {

    Map<String, String> paramMap = new HashMap<>();
    paramMap.put("sub_mch_id", subMachId);
    // 商户分账单号(分帐后返回)
    paramMap.put("out_order_no", out_order_no);
    // 商户分帐单号
    paramMap.put("out_return_no", OrderCoderUtil.getProfitsharingreturnCode());
    paramMap.put("return_account_type", "MERCHANT_ID");
    paramMap.put("return_account", WechatConstant.MCH_ID);
    paramMap.put("return_amount", return_amount);
    paramMap.put("description", "用户退款");

    try {
      WXPay wxPay = new WXPay(new MyWXPayConfig(), false);
      Map<String, String> response = wxPay.profitsharingreturn(paramMap);
      log.info("分帐回退response:" + JSON.toJSONString(response));

      clubRefund.setProfitRefundFee(return_amount);
      if (response.get("return_code").equals(WXPayConstants.SUCCESS)) {
        clubRefund.setProfitRefundStatus(1);
        clubRefund.setProfitRefundResponse(response.get("return_msg"));
      } else {
        clubRefund.setProfitRefundStatus(2);
        clubRefund.setProfitRefundResponse(response.get("return_msg"));
      }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  @Override
  public ClubOrderDetailResponse orderDetailQuery(Long orderId) throws Exception {
    ClubOrder clubOrder =
        Optional.ofNullable(clubOrderMapper.selectByPrimaryKey(orderId.intValue()))
            .orElseThrow(() -> new CustomException("订单信息不存在"));

    if (clubOrder.getStatus() == 0) {
      clubOrder = clubOrderMapper.selectByPrimaryKey(clubOrder.getId());
      Map<String, String> response = orderQuery(clubOrder.getOutTradeNo(), clubOrder.getSubMchId());
      if (response.get("return_code").equals(WXPayConstants.SUCCESS)) {
        if (WXPayConstants.SUCCESS.equals(response.get("result_code"))) {
          clubOrder.setStatus(1);
          clubOrderMapper.updateByPrimaryKeySelective(clubOrder);
          clubOrder = clubOrderMapper.selectByPrimaryKey(clubOrder.getId());
        } else {
          // 订单未支付成功则返回空
          return new ClubOrderDetailResponse();
        }
      }
    }
    ActivityExt activity =
        Optional.ofNullable(activityMapper.selectActivityByProductId(clubOrder.getProductId()))
            .orElseThrow(() -> new CustomException("产品id错误"));

    ClubOrderDetailResponse clubOrderDetailResponse = new ClubOrderDetailResponse();
    BeanUtils.copyProperties(clubOrder, clubOrderDetailResponse);
    clubOrderDetailResponse.setActivityName(activity.getActivityName());
    clubOrderDetailResponse.setActivityPictureUrl(activity.getActivityPictureUrl());
    clubOrderDetailResponse.setBikeShopId(activity.getBikeShopId());
    clubOrderDetailResponse.setBikeShopName(activity.getBikeShopName());
    clubOrderDetailResponse.setBikeShopAvatarUrl(activity.getBikeShopAvatarUrl());
    clubOrderDetailResponse.setBikeShopPictureUrl(activity.getBikeShopPictureUrl());

    return clubOrderDetailResponse;
  }

  /**
   * 关闭订单
   *
   * @param outTradeNo
   * @return
   * @throws Exception
   */
  @Override
  public Map<String, String> closeOrder(String outTradeNo) throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    // 商户订单号  out_trade_no
    paramMap.put("out_trade_no", outTradeNo);

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);

    Map<String, String> response = wxPay.closeOrder(paramMap);
    log.info("关闭订单response:" + JSON.toJSONString(response));
    // todo 修改订单状态
    return response;
  }

  /**
   * 获取退款金额
   *
   * @param orderId 订单id
   * @throws CustomException
   */
  @Override
  public ClubRefundPolicyResponse getRefundMoney(Integer orderId) throws CustomException {
    ClubOrder clubOrder =
        Optional.ofNullable(clubOrderMapper.selectByPrimaryKey(orderId))
            .orElseThrow(() -> new CustomException("订单id错误"));

    ActivityExt activity =
        Optional.ofNullable(activityMapper.selectActivityByProductId(clubOrder.getProductId()))
            .orElseThrow(() -> new CustomException("产品id错误"));

    ClubRefundPolicy clubRefundPolicy =
        Optional.ofNullable(clubRefundPolicyMapper.selectRefundPolicyByActivityId(activity.getId()))
            .orElseThrow(() -> new CustomException("临近活动开始不能退款"));

    // 四舍五入保留两位小数
    BigDecimal refundFee =
        clubOrder
            .getOrderFee()
            .multiply(BigDecimal.valueOf(clubRefundPolicy.getRefundpercentage()))
            .setScale(2, BigDecimal.ROUND_HALF_UP);

    return ClubRefundPolicyResponse.builder()
        .activityId(activity.getId())
        .activityName(activity.getActivityName())
        .orderId(orderId)
        .orderFee(clubOrder.getOrderFee())
        .refundFee(refundFee)
        .build();
  }

  /**
   * 退款
   *
   * @param orderId 订单id
   * @throws Exception
   */
  @Override
  @Transactional(
      propagation = Propagation.REQUIRED,
      isolation = Isolation.DEFAULT,
      timeout = 36000,
      rollbackFor = Exception.class)
  public void refund(Integer orderId) throws Exception {
    ClubOrder clubOrder =
        Optional.ofNullable(clubOrderMapper.selectByPrimaryKey(orderId))
            .orElseThrow(() -> new CustomException("订单id错误"));

    if (clubOrder.getStatus() != 1) {
      throw new CustomException("订单状态错误");
    }

    ClubRefund clubRefund = clubRefundMapper.selectByOutTradeNo(clubOrder.getOutTradeNo());
    if (clubRefund != null) {
      throw new CustomException("退款受理中，请耐心等待");
    }
    // 查询退款政策
    ClubRefundPolicyResponse clubRefundPolicyResponse = getRefundMoney(orderId);
    // 退款金额小于0.01 不能退款
    if (clubRefundPolicyResponse.getRefundFee().compareTo(new BigDecimal("0.01")) < 0) {
      throw new CustomException("临近活动开始不能退款");
    }

    Map<String, String> paramMap = new HashMap<>();
    // 商户订单号  out_trade_no
    paramMap.put("out_trade_no", clubOrder.getOutTradeNo());
    // 退款单号 唯一，重复提交只会退一次
    paramMap.put("out_refund_no", OrderCoderUtil.getRefundCode());
    // 单位为分，只能为整数
    paramMap.put("total_fee", getWxFee(clubOrder.getOrderFee()));

    // 实际退款金额等于总金额 * 百分比
    BigDecimal refundFee = clubRefundPolicyResponse.getRefundFee();
    paramMap.put("refund_fee", getWxFee(refundFee));
    paramMap.put("sub_mch_id", clubOrder.getSubMchId());

    clubRefund =
        ClubRefund.builder()
            .userId(clubOrder.getUserId())
            .outTradeNo(clubOrder.getOutTradeNo())
            .outRefundNo(paramMap.get("out_refund_no"))
            .totalFee(clubOrder.getOrderFee())
            .refundFee(refundFee)
            .createTime(new Date())
            .updateTime(new Date())
            .status(0)
            .build();

    // 退保
    InsuranceOrder insuranceOrder = insuranceOrderMapper.selectByOutTradeNo(clubOrder.getOutTradeNo());
    if(insuranceOrder != null){
      try{
      insuranceService.surrenderPolicy(insuranceOrder);
      } catch (Exception e){
         log.info("保单退保失败：" + e.getMessage());
      }
    }

    // 分帐回退
    if(clubOrder.getIsfinish() != null && clubOrder.getIsfinish() == 1){
      profitsharingreturn(clubRefund, clubOrder.getSubMchId(), clubOrder.getOutOrderNo(), insuranceOrder.getSinglePrice().toString());
    }

    WXPay wxPay = new WXPay(new MyWXPayConfig(), null, false);
    Map<String, String> response = wxPay.refund(paramMap);
    log.info("退款response:" + JSON.toJSONString(response));

    if (responseIsOk(response)) {
      clubRefund.setStatus(1);
      // 更新订单状态
      clubOrderMapper.updateByPrimaryKeySelective(ClubOrder.builder().id(orderId).status(2).build());
    } else {
      clubRefund.setStatus(2);
    }
    clubRefund.setReturnMsg(response.get("return_msg") != null ? response.get("return_msg") : "");
    clubRefundMapper.insert(clubRefund);
  }

  @Override
  public ClubRefund refundDetail(Integer orderId) throws Exception {
    ClubOrder clubOrder =
        Optional.ofNullable(clubOrderMapper.selectByPrimaryKey(orderId))
            .orElseThrow(() -> new CustomException("订单id错误"));
    ClubRefund clubRefund = clubRefundMapper.selectByOutTradeNo(clubOrder.getOutTradeNo());
    // 待退款查询退款进度并更新
    if (clubRefund.getStatus() == 0) {
      Map<String, String> response = refundQuery(clubRefund.getOutTradeNo());
      if (response.get("return_code").equals(WXPayConstants.SUCCESS)) {
        if (response.get("result_code").equals(WXPayConstants.SUCCESS)) {
          clubRefund.setStatus(1);
          clubRefund.setRefundStatus(response.get("refund_status"));
          clubRefundMapper.updateByPrimaryKeySelective(clubRefund);
        }
      }
    }
    return clubRefund;
  }

  /**
   * 退款查询
   *
   * @param outTradeNo 商户订单号
   * @return
   * @throws Exception
   */
  public Map<String, String> refundQuery(String outTradeNo) throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    // 商户订单号  out_trade_no
    paramMap.put("out_trade_no", outTradeNo);
    paramMap.put("sub_mch_id", "1603624031");

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);

    Map<String, String> response = wxPay.refundQuery(paramMap);
    log.info("退款查询response:" + JSON.toJSONString(response));
    return response;
  }

  /**
   * 下载对账单 次日9点启动生成前一天的对账单，建议10点后再获取 做定时任务去拉取，或是在调用的时候在去拉取
   *
   * @param billDate
   * @return
   * @throws Exception
   */
  @Override
  public Map<String, String> downloadBill(String billDate) throws Exception {
    Map<String, String> paramMap = new HashMap<>();
    // 对账日期 格式：20140603
    paramMap.put("bill_date", billDate);

    WXPay wxPay = new WXPay(new MyWXPayConfig(), false);

    Map<String, String> response = wxPay.downloadBill(paramMap);
    log.info("对账单response:" + JSON.toJSONString(response));
    // todo 记录对账单
    return response;
  }

  @Override
  public String paySuccessNotify(WxPayNotifyV0 param) throws Exception {
    Map<String, String> result = new HashMap<>();
    // todo 修改订单支付状态
    // todo 需要做接口防重复调用校验

    if ("SUCCESS".equals(param.getReturn_code())) {
      // db order.update  status success
      result.put("return_code", "SUCCESS");
      result.put("return_msg", "OK");
    }
    return WXPayUtil.mapToXml(result);
  }
}
