package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/15 10:36:46
 * @Description: 后勤保障 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Logistics extends BaseEntity implements Serializable {

    private Long logisticsId; // Primary Key ID。

    private Integer state; // 状态（1：可用；2：废弃。）。
    private String logisticsName; // 后勤保障名称。
    private String logisticsPictureUrl; // 后勤保障图片链接 URL。

    public String toJson(Logistics logistics) {

        // Logistics 对象转 JSON。
        return JSON.toJSONString(logistics);

    }

}
