package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.SignUpActivity;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/13 10:06:40
 * @Description: 报名活动服务类。
 */
public interface ISignUpActivityService {

    /**
     * 列举报名活动信息。
     *
     * @param signUpActivity
     * @return
     */
    List<SignUpActivity> selectSignUpActivity(SignUpActivity signUpActivity);

}
