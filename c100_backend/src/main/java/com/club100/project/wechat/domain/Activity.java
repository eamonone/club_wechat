package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Activity extends BaseEntity implements Serializable {

    private Long activityId; // Primary Key ID。

    private Double rideDistance; // 骑行距离（单位：km）。
    private Double rideTimeLong; // 骑行时长（单位：小时）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Integer activityDays; // 活动天数。
    private Integer activityFeeType; // 费用类型（1：免费；2：线上收费；3：线下收费。）。
    private Integer activityFrequency; // 活动频率。
    private Integer activityType; // 活动类型（）。
    private Integer limitPeopleNumber; // 人数上限。
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime activityBeginTime; // 活动开始时间。
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime activityEndTime; // 活动结束时间。
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime assembleTime; // 集合时间。
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signUpEndTime; // 报名截止时间。
    private Long bikeShopId; // 车店主键。
    private String subMchId; // 子商户号。
    private Long leaderId; // 领队主键。
    private Long sysUserId; // 管理后台登录用户主键。
    private String activityDescription; // 活动详情介绍。
    private String activityLocation; // 活动地点。
    private String activityName; // 活动名称。
    private String activityPictureUrl; // 活动图片。
    private String assembleLocation; // 集合地点。
    private String contactName; // 领队姓名。
    private String contactPhone; // 领队电话。
    private String contactWeChat; // 领队微信号。
    private String rideBikeType; // 骑行车型（1：公路；2：山地；3：小折；4：不限。）。
    private String rideTerrain; // 骑行地形（1：平路；2：起伏；3：爬坡。）。
    private String activityAudit; // 活动审核结果。
    private String bikeShopName; // 车店名称。
    private String activityQrcodeUrl; // 活动二维码。
    private Integer activityDay; // 活动周期。
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime activityTime; // 活动时间。

    public String toJson(Activity activity) {

        // Activity 对象转 JSON。
        return JSON.toJSONString(activity);

    }

}
