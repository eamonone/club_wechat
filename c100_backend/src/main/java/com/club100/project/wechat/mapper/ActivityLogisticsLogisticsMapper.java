package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityLogisticsLogistics;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/17 11:10:04
 * @Description: 活动后勤保障 Mapper 接口。
 */
public interface ActivityLogisticsLogisticsMapper {

    List<ActivityLogisticsLogistics> selectActivityLogisticsLogisticsList(ActivityLogisticsLogistics activityLogisticsLogistics);

}
