package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.PizzaGetter;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 12:21:30
 * @Description: 披萨获得者 Mapper 接口。
 */
public interface PizzaGetterMapper {

    int insertPizzaGetter(PizzaGetter pizzaGetter);

    int updatePizzaGetter(PizzaGetter pizzaGetter);

    int selectPizzaGetterCount();

}
