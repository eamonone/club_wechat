package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.BadgeUser;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 16:34:05
 * @Description: 徽章获得用户 Mapper 接口。
 */
public interface BadgeUserMapper {

    int insertBadgeUser(BadgeUser badgeUser);

    BadgeUser selectOneBadgeUser(BadgeUser badgeUser);

}
