package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 10:25:18
 * @Description: 激励 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Inspire extends BaseEntity implements Serializable {

    private Long inspireId; // Primary Key ID。

    private Integer state; // 激励状态（1：可用；2：废弃。）。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private String inspireDescription; // 激励描述。
    private String inspirePicUrl; // 激励图片 URL。
    private String inspireTitle; // 激励标题。

    public String toJson(Inspire inspire) {

        // Inspire 对象转 JSON。
        return JSON.toJSONString(inspire);

    }

}
