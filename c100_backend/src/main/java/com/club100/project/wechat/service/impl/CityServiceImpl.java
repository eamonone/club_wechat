package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.City;
import com.club100.project.wechat.mapper.CityMapper;
import com.club100.project.wechat.service.ICityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/19 21:03:48
 * @Description: 城市服务实现类。
 */
@Slf4j
@Service
public class CityServiceImpl implements ICityService {

    @Autowired
    private CityMapper cityMapper;

    @Override
    public List<City> page(City city) {
        return cityMapper.selectCityList(city);
    }

    @Override
    public List<City> selectCityListOrderById() {
        return cityMapper.selectCityListOrderById();
    }

    @Override
    public List<City> selectCityById(Long cityId) {
        return cityMapper.selectCityById(cityId);
    }

    @Override
    public int updateCity(City city) {
        return cityMapper.updateCity(city);
    }

    @Override
    public int insert(City city) {
        return cityMapper.insertCity(city);
    }

    @Override
    @Transactional
    public int deleteByCityIds(Long... id) {
        return cityMapper.deleteByCityIds(id);
    }

    @Override
    public List<City> selectCityDropdownList() {
        return cityMapper.selectCityDropdownList();
    }

}
