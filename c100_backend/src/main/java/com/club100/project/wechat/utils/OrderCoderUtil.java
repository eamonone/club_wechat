package com.club100.project.wechat.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.DateUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @Author:
 * @Date: 2020/09/13 14:31:06
 * @Description: 生产订单编号
 */
public class OrderCoderUtil {


  /** 订单类别头 */
  private static final String ORDER_CODE = "1";
  /** 退货类别头 */
  private static final String RETURN_ORDER = "2";
  /** 退款类别头 */
  private static final String REFUND_ORDER = "3";
  /** 未付款重新支付别头 */
  private static final String AGAIN_ORDER = "4";
  //商户分帐单号
  private static final String PROFITSHARING_ORDER = "5";

  private static final String INSURANCE_ORDER = "C100";



  /**
   * 生成订单单号编码
   */
  public static String getOrderCode(){
    return ORDER_CODE + getCode();
  }


  /**
   * 生成退货单号编码
   */
  public static String getReturnCode(){
    return RETURN_ORDER + getCode();
  }


  /**
   * 生成退款单号编码
   */
  public static String getRefundCode(){
    return REFUND_ORDER + getCode();
  }

  public static String getProfitsharingCode(){
    return PROFITSHARING_ORDER + getCode();
  }

  public static String getProfitsharingreturnCode(){
    return "R" + getCode();
  }

  /**
   * 生成保险单号
   *
   * @return
   */
  public static String getInsuranceCode(){
    return INSURANCE_ORDER + getCode(7);
  }


  /**
   * 未付款重新支付
   */
  public static String getAgainCode(){
    return AGAIN_ORDER + getCode();
  }


  /**
   * 生成时间戳
   */
  private static String getDateTime(){
    DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    return sdf.format(new Date());
  }


  private static String getCode(){
    return getDateTime() + RandomStringUtils.randomNumeric(8);
  }

  private static String getCode(int count){
    return getDateTime() + RandomStringUtils.randomNumeric(count);
  }

  public static void main(String[] args) {
    System.out.println(getInsuranceCode());
  }
}
