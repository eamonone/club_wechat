package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/29 15:40:53
 * @Description: 分页 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageInfo implements Serializable {

    private Integer pageNo;
    private Integer pageSize;

    public String toJson(PageInfo pageInfo) {

        // PageInfo 对象转 JSON。
        return JSON.toJSONString(pageInfo);

    }

}
