package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ActivityPicture;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/12 14:30:55
 * @Description: 活动详情图片 Mapper 接口。
 */
public interface ActivityPictureMapper {

    List<ActivityPicture> selectActivityPicture(ActivityPicture activityPicture);

    List<ActivityPicture> selectActivityPictureWithPrivilege(ActivityPicture activityPicture);

    int insertActivityPicture(ActivityPicture activityPicture);

    int insertActivityPictureInBatch(List<ActivityPicture> activityPictureList);

    int deleteActivityPicture(Long... activityId);

}
