package com.club100.project.wechat.mapper;

import com.club100.project.wechat.domain.ClubRefund;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ClubRefundMapper {
  int deleteByPrimaryKey(Integer id);

  int insert(ClubRefund record);

  int insertSelective(ClubRefund record);

  ClubRefund selectByPrimaryKey(Integer id);

  /**
   * 通过订单号查询退款单信息
   *
   * @param outTradeNo 订单号
   * @return
   */
  @Select("select * from club_refund where out_trade_no = #{outTradeNo} limit 1")
  @ResultMap("BaseResultMap")
  ClubRefund selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);

  /**
   * 查询退款失败的退款单
   *
   * @return
   */
  @Select("select * from club_refund where status = 2 ")
  @ResultMap("BaseResultMap")
  List<ClubRefund> selectRefundFailInfo();

  int updateByPrimaryKeySelective(ClubRefund record);

  int updateByPrimaryKey(ClubRefund record);
}
