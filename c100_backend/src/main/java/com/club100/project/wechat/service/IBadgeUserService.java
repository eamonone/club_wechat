package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.BadgeUser;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/18 16:42:32
 * @Description: 徽章获得用户服务类。
 */
public interface IBadgeUserService {

    /**
     * 插入一条徽章获得用户信息。
     *
     * @param badgeUser
     * @return
     */
    int insertBadgeUser(BadgeUser badgeUser);

    /**
     * 查询一条徽章获得用户信息。
     *
     * @param badgeUser
     * @return
     */
    BadgeUser selectOneBadgeUser(BadgeUser badgeUser);

}
