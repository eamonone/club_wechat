package com.club100.project.wechat.utils;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/07/22 19:48:27
 * @Description: JSON 相关工具类。
 */
public class JsonUtils {

    // 私有 JsonUtils 构造类，让外界不能创建对象，只能静态调用方法。
    private JsonUtils() {

        throw new UnsupportedOperationException("'JsonUtils' can't be instantiated~");

    }

    public static String S_listToJsonArray(List modifiedList) {

        return JSON.toJSONString(modifiedList);

    }

}
