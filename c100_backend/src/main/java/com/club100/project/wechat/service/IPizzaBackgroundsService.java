package com.club100.project.wechat.service;

import com.club100.project.wechat.domain.PizzaBackgrounds;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/13 14:31:50
 * @Description: 披萨基本情况服务类。
 */
public interface IPizzaBackgroundsService {

    /**
     * 插入一条披萨基本情况信息。
     *
     * @param pizzaBackgrounds
     * @return
     */
    int insertPizzaBackgrounds(PizzaBackgrounds pizzaBackgrounds);

    /**
     * 通过主键查询一条披萨基本情况信息。
     *
     * @param pizzaBackgroundsId
     * @return
     */
    PizzaBackgrounds selectPizzaBackgroundsById(Long pizzaBackgroundsId);

}
