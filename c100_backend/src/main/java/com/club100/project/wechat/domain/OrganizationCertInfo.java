package com.club100.project.wechat.domain;

import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/09/10 14:55:43
 * @Description: 活动 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationCertInfo extends BaseEntity implements Serializable {

  /**
   * organization_copy : 47ZC6GC-vnrbEny__Ie_An5-tCpqxucuxi-vByf3Gjm7KE53JXvGy9tqZm2XAUf-4K  GprrKhpVBDIUv0OF4wFNIO4kqg05InE4d2I6_H7I4
   * organization_time : ["2014-01-01","长期"]
   * organization_number : 12345679-A
   */

  private String organization_copy;
  private String organization_time;
  private String organization_number;

  public String getOrganization_copy() {
    return organization_copy;
  }

  public void setOrganization_copy(String organization_copy) {
    this.organization_copy = organization_copy;
  }

  public String getOrganization_time() {
    return organization_time;
  }

  public void setOrganization_time(String organization_time) {
    this.organization_time = organization_time;
  }

  public String getOrganization_number() {
    return organization_number;
  }

  public void setOrganization_number(String organization_number) {
    this.organization_number = organization_number;
  }
}
