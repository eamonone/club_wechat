package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/8/10 16:19:48
 * @Description: 路线详情视频 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteVideo extends BaseEntity implements Serializable {

    private Long routeVideoId; // Primary Key ID。

    private Integer state; // 收藏路线状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主表的主键 ID。
    private String routeVideoUrl; // 路线视频链接 URL。

    public String toJson(RouteVideo routeVideo) {

        // RoutePicture 对象转 JSON。
        return JSON.toJSONString(routeVideo);

    }

}
