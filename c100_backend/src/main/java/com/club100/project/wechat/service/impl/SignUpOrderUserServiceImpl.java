package com.club100.project.wechat.service.impl;

import com.club100.project.wechat.domain.SignUpOrderUser;
import com.club100.project.wechat.mapper.SignUpOrderUserMapper;
import com.club100.project.wechat.service.ISignUpOrderUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/11/08 21:33:08
 * @Description: 报名人数服务实现类。
 */
@Slf4j
@Service
public class SignUpOrderUserServiceImpl implements ISignUpOrderUserService {

    @Autowired
    private SignUpOrderUserMapper signUpOrderUserMapper;

    @Override
    public List<SignUpOrderUser> selectSignUpOrderUser(SignUpOrderUser signUpOrderUser) {
        return signUpOrderUserMapper.selectSignUpOrderUser(signUpOrderUser);
    }

    @Override
    public int selectSignUpOrderUserCount(SignUpOrderUser signUpOrderUser) {
        return signUpOrderUserMapper.selectSignUpOrderUserCount(signUpOrderUser);
    }

}
