package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/7/28 15:34:25
 * @Description: 路线详情 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RouteDetail extends BaseEntity implements Serializable {

    private Long routeDetailId; // Primary Key ID。

    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主表的主键 ID。
    private String gpxUrl; // GPX 文件的下载 URL。
    private String routeProvider; // 路线提供车店名称。
    private String routeDistrict; // 路线地区名称。
    private String jsonUrl; // json 文件的下载 URL。

    public String toJson(RouteDetail routeDetail) {

        // RouteDetail 对象转 JSON。
        return JSON.toJSONString(routeDetail);

    }

}
