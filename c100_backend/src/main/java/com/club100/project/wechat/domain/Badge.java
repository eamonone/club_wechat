package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/12/17 09:51:16
 * @Description: 徽章 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Badge extends BaseEntity implements Serializable {

    private Long badgeId; // Primary Key ID。

    private Boolean ifUserGetBadge; // 用户是否获得此徽章。
    private Double badgePrice; // 徽章价格。
    private Integer cityType; // 关联城市类型（详情见 club_city 表）。
    private Integer state; // 状态（1：可用；2：废弃。）。
    private Long routeId; // 路线主键 ID。
    private String badgeName; // 徽章名称。
    private String badgeDescription; // 徽章描述。
    private String badgePictureUrl; // 徽章图片链接 URL。

    public String toJson(Badge badge) {

        // Badge 对象转 JSON。
        return JSON.toJSONString(badge);

    }

}
