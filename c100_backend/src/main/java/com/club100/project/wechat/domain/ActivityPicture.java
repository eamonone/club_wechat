package com.club100.project.wechat.domain;

import com.alibaba.fastjson.JSON;
import com.club100.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Grazer_Ma
 * @Date: 2020/10/12 12:29:45
 * @Description: 活动详情图片 Entity。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityPicture extends BaseEntity implements Serializable {

    private Long activityPictureId; // Primary Key ID。

    private Integer state; // 收藏路线状态（1：可用；2：废弃。）。
    private Long activityId; // 活动的主键 ID。
    private String activityPictureUrl; // 活动图片链接 URL。
    private Long sysUserId;
    private String activityName;

    public String toJson(ActivityPicture activityPicture) {

        // ActivityPicture 对象转 JSON。
        return JSON.toJSONString(activityPicture);

    }

}
